@extends('layouts.app')

@section('content')
  	<div class="ui container">
  		<div class="ui basic segment">
  			<div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
            	<?php if(function_exists('bcn_display')){bcn_display();}?>
          	</div>
		  	@if (!have_posts())
		  		@include('ui/no-results')
		  	@endif
		</div>
	</div>
@endsection
