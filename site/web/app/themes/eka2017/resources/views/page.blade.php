@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
  	@php
  		$pagename = get_query_var('pagename');  
		if (!$pagename && $id > 0) {  
		    $post = $wp_query->get_queried_object();  
		    $pagename = $post->post_name;  
		}
	@endphp
	@if ($pagename == "avatud-akadeemia" or $pagename == "open-academy")
	    @include('partials.open-academy.content-open-academy')
	@elseif ($pagename == "kontaktid" or $pagename == "contacts")
		@include('partials.content-contacts')
	@elseif ($pagename == "erialad" or $pagename == "curricula")
		@include('partials.content-curricula')
	@else
		@include('partials.content-page')
	@endif
  @endwhile
@endsection
