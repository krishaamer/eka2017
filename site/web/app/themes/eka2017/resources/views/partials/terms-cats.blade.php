<div class="ui fluid vertical inverted menu">
  <div class="item">
    <h3 class="ui header"><? _e("Categories", "eka2017") ?></h3>
  </div>
  @php
    $cats = get_terms([
      'taxonomy' => 'category',
      'hide_empty' => true,
      'orderby' => 'count',
      'order' => 'ASC',
      'number' => 10,
    ]);
    if ($cats) {
      foreach ($cats as $cat) {
        echo '<a class="item" href="' . get_tag_link($cat->term_id) . '">' . $cat->name . "</a>";
      }
    }
  @endphp
</div>