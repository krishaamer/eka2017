<div class="ui fluid vertical inverted menu">
  <div class="item">
    <h3 class="ui header"><? _e("Tags", "eka2017") ?></h3>
  </div>
  @php
    $tags = get_terms([
      'taxonomy' => 'post_tag',
      'hide_empty' => true,
      'orderby' => 'count',
      'order' => 'ASC',
      'number' => 10,
    ]);
    if ($tags) {
      foreach ($tags as $tag) {
        echo '<a class="item" style="text-transform: capitalize;" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . "</a>";
      }
    }
  @endphp
</div>