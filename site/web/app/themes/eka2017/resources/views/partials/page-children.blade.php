<div class="ui fluid vertical inverted menu">
<?

  global $post;
  $ancestors = get_post_ancestors($post->ID);
  $level = count($ancestors);
  $root = ($ancestors) ? $ancestors[count($ancestors)-1]: $post->ID;
  $rootsub = $post;

  //echo '<div class="item">' . $level . '</div>';

  $parent = $post->ID;
  if ($level > 1) {
    $parent = $post->post_parent;
    $rootsub = $ancestors[0];
  }

  $sub_children = get_children([
    'post_parent' => $parent,
    'post_type'   => 'page',
    'orderby'     => 'menu_order',
    'order'       => 'ASC',
    'numberposts' => -1,
    'post_status' => 'publish' 
  ]);

  $sub_children_count = count($sub_children);
  if ($sub_children_count == 0) {
    $parent = $post->post_parent;
  }

  $slug_arr = ['erialad', 'curricula'];

  echo '<a class="item" style="border-bottom: 4px #fff solid;word-wrap: break-word;" href="' . get_permalink($root) . '"><h2>' . get_post($root)->post_title . '</h2></a>';

  if (in_array(get_post($root)->post_name, $slug_arr)) {
    echo '<a class="active item" style="border-bottom: 4px #fff solid;word-wrap: break-word;" href="' . get_permalink($rootsub) . '"><h2>' . get_post($rootsub)->post_title . '</h2></a>';
  }

  $children = get_children([
    'post_parent' => $parent,
    'post_type'   => 'page',
    'orderby'     => 'menu_order',
    'order'       => 'ASC',
    'numberposts' => -1,
    'post_status' => 'publish' 
  ]);

  if ($children) {
    foreach ($children as $child) {

      $class = "";
      if ($post->ID == $child->ID)
        $class = "active";

      $title = $child->post_title;
      if (!$title)
        $title = $child->post_name;

      echo '<a class="item ' . $class . '" style="text-transform: capitalize;" href="' . get_permalink($child->ID) . '">' . $title . "</a>";
    }
  }

  if ($level > 0 && $sub_children_count != 0) {
    echo '<a class="item" style="color:#fff !important; border-top: 4px #fff solid;" href="' . get_permalink($post->post_parent) . '"><i class="caret left icon"></i>' . __('Back', 'eka2017') . '</a>';
  }

?>
</div>