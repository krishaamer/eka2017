<div class="ui basic segment container">
	<div class="ui stackable doubling grid">
		<div class="four wide computer tablet only column">
			@include('partials/page-children')
		</div>
		<div class="twelve wide column">
			<div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				<?php if(function_exists('bcn_display')){ bcn_display(); }?>
			</div>
            <div class="ui basic segment">
                <div class="search-container">
	                <div class="ui contact search">
					  <div class="ui icon input">
					    <input class="prompt" type="text" placeholder="<? _e("Search contacts...", "eka2017") ?>">
					    <i class="search icon"></i>
					  </div>
					  <div class="results"></div>
					</div>
                </div>
            </div>
			<?php

				//Contacts:: get_contacts_array();
				// Get all the categories, arrange them into a multidimensional parent->cat->child array for hierarchy
				// 1 = undefined, 3 = erialad, 39 = tugiüksused

				if (ICL_LANGUAGE_CODE == 'en') {
					$depts = 270;
					$structs = 271;
				} else {
					$depts = 3;
					$structs = 39;
				}

				$allcats = get_terms('category', [ 
					'orderby'	=> 'name',
					'exclude'	=> [1, $depts, $structs]
				]);

				$cats = array();
				foreach($allcats as $cat) {
					$cats[$cat->parent][] = $cat;
				}

				?>
			<table class="ui table">
			  	<tbody>
					<?
					// Tugistruktuurid
					echo '<tr>
								<td colspan="5">
									<h1>Tugistruktuurid</h1>
								</td>
							</tr>';
					foreach($cats[$structs] as $cat) {
						echo '<tr>
								<td colspan="5">
									<h2><a href="' . $cat->term_id . '">' . $cat->name . '</a></h2>
								</td>
							</tr>
							<tr>' . Contacts::get($cat->term_id) . '</tr>';
					}

					// Teaduskonnad
					foreach($cats[$depts] as $div) {
						echo '<tr>
								<td colspan="5">
									<h1><a href="' . $div->term_id . '">' . $div->name . '</a></h1>
								</td>
							</tr>';

						if (array_key_exists($div->term_id, $cats)) {

							foreach($cats[$div->term_id] as $cat) {
								echo '<tr>
										<td colspan="5">
											<h2><a href="' . $cat->term_id . '">' . $cat->name . '</a></h2>
										</td>
									</tr>
									' . Contacts::get($cat->term_id);
							}

						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
