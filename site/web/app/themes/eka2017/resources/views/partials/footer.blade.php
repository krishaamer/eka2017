<footer>
	<div class="ui container" id="footer-menu" style="@if (!is_front_page()) border-top: 2px solid #fbfbfb; @endif">
		<div class="ui basic segment">
			<div class="ui stackable doubling internally celled grid container">
				<div class="four column row">
					<div class="column">
						@include("ui.footer.social")
					</div>
					<div class="column">
						@include("ui.footer.contact")
					</div>
					<div class="column">
						@include("ui.footer.chat")
					</div>
					<div class="column">
						@include("ui.footer.admissions")
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="ui container" id="copyright">
		<div class="ui stackable doubling grid" style="border-top: 2px solid #fbfbfb;">
			<div class="three column row">
				<div class="column">
					<nav class="ui stackable secondary menu" id="footer-copyright-menu">
						<div class="item">Artun.ee 2017</div>
					  	<a class="item" href="/">Terms & Conditions</a>
					  	<a class="item" href="/">Privacy Policy</a>
					</nav>
				</div>
				<div class="column">
					<nav class="ui stackable secondary menu" id="footer-logo-menu">
						<img class="ui centered image" alt="EKA logo" src="@asset('images/logo.png')">
					</nav>
				</div>
				<div class="column">
					<nav class="ui stackable secondary menu" id="footer-madeby-menu">
						<a class="right item" target="_blank" href="https://www.haam.co/">Made by HAAM</a>
					</nav>
				</div>
			</div>
		</div>
	</div>
</footer>
