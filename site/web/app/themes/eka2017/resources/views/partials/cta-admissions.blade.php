<div class="ui centered stackable grid">
  <div class="sixteen wide column">
    <div class="ui inverted segment">
      <div class="ui items">
        <div class="item">
          <div class="image">
          <?
            global $post;
            $media = get_attached_media('image', $post->ID);
            $title = get_permalink($post->ID);

            if ($media) {

              $images = [];
                foreach($media as $m) { 
                  array_push($images, $m);
                }
                echo '<img src="' . wp_get_attachment_image_src($images[0]->ID, "medium-large")[0] . '" alt="' . $title . '">';
              }
          ?>
          </div>
          <div class="content">
            <a class="header"><? _e("Want to do something like this? Admissions are open!", "eka2017") ?></a>
            <div class="description">
              <p><? _e("We're looking for people with deep interest in this area", "eka2017") ?></p>
            </div>
            <div class="extra">
              <a class="ui bottom attached basic red button" href="/sisseastumine"><? _e("Learn more", "eka2017") ?></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>