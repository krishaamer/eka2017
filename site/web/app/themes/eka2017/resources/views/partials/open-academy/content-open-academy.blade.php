<div class="ui fluid container" id="oa-banners">
	<div class="ui two column grid">
	  <div class="stretched row">
	    <div class="column" style="padding-right: 0;">
	        <div class="ui fluid image">
			<img src="https://unsplash.it/800/800/?random" alt="Plap">
		</div>
		<!--<h1><? _e("The first step of your career starts here") ?></h1>
		Avatud Akadeemia on Eesti suurim elukestva kunstiõppe keskkond. Akadeemia pakub mitmekesiseid kunsti-, disaini- ja arhitektuuriõppe võimalusi nii personaalseks kui professionaalseks arenguks.-->
	    </div>
	    <div class="column" style="padding-left: 0;">
	      <img class="ui image" src="https://unsplash.it/600/300/?random">
	      <img class="ui image" src="https://unsplash.it/600/300/?random">
	    </div>
	  </div>
	</div>
</div>

<div class="ui container" id="oa-short-courses">
	@include('partials.open-academy.short-courses')
</div>

<div class="ui container" id="oa-long-courses">
	@include('partials.open-academy.long-courses')
</div>

<div class="ui container" id="oa-quotes">
	@include('partials.open-academy.quotes')
</div>