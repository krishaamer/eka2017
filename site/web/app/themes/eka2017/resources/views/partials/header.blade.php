<?

$pagename = get_query_var('pagename');  
$style = "border-bottom: 2px solid #fbfbfb;";
$noborder = ["avatud-akadeemia", "open-academy"];
if (is_front_page() OR in_array($pagename, $noborder)) {
	$style = "";
}
?>

<header style="<?= $style ?>" id="main-header">
	<div class="ui fluid container">
		<nav class="ui fixed massive menu" id="mobile-nav">
			<div class="ui container">
				<a class="launch icon item open button">
			      <i class="content icon"></i>
			    </a>
				<a class="item brand" href="{{ home_url('/') }}">
				    <img class="ui mini image" alt="<? _e("Logo of the Estonian Academy of Arts") ?>" src="@asset('images/logo.png')">
				</a>
				<div class="fitted item"><?= get_search_form(false) ?></div>
			</div>
		</nav>
		<nav class="ui inverted small menu" id="ticker-nav">
			<div class="fitted item">
				<? 
					echo News::eka_news_ticker(); 
				?>	
			</div>
		    <div class="menu right">
		    	<?
		    	if (has_nav_menu('personal_menu'))
			    	echo wp_nav_menu(App::nav_defaults("personal_menu"));
			    ?>
		    </div>
		</nav>
		<nav class="ui small menu" id="secondary-nav">
		  	<?
		  	if (has_nav_menu('secondary_navigation'))
		        wp_nav_menu(App::nav_defaults("secondary_navigation"));
		    ?>
	    	<div class="menu right" id="language-menu">
	    		<?
	    		if (has_nav_menu('language_menu'))
			        echo wp_nav_menu(App::nav_defaults("language_menu"));
			    ?>
		    </div>
		</nav>
		<nav class="ui big menu" id="primary-nav">
			<a class="item brand" href="{{ home_url('/') }}">
			    <img class="ui mini image" alt="<? _e("Logo of the Estonian Academy of Arts") ?>" src="@asset('images/logo.png')">
			</a>
			<? 
			if (has_nav_menu('primary_navigation'))
		        echo wp_nav_menu(App::nav_defaults("primary_navigation"));
		    ?>
		</nav>
	</div>
</header>
