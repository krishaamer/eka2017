<article @php(post_class())>
  <div class="ui container">
      <div class="ui stackable doubling grid">
          <div class="twelve wide column">
            <div class="ui basic segment" style="border-bottom: 2px solid #fbfbfb;">
              <div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                <?php if(function_exists('bcn_display')){bcn_display();}?>
              </div>
              <header>
                <h1 class="entry-title">{{ get_the_title() }}</h1>
              </header>
              <?
                global $post;
                echo Media::get_attached_images($post);
              ?>
              <div class="entry-content">
                @php(the_content())
              </div>
              @php
                $tags = wp_get_post_tags($post->ID);
                if ($tags) {
                  foreach ($tags as $tag) {

                    echo '<a class="ui mini basic black button tag" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . "</a>";
                  }
                }
              @endphp
            </div>
            @include('partials/cta-admissions')
            {!! News::related_posts() !!}
          </div>
      </div>
  </div>
</article>
