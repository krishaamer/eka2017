<div class="ui basic segment container">
	<div class="ui stackable doubling grid">
		<div class="four wide computer tablet only column">
			@include('partials/page-children')
		</div>
		<div class="twelve wide column">
			<div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				<?php if(function_exists('bcn_display')){ bcn_display(); }?>
			</div>
			<article>
				<header>
                  <h1 class="entry-title">{!! App\title() !!}</h1>
                </header>
				<div class="entry-content">
					<?

					global $post;

					echo the_content();
					echo Media::get_attached_images($post, "large", 2, true, false);
					$tpl = get_page_template_slug($post->ID);
					//echo $tpl;

					if ($tpl == 'page-eriala-esileht-new.php') {
						
						if (empty($post->post_content)) {
								$children = get_children([
								    'post_parent' => $post->ID,
								    'post_type'   => 'page',
								    'orderby'     => 'menu_order',
								    'order'       => 'ASC',
								    'numberposts' => -1,
								    'post_status' => 'publish' 
							  	]);

							  	foreach($children as $child) break;
									echo '<div class="ui basic segment">' . $child->post_content . '</div>';  
						}

						echo Highlights::get_testimonials();
						echo Media::eka_load_gallery_new($post, ['show_on_page' => $post->post_parent]);
					}

					if ($tpl == 'page-eriala-inimesed-new.php')
						echo Contacts::get_course_contacts($post);

					if ($tpl == 'page-eriala-galerii-new.php')
						echo Media::get_course_gallery($post);

					/*
					
					$more = get_field("column_right", $post->ID);
					if ($more)
						echo $more;

					if (empty($post->post_content)) {
						echo Courses::get_course_news();
					}
					*/
					
					?>
				</div>
			</article>
		</div>
	</div>
</div>
