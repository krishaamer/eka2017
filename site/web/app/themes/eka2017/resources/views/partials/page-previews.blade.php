<?
  global $post;

  $children = get_children([
    'post_parent' => $post->ID,
    'post_type'   => 'page',
    'orderby'     => 'title',
    'order'       => 'ASC',
    'numberposts' => -1,
    'post_status' => 'publish' 
  ]);

  echo Media::get_previews($children);  
?>