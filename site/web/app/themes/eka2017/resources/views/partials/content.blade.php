<article @php(post_class())>
	<div class="ui fluid card">
		<div class="image">
		<?
			$post_id = get_the_ID();
			$media = get_attached_media('image', $post_id);
			$link = get_permalink($post_id);
			$title = get_permalink($post_id);

			if ($media) {

				$images = [];
			  	foreach($media as $m) { 
			  		array_push($images, $m);
			  	}
  				echo '<img src="' . wp_get_attachment_image_src($images[0]->ID, "medium_large")[0] . '" alt="' . $title . '">';
  			}
		?>
		</div>
		<div class="content">
		  	<header>
		    	<h2 class="ui header entry-title">
		    		<a href="{{ get_permalink() }}">{{ get_the_title() }}</a>
		    	</h2>
		  	</header>
		  	<div class="meta">
		  		<? 
			  		$category = get_the_category();
			  		if ($category) {
			  			echo $category[0]->cat_name;
			  		}
		  		?>
				<time class="updated" datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
		  	</div>
		</div>
	</div>
</article>