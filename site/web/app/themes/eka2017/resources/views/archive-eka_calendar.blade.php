@extends('layouts.app')

@section('content')
  	<div class="ui container">
	    <div class="ui stackable doubling grid">
		    <div class="twelve wide column">
		        <div class="ui basic very padded segment">
		        	<div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
                      <?php if(function_exists('bcn_display')){bcn_display();}?>
                    </div>
		          	<h1><? _e("Calendar") ?></h1>
				  	@if (!have_posts())
				  		@include('ui/no-results')
				  	@endif
				  	@while (have_posts()) @php(the_post())
				    	@include ('partials.content-'.(get_post_type() === 'post' ?: get_post_type()))
				  	@endwhile
		  		</div>
		  	</div>
		</div>
	</div>
@endsection
