<div class="ui fluid container" id="highlights">
	<div id="glide" class="glide" style="margin-bottom: -24px;margin-top: -16px;">
		<!--
		<div class="glide__featured">
	        <button class="glide__arrow"><? _e("Featured", "eka2017") ?></button>
	    </div>
	    -->
	    <div class="glide__arrows">
	        <button class="glide__arrow prev" data-glide-dir="<"><i class="caret left icon"></i></button>
	        <button class="glide__arrow next" data-glide-dir=">"><i class="caret right icon"></i></button>
	    </div>
	    <div class="glide__wrapper">
	        <ul class="glide__track">
	            {!! Highlights::glides() !!}
	        </ul>
	    </div>
	    <div class="glide__bullets"></div>
	</div>
	<div id="glide2" class="glide" style="background: #000;">
	    <div class="glide__arrows">
	        <button class="glide__arrow prev" data-glide-dir="<"><i class="caret left icon"></i></button>
	        <button class="glide__arrow next" data-glide-dir=">"><i class="caret right icon"></i></button>
	    </div>
	    <div class="glide__wrapper" style="margin-bottom: -10px;">
	        <ul class="glide__track">
	            {!! Highlights::news() !!}
	        </ul>
	    </div>
	</div>
</div>
