<h2 class="header"><? _e("Social", "eka2017") ?></h2>
<div class="ui vertical text menu">
  <a class="item" target="_blank" href="https://www.facebook.com/Eestikunstiakadeemia"><i class="facebook icon"></i> Facebook</a>
  <a class="item" target="_blank" href="https://www.instagram.com/estonian_academy_of_arts/"><i class="instagram icon"></i> Instagram</a>
  <a class="item" target="_blank" href="https://www.flickr.com/groups/eka/"><i class="flickr icon"></i> Flickr</a>
  <a class="item" target="_blank" href="http://vimeo.com/artun/"><i class="vimeo icon"></i> Vimeo</a></a>
  <a class="item" target="_blank" href="https://www.youtube.com/channel/UCL0V3XR8iViHNtRYDK5G1yQ"><i class="youtube icon"></i> YouTube</a>
</div>