<div class="ui fluid stackable doubling relaxed grid container" id="activity" style="margin-top: 1rem;">
	<div class="three column row">
		<div class="column">
			@include("ui.calendar")
		</div>
		<div class="column">
			@include("ui.news")
		</div>
		<div class="column">
			@include("ui.competitions")
		</div>
	</div>
</div>