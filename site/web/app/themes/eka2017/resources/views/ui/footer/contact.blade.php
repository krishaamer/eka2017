<h2 class="header"><? _e("Estonian Academy of Arts", "eka2017") ?></h2>
<div class="ui vertical text menu">
  <a class="item" target="_blank" href="https://goo.gl/maps/ECLiQBWotJF2"><i class="marker icon"></i> Estonia pst 7 10143 Tallinn</a>
  <a class="item" target="_blank" href="mailto:artun@artun.ee"><i class="mail outline icon"></i> artun@artun.ee</a></a>
  <a class="item" target="_blank" href="skype:003726267301"><i class="call icon"></i> +372 6267301</a>
</div>