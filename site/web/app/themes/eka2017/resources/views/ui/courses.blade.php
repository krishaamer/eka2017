<div class="ui container" id="courses" style="margin-top: 3rem;">
	<nav class="ui stackable secondary fluid menu" id="courses-menu">
		<?
			$slug = "/erialad/";
			if(get_locale() == "en_US") {
			  $slug = "/en/curricula/";
			}
			echo '<a class="item" href="' . $slug . '">
					<h1>' . __("Curricula", "eka2017") . '</h1>
				</a>';
		?>
	</nav>
	<div class="ui stackable doubling grid">
		<div class="row">
			<div class="column">
				<div class="ui stackable inverted menu">
				  <a class="item active" data-tab="first"><? _e("Bachelor", "eka2017") ?></a>
				  <a class="item" data-tab="second"><? _e("Master's", "eka2017") ?></a>
				  <a class="item" data-tab="third"><? _e("PhD", "eka2017") ?></a>
				  <a class="item" data-tab="fourth"><? _e("Cyclical Studies", "eka2017") ?></a>
				  <a class="item" data-tab="fifth"><? _e("Open Academy", "eka2017") ?></a>
				</div>
				{!! Courses::courses_by_cp() !!}
			</div>
		</div>
	</div>
</div>
