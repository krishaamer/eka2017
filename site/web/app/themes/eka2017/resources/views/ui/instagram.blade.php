<div class="ui fluid container" id="instagram" style="margin-top: 5rem;">
	<div class="ui container">
		<nav class="ui stackable secondary fluid menu" id="instagram-menu">
			<a class="item" target="_blank" href="https://www.instagram.com/estonian_academy_of_arts/">
				<h1>@estonian_academy_of_arts</h1>
			</a>
		  	<div class="right item">
		  		<a class="ui basic black button" target="_blank" href="https://www.instagram.com/estonian_academy_of_arts/">
		  			<? _e("View all", "eka2017") ?>
		  		</a>
		  	</div>
		</nav>
	</div>
	<div class="my-instashow"></div>
</div>
