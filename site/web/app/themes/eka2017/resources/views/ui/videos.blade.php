<div class="ui stackable fluid grid container" id="videos" style="margin-top: 5rem;">
	<nav class="ui stackable secondary fluid menu" id="videos-menu">
		<a class="item" target="_blank" href="https://www.youtube.com/channel/UCL0V3XR8iViHNtRYDK5G1yQ">
			<h1><? _e("Videos", "eka2017") ?></h1></a>
	  	<div class="right item">
	  		<a class="ui basic black button" target="_blank" href="https://www.youtube.com/channel/UCL0V3XR8iViHNtRYDK5G1yQ">
	  			<? _e("View all", "eka2017") ?>
	  		</a>
	  	</div>
	</nav>
	{!! Videos::videos() !!}
</div>
