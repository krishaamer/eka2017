@extends('layouts.app')

@section('content')
  	<div class="ui fluid container">
  		<div class="ui basic very padded segment">
	  		<div class="ui breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
	         	 <?php if(function_exists('bcn_display')){bcn_display();}?>
	        </div>
	        <h1>{!! App\title() !!}</h1>
		    <div class="ui stackable doubling centered three column masonry grid">
			  	@if (!have_posts())
			  		@include('ui/no-results')
			  	@endif
			  	@while (have_posts()) @php(the_post())
			  		<div class="column">
			    	@include ('partials.content-'.(get_post_type() === 'post' ?: get_post_type()))
			    	</div>
			  	@endwhile
			</div>
		</div>
	</div>
@endsection
