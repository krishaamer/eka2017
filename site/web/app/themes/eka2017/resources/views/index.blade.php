@extends('layouts.app')

@section('content')
  <div class="ui container">
    <div class="ui basic segment">
      <div class="ui stackable doubling grid">
        <div class="ten wide column">
          <div class="ui fluid card">
            <div class="content">
              <div class="header curricula" style="background:#f00 !important;"><h2>{!! App\title() !!}</h2>
              </div>
            </div>
          </div>
        </div>
        @if (!have_posts())
          <div class="ten wide column">
            @include('ui/no-results')
          </div>
        @endif
        @while (have_posts()) @php(the_post())
          <div class="ten wide column">
            @include ('partials.content-'.(get_post_type() === 'post' ?: get_post_type()))
          </div>
        @endwhile
      </div>
    </div>
  </div>
@endsection
