<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')
  <body @php(body_class())>
    <div class="ui left vertical inverted menu sidebar">
      <?
      if (has_nav_menu('mobile_menu'))
        echo wp_nav_menu(App::nav_defaults("mobile_menu"));
      ?>
    </div>
    <div class="pusher">
      @include('partials.header')
      <main role="document">
        @if (is_front_page())
          @include('ui.highlights')
          @include('ui.courses')
          @include('ui.activity')
          @include('ui.videos')
          @include('ui.instagram')
        @else
          @yield('content')
        @endif
      </main>
      @php(do_action('get_footer'))
      @include('partials.footer')
      @php(wp_footer())
    </div>
  </body>
</html>
