export default {
	init() {

            $('.menu .item').tab();
            $('.ui.embed').embed();
	}, 
	finalize() {

            $('.my-instashow').instaShow({
                  api: '/instashow/api/',
                  source: '@estonian_academy_of_arts',
                  width: 'auto',
                  gutter: 0,
                  height: '280px',
                  columns: 6,
                  scrollbar: false,
                  rows: 1,
                  direction: 'horizontal',
                  lang: 'en',
                  popupInfo: '',
            });

            $("#glide").glide({type: "carousel", autoplay: false});
            $("#glide2").glide({type: "carousel", autoplay: false});
      },
};
