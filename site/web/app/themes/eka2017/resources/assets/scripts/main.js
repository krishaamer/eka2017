/** import external dependencies */
import 'jquery';
import 'semantic-ui-sass';
import 'glidejs';

/** import local dependencies */
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import contact from './routes/contact';
import './ui/jquery.instashow.packaged.js';

/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */
const routes = new Router({
  common,
  home,
  contact,
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
