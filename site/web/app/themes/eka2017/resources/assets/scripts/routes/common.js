export default {
	init() {
    
	$('.ui.sidebar').sidebar('attach events', '.open.button', 'show');
		$('.open.button').removeClass('disabled');

		// News ticker
		function tick(){
			$('#newsticker li:first').slideUp(function () { $(this).appendTo($('#newsticker')).slideDown(); });
		}
		
		$('#newsticker').mouseover(() => {
			window.clearInterval(intervalID);
		}).mouseout(() =>{
			intervalID = window.setInterval(() => {tick();}, 4000);
		});
		
		let intervalID = window.setInterval(() => {tick();}, 4000);
	},
	finalize() {

		$("#glide-testimonials").glide({autoplay: false});
		$('#primary-nav').popup({popup: $('.admission.popup'), hoverable: true, position: 'bottom left', delay: {show: 300, hide: 800}});

		const content = [
			{ title: 'Andorra' },
			{ title: 'United Arab Emirates' },
			{ title: 'Afghanistan' },
			{ title: 'Antigua' },
			{ title: 'Anguilla' },
			{ title: 'Albania' },
			{ title: 'Armenia' },
			{ title: 'Netherlands Antilles' },
			{ title: 'Angola' },
			{ title: 'Argentina' },
			{ title: 'American Samoa' },
			{ title: 'Austria' },
			{ title: 'Australia' },
			{ title: 'Aruba' },
			{ title: 'Aland Islands' },
			{ title: 'Azerbaijan' },
			{ title: 'Bosnia' },
			{ title: 'Barbados' },
			{ title: 'Bangladesh' },
			{ title: 'Belgium' },
			{ title: 'Burkina Faso' },
			{ title: 'Bulgaria' },
			{ title: 'Bahrain' },
			{ title: 'Burundi' },
		];

		$('.ui.contact.search').search({source: content});
	},
};
