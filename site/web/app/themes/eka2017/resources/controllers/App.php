<?php

namespace App;

use Sober\Controller\Controller;
use App\NavWalker;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function nav_defaults ($location) {

		$args = ['theme_location'  => $location,
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => '',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
	        'items_wrap'      => '%3$s',		
	        'depth'           => 0,
			'walker'          => new NavWalker];

		return $args;
	}
}
