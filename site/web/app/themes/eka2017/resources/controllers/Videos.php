<?php

namespace App;

use Sober\Controller\Controller;

class Videos extends Controller {

	public static function videos() {
  
	  $cache_do = false;
	  $cache_name = "eka_videos_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {

		$items = [];
		$feat = "";
		$posts = get_posts([
		  'numberposts' => 6,
		  'orderby' => 'rand',
		  'post_type'   => 'eka_videos',
		  'post_status' => 'publish'
		]);

		foreach ($posts as $post) {
		  
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_video_url = get_field("video", $post_id, false);
		  	$post_video_code = substr($post_video_url, stripos($post_video_url, "=")+1);

		  	$items[] = '<div class="ui embed" data-source="youtube" data-id="' . $post_video_code . '"></div>';
		}

		$count = count($items);
		if ($count > 0) {

			if ($count == 1) {
				$out = '<div class="row">
							<div class="sixteen wide column">' . $items[0] . '</div>
						</div>';
			}
			if ($count == 2) {
				$out = '<div class="row">
							<div class="eight wide column">' . $items[0] . '</div>
							<div class="eight wide column">' . $items[1] . '</div>
						</div>';
			}
			if ($count == 3) {
				$out = '<div class="equal width row">
							<div class="column">' . $items[0] . '</div>
							<div class="column">' . $items[1] . '</div>
							<div class="column">' . $items[2] . '</div>
						</div>';
			}
			if ($count == 4) {
				$out = '<div class="row">
							<div class="eight wide column">' . $items[0] . $items[1] . '</div>
							<div class="eight wide column">' . $items[2] . $items[3] . '</div>
						</div>';
			}
			if ($count > 4) {
				$out = '<div class="row">
							<div class="four wide column">
								<div class="ui stackable grid">
									<div class="one column row">
										<div class="column">'
											. $items[0] .
										'</div>
									</div>
									<div class="one column row">
										<div class="column">'
											. $items[1] .
										'</div>
									</div>
								</div>
							</div>
							<div class="eight wide column">' . $items[4] . '</div>
							<div class="four wide column">
								<div class="ui stackable grid">
									<div class="one column row">
										<div class="column">'
											. $items[2] .
										'</div>
									</div>
									<div class="one column row">
										<div class="column">'
											. $items[3] .
										'</div>
									</div>
								</div>
							</div>
						</div>';
			}
		}

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}
}
