<?php

namespace App;

use Sober\Controller\Controller;

class Highlights extends Controller {

	private static function build_glide ($posts) {

		$out = "";
		foreach ($posts as $post) {
		  
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;

		  	$post_image_ID = get_field('featured_image', $post_id);
		  	$post_image_title = get_field('overlay_title', $post_id);
		  	$post_image_url = wp_get_attachment_image_url($post_image_ID, "wide");
		  	$post_image_container = "";

		  	if (!empty($post_image_ID)) {

				$post_image_container = '<li class="glide__slide">
											<div class="glide-text-overlay">
												<h2 class="glide-title">' . $post_image_title . '</h2>
											</div>
											<img src="' . $post_image_url . '" alt="' . $post_title . '">
										</li>';
		  	}

		  	$out .= $post_image_container;
		}

		return $out;
	}

	public static function glides () {
  
	  $cache_do = false;
	  $cache_name = "eka_highlights_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		

		$first = get_posts([
		  	'numberposts' => 1,
		  	'orderby' => 'rand',
		  	'post_type' => 'any',
		  	'meta_key' => 'is_first',
		  	'meta_value' => true,
		  	'post_status' => 'publish'
		]);

		$posts = get_posts([
		  	'numberposts' => 5,
		  	'orderby' => 'rand',
		  	'post_type' => 'any',
		  	'meta_key' => 'is_featured',
		  	'meta_value' => true,
		  	'post_status' => 'publish'
		]);

		$out = self::build_glide($first) . self::build_glide($posts);

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}

	public static function news () {

		$out = "";
		$posts = get_posts([
		  	'numberposts' => 16,
		  	'orderby' => 'post_date',
		  	'order' => 'DESC',
		  	'post_type' => 'eka_project',
		  	'post_status' => 'publish'
		]);

		$container = "";
		$counter = 0;
		$count = count($posts);
		foreach ($posts as $post) {
		  
		  	$counter++;
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_category = get_the_category($post_id);
		  	$post_category_name = "";
		  	if ($post_category)
		  		$post_category_name = $post_category[0]->cat_name;
		  	$post_attached_media = get_attached_media('image', $post_id);
		  	$imgs = [];
			$image_id = "";
			$post_image_container = "";
			if (!empty($post_attached_media)) {
			  	foreach($post_attached_media as $i) {
			  		array_push($imgs, $i);
			  	}
			  	$image_id = $imgs[0]->ID;

			  	if (function_exists('fly_get_attachment_image_src')) {
			  		$post_image = fly_get_attachment_image_src($image_id, "tiny_square")["src"];
			  	}
			  	$post_image_container = '<img class="left floated tiny ui image" src="' . $post_image . '" alt="' . $post_title . '">';
		  	}

		  	$out .= '<div class="sixteen wide tablet four wide computer column" style="padding-bottom:0;padding-top:0;">
		  			<a class="ui card" href="' . $post_link . '">
					  		<div class="content">' .
					      		$post_image_container
				      			. '<div class="meta" style="color:#fff;">
				      				<div class="category">' . $post_category_name . '</div>
				      			</div>
				      			<h6 class="eka tiny title">' . mb_strtolower($post_title, mb_detect_encoding($post_title)) . '</h6>
					      	</div>
					  	</a>
					 </div>';

			if ($counter == 4) {

		  		$container .= '<li class="glide__slide">
		  						<div class="ui grid container">' . $out . '</div>
		  						</li>';
		  		$counter = 0;
		  		$out = "";
		  	}
		}

		return $container;
	}

	private static function build_testimonials ($posts) {

		$out = "";
		foreach ($posts as $post) {
		  
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;

		  	$post_name = get_field('name', $post_id);
		  	$post_affiliation = get_field('affiliation', $post_id);
		  	$post_quote = get_field('quote', $post_id);
		  	$post_photo = wp_get_attachment_image_url(get_field('photo', $post_id), 'thumbnail');

		  	if (!empty($post_photo)) {

				$out .= '<div class="glide__slide">
							<div class="ui container">
								<div class="ui grid">
									<div class="row">
										<div class="sixteen wide column">
											<div class="entry-content">
												<i class="big grey quote left icon"></i>' . $post_quote . '<i class="big grey quote right icon"></i>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="four wide column">
											<img src="' . $post_photo . '" alt="' . $post_quote. '">
										</div>
										<div class="twelve wide column">
											<div class="ui fluid card">
												<div class="content">
													<div class="header">' . $post_name . '</div>
													<div class="meta eka affiliation">' . $post_affiliation . '</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>';
		  	}
		}

	  	$out = '<div class="ui basic segment testimonials">
				  	<div id="glide-testimonials" class="glide">
					    <div class="eka testimonial glide__arrows">
					        <button class="eka testimonial glide__arrow prev" data-glide-dir="<"><i class="caret left icon"></i></button>
					        <button class="eka testimonial glide__arrow next" data-glide-dir=">"><i class="caret right icon"></i></button>
					    </div>
					    <div class="glide__wrapper">
					        <div class="glide__track">'
					        	. $out . 
					       	'</div>
					    </div>
					</div>
				</div>';

		return $out;
	}

	public static function get_testimonials () {
  
  		$out = "";
	  	$cache_do = false;
	  	$cache_name = "eka_testimonials_cache_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name); 
	  
	  	if (false === ($out = get_transient($cache_name))) {

			$posts = get_posts([
			  	'numberposts' => 5,
			  	'orderby' => 'rand',
			  	'post_type' => 'eka_testimonials',
			  	'post_status' => 'publish'
			]);
			$out = self::build_testimonials($posts);

			// Put the results in a transient. Expire after N hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}
}
