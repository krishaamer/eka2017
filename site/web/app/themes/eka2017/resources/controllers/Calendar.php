<?php

namespace App;

use Sober\Controller\Controller;

class Calendar extends Controller {

	public static function cards()
    {
        $cache_do = false;
	    $cache_time = 1 * 10 * 60;
	    $cache_name = "card_calendar_cache_" . get_locale();
	  
	    if(!$cache_do) {
	        delete_transient($cache_name); 
	    }
	  
	    if (false === ($out = get_transient($cache_name))) {
	  
	            $card_slug = '/kalender/';
	            if(get_locale() == "en_US") {
	              $card_slug = "/en/calendar/";
	            }

	            $posts = get_posts(['post_type' => 'eka_calendar', 
	            					'numberposts' => 6, 
	            					'post_status' => 'publish',
	            					'meta_key' => 'event_start_date',
	            					'orderby' => 'meta_value_num',
	            					'order' => 'DESC']);

	            $i = 0;
	            $size = count($posts);
	            foreach ($posts as $post) {

	            	$i++;
	              	$post_id = $post->ID;
	              	$post_link = get_permalink($post_id);
	              	$post_title = $post->post_title;

	              	$post_category_arr = get_the_category($post_id);
	              	$post_category = "";
	              	if ($post_category_arr) {
	              		$post_category = $post_category_arr[0]->cat_name;
	              	}
	              	$post_event_start_date = get_post_meta($post_id, 'event_start_date', true);
	              	$post_event_datetime = date_create_from_format("Ymj", $post_event_start_date);

	              	$post_event_year = "NaN";
	              	$post_event_month = "NaN";
	              	$post_event_day = "NaN";
	              	if ($post_event_datetime) {
	              		$post_event_year = date_format($post_event_datetime, "Y");
	              		$post_event_month = date_format($post_event_datetime, "M");
	              		$post_event_day = date_format($post_event_datetime,"d");
	              	}
	              	$style = "border-bottom: 2px #fbfbfb solid;";
				  	if ($i == $size) {
				  		$style = "";
				  	}
	              	
	              	$out .= '<div class="ui fluid card" style="' . $style . '">
	              				<div class="ui items">
								  	<div class="item">
									  	<div class="ui left floated statistic">
									  		<div class="value">' . $post_event_day . '</div>
									  		<div class="label">' . $post_event_month . ' ' . $post_event_year . '</div>
									 	</div>
									    <div class="content">
									    	<div class="meta">
									        	<span></span>
									      	</div>
									      	<div class="header">
									      		<a href="' . $post_link . '">' . $post_title . '</a>
									      	</div>
									      	<div class="meta">
									        	<span>' . strtoupper($post_category) . '</span>
									      	</div>
									    </div>
									</div>
								</div>
	              			</div>';
	            }

	            $out = '<a href="' . $card_slug . '"><h1>' . __("Calendar", "eka2017") . '</h1></a>' 
	            		. $out . 
	            		'<a class="ui bottom attached basic black button" href="' . $card_slug . '">' . __("Read more", "eka2017") . '</a>';

	            // Put the results in a transient. Expire after $cache_time.
	            set_transient($cache_name, $out, $cache_time);
	    }
	  
	    return $out;
    }
}
