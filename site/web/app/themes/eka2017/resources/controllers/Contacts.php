<?php

namespace App;

use Sober\Controller\Controller;

class Contacts extends Controller {

	public static function get_tags_table ($tags) {

		$out = "";
		if ($tags) {
			$out = '<a href="' . get_permalink() . '">' . __('All', "eka2017") . '</a><br />';
			foreach ($tags as $tag) {
				$out .= '<a href="' . get_permalink() . '?tag=' . $tag->slug . '">' . $tag->name . '</a><br />';
			}
		}

		return $out;
	}

	public static function get_contacts_array () {

		$contacts = get_posts([ 
			'post_type' 		=> 'eka_contacts',
			'numberposts'     	=> -1,
			'orderby'         	=> 'menu_order title',
			'order'           	=> 'ASC' 
		]);

		var_dump($contacts);

		//wp_localize_script('my_js_library', 'php_vars', []);

		return true;
	}

	public static function get_contacts_table ($posts) {

		$out = "";
		foreach ($posts as $post) {

			$out .= '<tr>';

			$phone_details = "";
			$phone = get_field('contact_phone', $post->ID);
			$mobile = get_field('contact_mobile', $post->ID);
			$link = get_field('contact_www', $post->ID);

			if ($phone)
				$phone_details = $phone;

			if ($mobile)
				$phone_details .= ' ' . $mobile;

			if ($link)
				$link = '<a href="' . $link . '" target="_blank" style="border: none;"><i class="external icon"></i></a>';
			
			$out .= '<td>' . Media::get_attached_images($post, 'thumbnail', 1, false) . '</td>
			<td>' . $post->post_title . '</td>
			<td>' . get_field('contact_role', $post->ID) . '</td>
			<td>' . get_field('contact_email', $post->ID) . '</td>
			<td>' . $phone_details . '</td>
			<td>' . $link . '</td>
			<td>' . get_field('contact_info', $post->ID) . '</td>';

			$out .= '</tr>';
		}
		$out = '<table class="ui very basic very compact table">' . $out . '</table>';

		return $out;
	}

	public static function get_course_contacts ($post) {

		global $wp_query;

		$out = "";
		$category = get_the_category($post->ID); 
		$et_cat_id = icl_object_id($category[0]->term_id, 'category', true, 'et');

		if($wp_query->query_vars['tag'] != '') {
			$selected_tag = $wp_query->query_vars['tag'];
			$tagObj = get_term_by('slug', $selected_tag, 'post_tag');
			$tag_id = $tagObj->term_id;
			$selected_tag = $tagObj->slug;
		} else {
			$selected_tag = 'all';
			$tag_id = '';
		}	

		$tags = Taxonomy::get_category_tags([ 
			'categories' 	=> $et_cat_id, 
			'taxonomy' 		=> 'category',
			'term' 			=> 'post_tag',
			'post_type'		=> 'eka_contacts'
		]);
		$out .= self::get_tags_table($tags);

		$contacts = get_posts([ 
			'post_type' 		=> 'eka_contacts',
			'numberposts'     	=> -1,
			'tag_id'			=> $tag_id,
			'lang'				=> '',
			'category__in' 		=> [$et_cat_id],
			'orderby'         	=> 'menu_order title',
			'order'           	=> 'ASC' 
		]);
		$out .= self::get_contacts_table($contacts);

		return $out;
	}

	public static function get_contacts () {

		if (!function_exists("pll_get_post"))
			return false;
  
	  	$cache_do = false;
	  	$cache_name = "eka_contacts_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name);
	  
	  	if (false === ($out = get_transient($cache_name))) {
		
			$posts = get_posts(['post_type' => 'eka_contacts', 
            					'numberposts' => -1, 
            					'post_status' => 'publish',
            					'order' => 'DESC']);

			foreach ($posts as $post) { 

				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_link = get_permalink($post_id);

				$out .= '<div class="column">
							<div class="ui fluid card">
								<div class="content">
									<a class="header" href="' . $post_link . '">' . $post_title . '</a>
								</div>
							</div>
						</div>';
			}

			$out = '<div class="ui stackable doubling four column grid">' . $out . '</div>';

			// Put the results in a transient. Expire after 1 hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}

	public static function get($id) {
		global $post;
		
		//$et_cat_id = pll_get_term((int)$_POST['cat'], 'et');
		$out = "";
		$contacts = get_posts([ 
			'post_type' 	=> 'eka_contacts',
			'numberposts'   => -1,
			'category__in' 	=> [$id],
			'orderby'       => 'menu_order',
			'order'         => 'ASC',
			'meta_query'    => [['key' => 'contact_list', 'value' => true, 'compare' => '==']]]
		);
		foreach ($contacts as $post) {

			$out .= '<tr>
						<td>' . Media::get_attached_images($post, 'thumbnail', 1, false, false, 'tiny') . '</td>
						<td>' . trim($post->post_title) . '</td>
						<td>' . get_field('contact_role') . '</td>
						<td><a href="mailto:' . get_field('contact_email') . '">' . get_field('contact_email') . '</a></td>
						<td>' . get_field('contact_phone') . '</td>
					</tr>';
		}

		return $out;
	}
}
