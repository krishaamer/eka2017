<?php

namespace App;

use Sober\Controller\Controller;

class Courses extends Controller {

	private static function get_cols ($courses) {

		$i = 0;
		$items = "";
		$cols = "";
		$count = count($courses);
		foreach ($courses as $course) {
		  
		  	$i++;
		  	$items .= '<a class="item" href="' . $course["url"] . '">
		  					<i class="genderless icon"></i>
		  					<div class="content"><div class="header">' . $course["title"] . '</div></div>
		  				</a>';

		  	if ($count >= 5) { 
			  	if ($i == 5) {

			  		$cols .= '<div class="three wide column">
			  					<div class="ui middle aligned selection list">' . $items . '</div>
			  				</div>';
			  		$items = "";
			  		$i = 0;
			  	}
			} else {
				if ($i == $count) {

			  		$cols .= '<div class="three wide column">
			  					<div class="ui middle aligned selection list">' . $items . '</div>
			  				</div>';
			  		$items = "";
			  		$i = 0;
			  	}
			}
		}

		$wrapper = '<div class="ui stackable doubling grid">' . $cols . '</div>';
		return $wrapper;
	}

	public static function find_image ($id) {

		$alt = '';
		$src = '';

		$out = '<img src="' . $src . '" alt="' . $alt . '">';

		return $out;	
	}

	public static function get_open_academy_courses () {

		if (!function_exists("pll_get_post"))
			return false;
  
	  	$cache_do = false;
	  	$cache_name = "eka_open_academy_courses_cache_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name);
	  
	  	if (false === ($out = get_transient($cache_name))) {
		
			$posts = get_posts([
			  	'numberposts' => 4,
			  	'orderby'=> 'title', 
			  	'order' => 'ASC',
			  	'post_type'   => 'eka_open'
			]);

			foreach ($posts as $post) { 

				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_related_post_ID = get_field("related_page_id", $post_id);
				if ($post_related_post_ID) {

					$pid = pll_get_post($post_related_post_ID);
					$post_title = get_the_title($pid);
					$post_related_page_url = get_permalink($pid);

					$out .= '<div class="ui column">
								<a class="ui card" href="' . $post_related_page_url . '">
									<div class="image">' . self::find_image($pid) . '</div>
								</a>
							</div>';
				}
			}

			$out = '<div class="ui four column stackable doubling grid">' . $out . '</div>';

			// Put the results in a transient. Expire after 1 hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}

	public static function courses_by_cp() {

		if (!function_exists("pll_get_post"))
			return false;
  
	  	$cache_do = false;
	  	$cache_name = "eka_courses_by_cp_cache_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name);
	  
	  	if (false === ($out = get_transient($cache_name))) {
		
			$ba = [];
			$ma = [];
			$phd = [];
			$distance = [];
			$open_academy = [];
			$posts = get_posts([
			  	'numberposts' => -1,
			  	'orderby'=> 'title', 
			  	'order' => 'ASC',
			  	'post_type'   => 'eka_courses'
			]);

		foreach ($posts as $post) { 

			$post_id = $post->ID;
			$post_title = $post->post_title;
			$post_related_page_url = "";
			$post_related_page_ID = get_field("related_page_id", $post_id);
			if ($post_related_page_ID) {

				$pid = pll_get_post($post_related_page_ID);
				$post_title = get_the_title($pid);
				$post_related_page_url = get_permalink($pid);
			}
			$post_academic_level = get_field("academic_level", $post_id);

			if ($post_academic_level) {
				$props = [
					"title" => $post_title,
					"url" => $post_related_page_url
				];

				if (in_array("ba", $post_academic_level))
					array_push($ba, $props);

				if (in_array("ma", $post_academic_level))
					array_push($ma, $props);

				if (in_array("phd", $post_academic_level))
					array_push($phd, $props);

				if (in_array("distance", $post_academic_level))
					array_push($distance, $props);

				if (in_array("open_academy", $post_academic_level))
					array_push($open_academy, $props);
			}
		}

		$out = '<div class="ui tab basic segment active" data-tab="first">' . Courses::get_cols($ba) . '</div>
				<div class="ui tab basic segment" data-tab="second">' . Courses::get_cols($ma) . '</div>
				<div class="ui tab basic segment" data-tab="third">' . Courses::get_cols($phd) . '</div>
				<div class="ui tab basic segment" data-tab="fourth">' . Courses::get_cols($distance) . '</div>
				<div class="ui tab basic segment" data-tab="fifth">' . Courses::get_cols($open_academy) . '</div>';

			// Put the results in a transient. Expire after 1 hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}

	public static function get_curricula() {

		if (!function_exists("pll_get_post"))
			return false;
  
	  	$cache_do = false;
	  	$cache_name = "eka_curricula_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name);
	  
	  	if (false === ($out = get_transient($cache_name))) {
		
			$posts = get_posts([
			  	'numberposts' => -1,
			  	'orderby'=> 'title', 
			  	'order' => 'ASC',
			  	'post_type' => 'eka_courses'
			]);

			foreach ($posts as $post) { 

				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_related_page_url = "";
				$post_related_page_ID = get_field("related_page_id", $post_id);
				if ($post_related_page_ID) {

					$pid = pll_get_post($post_related_page_ID);
					$post_title = get_the_title($pid);
					$post_related_page_url = get_permalink($pid);
				}
				$post_academic_level_arr = get_field("academic_level", $post_id);
				$post_academic_level = "";
				foreach ($post_academic_level_arr as $l) {

					$post_academic_level .= '<div class="ui mini black button level">' . $l . '</div>';
				}

				$out .= '<div class="column">
							<div class="ui fluid card">
								<a class="content" href="' . $post_related_page_url . '" >
									<div class="header curricula">' . $post_title . '</div>
									<div class="description">' . $post_academic_level . '</div>
								</a>
							</div>
						</div>';
			}

			$out = '<div class="ui stackable doubling four column padded grid">
					<div class="column">
						<div class="ui fluid card">
							<div class="content">
								<div class="header curricula" style="background:#f00 !important;">' 
									. '<h3>' . __('Curricula', 'eka2017')  .'</h3>
								</div>
							</div>
						</div>
					</div>
					' . $out . '</div>';

			// Put the results in a transient. Expire after 1 hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}

	public static function get_course_news () {

		if (!function_exists("pll_get_post"))
			return false;
  
	  	$cache_do = false;
	  	$cache_name = "eka_curricula_news_" . get_locale();
	  	$cache_time = 1 * 5 * 60;
	  
	  	if(!$cache_do)
			delete_transient($cache_name);
	  
	  	if (false === ($out = get_transient($cache_name))) {
		
			$posts = get_posts(['post_type' => 'eka_calendar', 
            					'numberposts' => 6, 
            					'post_status' => 'publish',
            					'meta_key' => 'event_start_date',
            					'orderby' => 'meta_value_num',
            					'order' => 'DESC']);

			foreach ($posts as $post) { 

				$post_id = $post->ID;
				$post_title = $post->post_title;
				$post_link = get_permalink($post_id);

				$out .= '<div class="column">
							<div class="ui fluid card">
								<div class="content">
									<a class="header" href="' . $post_link . '">' . $post_title . '</a>
								</div>
							</div>
						</div>';
			}

			$out = '<div class="ui stackable doubling four column grid">' . $out . '</div>';

			// Put the results in a transient. Expire after 1 hour.
			set_transient($cache_name, $out, $cache_time);
	  	}
	  
	  	return $out;
	}
}
