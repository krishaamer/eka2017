<?php

namespace App;

use Sober\Controller\Controller;

class Taxonomy extends Controller {

	public static function get_category_tags($args) {
		global $wpdb;			
		
		return $wpdb->get_results($wpdb->prepare("
			SELECT DISTINCT terms2.term_id as term_id, terms2.name as name, terms2.slug as slug
			FROM
				$wpdb->posts as p1
				LEFT JOIN $wpdb->term_relationships as r1 ON p1.ID = r1.object_ID
				LEFT JOIN $wpdb->term_taxonomy as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
				LEFT JOIN $wpdb->terms as terms1 ON t1.term_id = terms1.term_id,
	
				$wpdb->posts as p2
				LEFT JOIN $wpdb->term_relationships as r2 ON p2.ID = r2.object_ID
				LEFT JOIN $wpdb->term_taxonomy as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
				LEFT JOIN $wpdb->terms as terms2 ON t2.term_id = terms2.term_id
			WHERE
				t1.taxonomy = '%s' AND p1.post_status = 'publish' AND terms1.term_id IN (".$args['categories'].") AND
				t2.taxonomy = '".$args['term']."' AND p2.post_status = 'publish' AND p2.post_type = '".$args['post_type']."'
				AND p1.ID = p2.ID
			ORDER by name
		", [$args['taxonomy']]));

	}

	public static function current_tag( $current_tag, $selected_tag ) {
		if($selected_tag == $current_tag) {
			return 'current_page_item';
		}

		return false;
	} 
}
