<?php

namespace App;

use Sober\Controller\Controller;

class Index extends Controller {

	/* Cards */
	public static function index () {
  
	  $cache_do = false;
	  $cache_name = "card_index_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		
		$card_title = __("News", "eka2017");
		$card_slug = "/uudised/";
		if(get_locale() == "en_US") {
		  $card_slug = "/news/";
		}

		$args = array(
		  'numberposts' => 4,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		);
		
		$posts = get_posts($args);

		$i = 0;
		$size = count($posts);
		foreach ($posts as $post) {

		  	$i++;
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_featured_thumb = get_the_post_thumbnail($post_id, "standard");
		  	$post_featured_thumb_url = get_the_post_thumbnail_url($post_id, "standard");
		  	$post_excerpt = $post->post_excerpt;
		  	$post_category = get_the_category($post_id)[0]->cat_name;
		  	$post_attached_media = get_attached_media('image', $post_id);
		  	$post_attached_thumb = "";
		  	$post_attached_thumb_container = "";
		  	$post_attached_medium = "";
		  	$post_attached_medium_container = "";
		  	if (!empty($post_attached_media)) {

		  		$post_attached_images = [];
			  	foreach($post_attached_media as $img) { 

			  		array_push($post_attached_images, $img);
			  	}
			  	$post_attached_thumb = wp_get_attachment_image_src($post_attached_images[0]->ID, "square")[0];
			  	$post_attached_thumb_container = '<a href="' . $post_link . '"><img class="right floated tiny ui image" src="' . $post_attached_thumb . '" alt="' . $post_title . '"></a>';

			  	$post_attached_medium = wp_get_attachment_image_src($post_attached_images[0]->ID, "medium")[0];
			  	$post_attached_medium_container = '<a href="' . $post_link . '"><img class="ui image" src="' . $post_attached_medium . '" alt="' . $post_title . '" /></a>';
		  	}

		  	$author_id = $post->post_author;
		  	$author = get_the_author_meta("display_name", $author_id);
		  	$author_link = get_author_posts_url($author_id);
		  	$time = ucfirst(get_the_time('F j, Y', $post_id));
		  	$time_ago = __("Time Ago", "eka2017");
		  	$style = "border-bottom: 2px #fbfbfb solid;";

		  	if ($i == $size) {
		  		$style = "";
		  	}

		  	if ($i == 1) {

		  		$out .= '<div class="ui fluid card" style="' . $style . '">' . 
		  				$post_attached_medium_container .
		  				'<div class="content">
		  					<div class="meta">
						      <span class="category">' . strtoupper($post_category) . '</span>
						    </div>
						    <div class="header">
		  						<a href="' . $post_link . '">' . $post_title . '</a>
		  					</div>
		  				</div>
		  			</div>';

			} else {

				$out .= '<div class="ui fluid card" style="' . $style . '">
						<div class="content">' . 
							$post_attached_thumb_container
							. '<div class="meta">
						      <span class="category">' . strtoupper($post_category) . '</span>
						    </div>
						    <div class="header">
		  						<a href="' . $post_link . '">' . $post_title . '</a>
		  					</div>
		  				</div>
		  			</div>';
			}
		}

		$out = '<h1 class="ui header" style="border-bottom: 4px #000 solid;">' . __("News", "eka2017") . '</h1>' . $out . '<div class="ui bottom attached basic black button">' . __("Read more", "eka2017") . '</div>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}

	

	/* Post Single */
	public static function post_single() {
  
	  $cache_do = false;
	  $cache_name = "card_newssingle_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		
		$args = array(
		  'numberposts' => 1,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		);
		
		$post = get_post($args);
	  	$post_id = $post->ID;
	  	$post_link = get_permalink($post_id);
	  	$post_title = $post->post_title;
	  	$post_featured_thumb = get_the_post_thumbnail($post_id, "standard");
	  	$post_featured_thumb_url = get_the_post_thumbnail_url($post_id, "standard");
	  	$post_excerpt = $post->post_excerpt;
	  	$post_category = get_the_category($post_id)[0]->cat_name;
	  	$post_attached_media = get_attached_media('image', $post_id);
	  	$post_attached_thumb = "";
	  	$post_image_container = "";
	  	if (!empty($post_attached_media)) {

	  		$post_attached_images = [];
		  	foreach($post_attached_media as $img) { 

		  		array_push($post_attached_images, $img);
		  	}
		  	$post_attached_thumb = wp_get_attachment_image_src($post_attached_images[0]->ID, "medium")[0];
		  	$post_image_container = '<a class="header" href="' . $post_link . '">
		  								<div class="ui image">
		  									<img src="' . $post_attached_thumb . '" alt="' . $post_title . '" />
		  								</div>
		  							</a>';
	  	}

	  	$author_id = $post->post_author;
	  	$author = get_the_author_meta("display_name", $author_id);
	  	$author_link = get_author_posts_url($author_id);
	  	$time = ucfirst(get_the_time('F j, Y', $post_id));
	  	$time_ago = "Time Ago";

	  	$out .= '<div class="ui fluid card">' . 
	  				$post_image_container .
	  				'<div class="content">
	  					<a class="header" href="' . $post_link . '">' . $post_title . '</a>
	  					<div class="meta">
					      <span class="category">' . strtoupper($post_category) . '</span>
					    </div>
	  				</div>
	  			</div>';

		$out = '<h1 class="ui header">' . __("News", "eka2017") . '</h1>' 
				. $out 
				. '<div class="ui bottom attached basic black button">' . __("Read more", "eka2017") . '</div>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}
}
