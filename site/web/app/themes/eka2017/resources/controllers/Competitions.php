<?php

namespace App;

use Sober\Controller\Controller;

class Competitions extends Controller {

	public static function cards() {
  
	  $cache_do = false;
	  $cache_name = "card_competitions_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		
		$posts = get_posts([
		  'numberposts' => 6,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		]);

		$i = 0;
	    $size = count($posts);
		foreach ($posts as $post) {
		  
		  	$i++;
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_featured_thumb = get_the_post_thumbnail($post_id, "standard");
		  	$post_featured_thumb_url = get_the_post_thumbnail_url($post_id, "standard");
		  	$post_excerpt = $post->post_excerpt;
		  	$author_id = $post->post_author;
		  	$author = get_the_author_meta("display_name", $author_id);
		  	$author_link = get_author_posts_url($author_id);
		  	$style = "border-bottom: 2px #fbfbfb solid;";

		  	if ($i == $size) {
		  		$style = "";
		  	}

		  	$out .= '<div class="ui fluid card" style="' . $style . '">' .
		  				'<div class="content">
		  					<div class="header">
		  						<a href="' . $post_link . '">' . $post_title . '</a>
		  					</div>
		  				</div>
		  			</div>';
		}

		$card_link = get_permalink(get_option('page_for_posts'));
		$out = '<a href="' . $card_link . '"><h1>' . __("Competitions", "eka2017") . '</h1></a>' 
				. $out . '<div class="ui bottom attached basic black button">' . __("Read more", "eka2017") . '</div>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}
}
