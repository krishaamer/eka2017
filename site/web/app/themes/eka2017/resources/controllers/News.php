<?php

namespace App;

use Sober\Controller\Controller;

class News extends Controller {

	private static function render_card ($role, $props) {

		$out = "";
		$imgs = [];
		$image_id = "";
		$images = $props["images"];
		if (!empty($images)) {
		  	foreach($images as $i) {
		  		array_push($imgs, $i);
		  	}
		  	$image_id = $imgs[0]->ID;
	  	}

		if ($role == "primary") {

			$img = self::render_image([
				"image_id" => $image_id, 
				"size" => "medium_large", 
				"class" => "ui image",
				"title" => $props["title"],
				"link" => $props["link"]
			]);

			$out = '<div class="ui fluid card" style="' . $props["style"] . '">' . 
				$img .
				'<div class="content">
					<div class="meta">
			      <span class="category">' . $props["category"] . '</span>
			    </div>
			    <div class="header">
						<a href="' . $props["link"] . '">' . $props["title"] . '</a>
					</div>
				</div>
			</div>';
		} else {

			$img = self::render_image([
				"image_id" => $image_id, 
				"size" => "thumbnail", 
				"class" => "right floated tiny ui image",
				"title" => $props["title"],
				"link" => $props["link"]
			]);

			$out .= '<div class="ui fluid card" style="' . $props["style"] . '">
			<div class="content">' . 
				$img .
				'<div class="meta">
			      <span class="category">' . $props["category"] . '</span>
			    </div>
			    <div class="header">
						<a href="' . $props["link"] . '">' . $props["title"] . '</a>
					</div>
				</div>
			</div>';
		}

		return $out;
	}

	private static function render_image ($props) {

		$out = "";
		$img = wp_get_attachment_image_url($props["image_id"], $props["size"]);
		if (!$img) {
			$img = "https://unsplash.it/200/200/?random";
		}

	  	$out = '<a href="' . $props["link"] . '">
	  				<img class="' . $props["class"] . '" src="' . $img . '" alt="' . $props["title"] . '">
	  			</a>';

		return $out;
	}

	/* Cards */
	public static function cards () {
  
	  $cache_do = false;
	  $cache_name = "eka_news_cards_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		
		$posts = get_posts([
		  'numberposts' => 4,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		]);

		$i = 0;
		$size = count($posts);
		foreach ($posts as $post) {

		  	$i++;
		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_category = get_the_category($post_id);
		  	$post_category_name = "";
		  	if ($post_category)
		  		$post_category_name = $post_category[0]->cat_name;
		  	$post_attached_media = get_attached_media('image', $post_id);

		  	$post_style = "border-bottom: 2px #fbfbfb solid;";
		  	if ($i == $size)
		  		$style = "";

		  	$props = [
		  		"style" => $post_style,
		  		"images" => $post_attached_media,
		  		"category" => $post_category_name,
		  		"link" => $post_link,
		  		"title" => $post_title
		  	];

		  	if ($i == 1)
		  		$out .= self::render_card("primary", $props);
			else
				$out .= self::render_card("secondary", $props);
		}

		$card_link = get_permalink(get_option('page_for_posts'));
		$out = '<a href="' . $card_link . '"><h1>' . __("News", "eka2017") . '</h1></a>'
				. $out . 
				'<a class="ui bottom attached basic black button" href="' . $card_link . '">' . __("Read more", "eka2017") . '</a>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}

	/* Post Single */
	public static function post_single() {
  
	  $cache_do = false;
	  $cache_name = "eka_newssingle_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {
		
		$args = array(
		  'numberposts' => 1,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		);
		
		$post = get_post($args);
	  	$post_id = $post->ID;
	  	$post_link = get_permalink($post_id);
	  	$post_title = $post->post_title;
	  	$post_featured_thumb = get_the_post_thumbnail($post_id, "standard");
	  	$post_featured_thumb_url = get_the_post_thumbnail_url($post_id, "standard");
	  	$post_excerpt = $post->post_excerpt;
	  	$post_category = get_the_category($post_id)[0]->cat_name;
	  	$post_attached_media = get_attached_media('image', $post_id);
	  	$post_attached_thumb = "";
	  	$post_image_container = "";
	  	if (!empty($post_attached_media)) {

	  		$post_attached_images = [];
		  	foreach($post_attached_media as $img) { 

		  		array_push($post_attached_images, $img);
		  	}
		  	$post_attached_thumb = wp_get_attachment_image_src($post_attached_images[0]->ID, "medium")[0];
		  	$post_image_container = '<a class="header" href="' . $post_link . '">
		  								<div class="ui image">
		  									<img src="' . $post_attached_thumb . '" alt="' . $post_title . '" />
		  								</div>
		  							</a>';
	  	}

	  	$author_id = $post->post_author;
	  	$author = get_the_author_meta("display_name", $author_id);
	  	$author_link = get_author_posts_url($author_id);
	  	$time = ucfirst(get_the_time('F j, Y', $post_id));
	  	$time_ago = "Time Ago";

	  	$out .= '<div class="ui fluid card">' . 
	  				$post_image_container .
	  				'<div class="content">
	  					<a class="header" href="' . $post_link . '">' . $post_title . '</a>
	  					<div class="meta">
					      <span class="category">' . $post_category . '</span>
					    </div>
	  				</div>
	  			</div>';

		$out = '<h1 class="ui header">' . __("News", "eka2017") . '</h1>' 
				. $out 
				. '<div class="ui bottom attached basic black button">' . __("Read more", "eka2017") . '</div>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}

	/* Related Posts */
	public static function related_posts () {
  
	  $cache_do = false;
	  $cache_name = "eka_related_posts_cache_" . get_locale();
	  $cache_time = 1 * 5 * 60;
	  
	  if(!$cache_do) {
		delete_transient($cache_name); 
	  }
	  
	  if (false === ($out = get_transient($cache_name))) {

		$posts = get_posts([
		  'numberposts' => 6,
		  'orderby' => 'post_date',
		  'order' => 'DESC',
		  'post_type'   => 'post',
		  'post_status' => 'publish'
		]);

		$post_tags = "";
		foreach ($posts as $post) {

		  	$post_id = $post->ID;
		  	$post_link = get_permalink($post_id);
		  	$post_title = $post->post_title;
		  	$post_excerpt = $post->post_excerpt;
		  	$post_category = get_the_category($post_id);
		  	$post_category_name = "";
		  	$post_category_link = "";
		  	if ($post_category) {
		  		$post_category_name = $post_category[0]->cat_name;
		  		$post_category_link = $post_category[0]->cat_name;
		  	};
		  	$post_attached_media = get_attached_media('image', $post_id);
		  	$post_attached_medium = "";
		  	$post_attached_medium_container = "";
		  	if (!empty($post_attached_media)) {

		  		$post_attached_images = [];
			  	foreach($post_attached_media as $img) { 

			  		array_push($post_attached_images, $img);
			  	}

			  	$post_attached_medium = wp_get_attachment_image_src($post_attached_images[0]->ID, "medium")[0];
			  	$post_attached_medium_container = '<a href="' . $post_link . '">
					  									<img class="ui image" src="' . $post_attached_medium . '" alt="' . $post_title . '" />
					  								</a>';
		  	}

		  	$author_id = $post->post_author;
		  	$author = get_the_author_meta("display_name", $author_id);
		  	$author_link = get_author_posts_url($author_id);

		  	$post_tags_raw = wp_get_post_tags($post_id);
  			if ($post_tags_raw) {
	  			foreach ($post_tags_raw as $tag) {

	  				$post_tags .= '<a class="ui mini basic black button tag" href="' . get_tag_link($tag->term_id) . '">' . $tag->name . "</a>";
	  			}
	  		}

			$out .= '<div class="ui stackable doubling grid">
						<div class="six wide column">' . $post_attached_medium_container . '</div>
						<div class="ten wide column">
							<div class="category">
								<a href="' . $post_category_link . '">' . $post_category_name . '</a>
							</div>
						    <h2 class="header">
		  						<a href="' . $post_link . '">' . $post_title . '</a>
		  					</h2>
		  					<p>' . $post_excerpt . '</p>' .
		  					$post_tags
		  				.'</div>
					</div>';
		}

		$out = '<div class="ui basic segment"><h1>' . __("About The Same Topic", "eka2017") . '</h1>' . $out . '</div>';

		// Put the results in a transient. Expire after 1 hour.
		set_transient($cache_name, $out, $cache_time);
	  }
	  
	  return $out;
	}

	// News ticker in the header
	public static function eka_news_ticker() {    

		$out = "";
	    $showposts = get_posts([
	        'post_type'     => ['post', 'eka_calendar'],
	        'numberposts'   => 5,
	        'suppress_filters' => 0,
	        'meta_query'    => [['key' => 'show_news_ticker', 'value' => '1']]
	    ]);
	    
        foreach($showposts as $post) {     
            $out .= '<li><a style="color: #fff;" href="' . $post->guid . '">' . $post->post_title . '</a></li>';
        }
	        
	        
	    $out = '<div class="newsticker"><ul id="newsticker">' . $out . '</ul></div>';

	    return $out;
	}
}
