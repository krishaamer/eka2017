<?php

namespace App;

use Sober\Controller\Controller;

class Media extends Controller {

	public static function get_attached_images ($post, $size = "large", $limit = 1, $is_wrapped = true, $is_fluid = false, $extra_classes = '') {

		$out = '';
		$media = get_attached_media('image', $post->ID);

		if ($media) {
			$title = get_permalink($post->ID);
			$count = count($media);
			$wrapper_start = '<div class="column">';
			$wrapper_end = '</div>';
			$class = 'ui';
			$i = 0;

			if (($count > 1 && $limit > 1) OR $is_fluid == true) {
				$class .= " fluid";
			}
			if ($extra_classes) {
				$class .= ' ' . $extra_classes;
			}
			$class .= ' image';

			if ($media) {
				foreach($media as $m) { 

					$i++;
					if ($i <= $limit) {

						$pic =  wp_get_attachment_image($m->ID, $size, false, ['alt' => $title, 'class' => $class]);
						if ($is_wrapped) {
							$out .= $wrapper_start . $pic . $wrapper_end;
						} else {
							$out .= $pic;
						}
					}
				}	
			}

			if ($is_wrapped) {
				$out = '<div class="ui equal width grid">' . $out . '</div>';
			}
		}
		
		return $out;
	}

	public static function eka_load_gallery($post, $args = []) {

		$out = "";
		$args['posts_per_page'] = 10;
		$args['post_type'] = 'eka_project';
		$args['orderby'] = ['menu_order' => 'ASC', 'date' => 'DESC'];

		if (!isset($args['show_on_page'])) {
			$args['show_on_page'] = $post->ID;
		}

		if (isset($_POST['tag'])) {
			$args['tag_id'] = $_POST['tag'];
		} 
		
		if(function_exists('pll_current_language' )){	
			$args['lang'] = pll_current_language();
		}

		$posts = get_posts($args);
		foreach ($posts as $post) {

			$out .= '<a href="' . get_permalink($post->ID) . '">' 
						. $post->post_title . '</a>' 
						. self::get_attached_images($post, "thumbnail", 5, true, false) 
						. '<br />';
		}

		return $out;
	}

	public static function eka_load_gallery_new($post, $args = []) {

		$out = "";
		$args['posts_per_page'] = 30;
		$args['post_type'] = 'eka_project';
		$args['orderby'] = ['menu_order' => 'ASC', 'date' => 'DESC'];

		if (!isset($args['show_on_page'])) {
			$args['show_on_page'] = $post->ID;
		}

		if (isset($_POST['tag'])) {
			$args['tag_id'] = $_POST['tag'];
		} 
		
		if(function_exists('pll_current_language' )){	
			$args['lang'] = pll_current_language();
		}

		$posts = get_posts($args);
		if ($posts) {
			foreach ($posts as $post) {

				$out .= '<div class="column"><a class="ui card" href="' . get_permalink($post->ID) . '"><div class="image">' 
							. self::get_attached_images($post, "thumbnail", 1, false, false) 
							. '</div></a></div>';
			}
			$out = '<div class="ui basic segment">
						<div class="ui container">
							<nav class="ui stackable secondary fluid menu" id="student-work-menu">
								<a class="item" target="_blank" href="/">
									<h1>' . __('Student Work', 'eka2017') . '</h1>
								</a>
							  	<div class="right item">
							  		<a class="ui basic black button" href="/">' . __("View all", "eka2017") . '</a>
							  	</div>
							</nav>
						</div>
						<div class="ui three column masonry grid">' . $out . '</div>
					</div>';
		}

		return $out;
	}

	public static function get_course_gallery ($post) {

		global $wp_query;
		$out = "";

		if (get_post_meta($post->ID, 'eka_disallow_tagging', true) != true) {

			if($wp_query->query_vars['tag'] != '') {

				$selected_tag = $wp_query->query_vars['tag'];
				$tagObj = get_term_by('slug', $selected_tag, 'post_tag');
				$args['tag_id'] = $tagObj->term_id;
				$selected_tag = $tagObj->slug;

			} else {

				$selected_tag = '';
				$args['tag_id'] = '';
			}
			
			$this_page = term_exists((string)$post->ID, 'show_on_page');
			if ($this_page) {

				$tags = Taxonomy::get_category_tags([ 
					'categories' 	=> $this_page['term_id'], 
					'taxonomy' 		=> 'show_on_page',
					'term' 			=> 'post_tag',
					'post_type' 	=> 'eka_project' 
				]);

				if (!empty($tags)) {

					$out .= '<li class="tagoption' . Taxonomy::current_tag('all', $selected_tag) . '"><a href="' . get_permalink() . '?tag=">' . __('All', 'eka2017') . '</a></li>';
				}
							
				foreach ($tags as $tag) {
					$out .= '<li class="tagoption' . Taxonomy::current_tag($tag->slug, $selected_tag) . '">
								<a href="' . get_permalink() . '?tag=' . $tag->slug . '">' . $tag->name . '</a></li>';
				}

				if (count($tags) >= 10) {
					$out .= '<li>' . __('more tags...', 'ela2017') . '</li>';
				}

				$out .= Media::eka_load_gallery($post, $args);
			}
		}

		return $out;
	}

	public static function get_previews ($children) {

		$out = "";
	    if ($children) {
	      	foreach ($children as $child) {

		        $title = $child->post_title;
		        if (!$title)
		          $title = $child->post_name;

		        $media = get_attached_media('image', $child->ID);

		        if ($media) {

		          $images = [];
		          foreach($media as $m) { 
		          	array_push($images, $m); 
		          }

		          $out .= '<div class="column">
		          			<div class="ui segment">
		          			<a class="ui fluid card" href="' . get_permalink($child->ID) . '">
		          				<div class="image">
		                  			<img src="' . wp_get_attachment_image_src($images[0]->ID, "medium_large")[0] . '" alt="' . $title . '">
		                  		</div>
		                  		<div class="content">
		                  			<div class="header">' . $title . '</div>
		                  		</div>
		                	</a>
		                	</div>
		                </div>';
		        } else {

		        	$out .= '<div class="column">
		        				<div class="ui segment">
				          			<a class="ui fluid card" href="' . get_permalink($child->ID) . '">
				                  		<div class="content">
				                  			<div class="header">' . $title . '</div>
				                  		</div>
				                	</a>
				                </div>
		                	</div>';
		        }
	      	}
	    }

	    $out = '<div class="ui doubling stackable three column masonry grid">' . $out . '</div>';

	    return $out;
	}
}
