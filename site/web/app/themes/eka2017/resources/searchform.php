<form class="ui form" method="get" id="searchform" action="<?= esc_url(home_url('/')) ?>">
	<div class="ui icon input">
		<input type="text" class="field" name="s" id="s" placeholder="<? _e("Search") ?>" />
		<i class="search link icon"></i>
	</div>
</form>