<?php

namespace App;

use Roots\Soil\Utils;

/**
 * Eka customizations
 */

add_action('admin_init', function () {
    register_taxonomy_for_object_type('category','page');
});

add_action( 'after_setup_theme', function () {
   
    add_image_size('medium_large', 768, 0, false); 
    add_image_size('wide', 1440, 600, true);

    if (function_exists('fly_add_image_size')) {
        fly_add_image_size('tiny_square', 100, 100, true);
    }
});

add_action('init', function () {

    register_taxonomy_for_object_type('category', 'attachment');
    register_post_type('eka_videos',
        ['labels' => [
                'name' => __('Videos', 'eka2017'),
                'singular_name' => __('Video', 'eka2017'),
                'add_new' => _x('Lisa uus', 'video'),
                'add_new_item' => __('Lisa uus video', 'eka2017'),
                'edit_item' => __('Muuda video', 'eka2017'),
                'new_item' => __('Uus video', 'eka2017'),
                'all_items' => __('Kõik videod', 'eka2017'),
                'view_item' => __('Vaata videoid', 'eka2017'),
                'search_items' => __('Otsi videoid', 'eka2017'),
                'not_found' =>  __('Videoid ei leitud', 'eka2017')
            ],
            'public' => true,
            'menu_position' => 7,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => ['title', 'editor', 'custom-fields'],
            'has_archive' => false,
            'show_in_admin_bar' => false,
            'taxonomies' => [],
            'rewrite' => ['slug' => 'videos']
        ]
    );

    register_post_type('eka_courses',
        ['labels' => [
                'name' => __( 'Courses' ),
                'singular_name' => __( 'Course' ),
                'add_new' => _x('Lisa uus', 'course'),
                'add_new_item' => __('Lisa uus course'),
                'edit_item' => __('Muuda course'),
                'new_item' => __('Uus course'),
                'all_items' => __('Kõik courses'),
                'view_item' => __('Vaata courses'),
                'search_items' => __('Otsi courses'),
                'not_found' =>  __('Courses ei leitud')
            ],
            'public' => true,
            'menu_position' => 7,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => ['title', 'editor', 'custom-fields'],
            'has_archive' => false,
            'show_in_admin_bar' => false,
            'taxonomies' => [],
            'rewrite' => ['slug' => 'course']
        ]
    );

    register_post_type('eka_open',
        ['labels' => [
                'name' => __( 'Open Academy', "eka2017"),
                'singular_name' => __( 'Open Academy', "eka2017"),
                'add_new' => _x('Add new', 'course', "eka2017"),
                'add_new_item' => __('Add new open course', "eka2017"),
                'edit_item' => __('Edit course', "eka2017"),
                'new_item' => __('New course', "eka2017"),
                'all_items' => __('All open courses', "eka2017"),
                'view_item' => __('View open courses', "eka2017"),
                'search_items' => __('Find open courses', "eka2017"),
                'not_found' =>  __('No open courses found', "eka2017")
            ],
            'public' => true,
            'menu_position' => 7,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => ['title', 'editor', 'custom-fields'],
            'has_archive' => false,
            'show_in_admin_bar' => false,
            'taxonomies' => [],
            'rewrite' => ['slug' => 'open-academy']
        ]
    );

    register_post_type('eka_testimonials',
        ['labels' => [
                'name' => __('Testimonials', "eka2017"),
                'singular_name' => __('Testimonial', 'eka2017'),
                'add_new' => __('Add new testimonial', 'eka2017'),
                'add_new_item' => __('Add new testimonial', 'eka2017'),
                'edit_item' => __('Edit testimonial', 'eka2017'),
                'new_item' => __('New testimonial', 'eka2017'),
                'all_items' => __('All testimonials', 'eka2017'),
                'view_item' => __('View testimonial', 'eka2017'),
                'search_items' => __('Find testimonials', 'eka2017'),
                'not_found' =>  __('No testimonials found', 'eka2017')
            ],
            'public' => true,
            'menu_position' => 7,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => ['title', 'editor', 'custom-fields'],
            'has_archive' => false,
            'show_in_admin_bar' => false,
            'taxonomies' => [],
            'rewrite' => ['slug' => 'open-academy']
        ]
    );

    register_post_type('eka_calendar',
        array(
            'labels' => array(
                'name' => __( 'Kalender' ),
                'singular_name' => __('Sündmus', 'eka2017'),
                'add_new' => _x('Lisa uus', 'sündmus'),
                'add_new_item' => __('Lisa uus sündmus', 'eka2017'),
                'edit_item' => __('Muuda sündmust', 'eka2017'),
                'new_item' => __('Uus sündmus', 'eka2017'),
                'all_items' => __('Kõik sündmused', 'eka2017'),
                'view_item' => __('Vaata sündmust', 'eka2017'),
                'search_items' => __('Otsi sündmust', 'eka2017'),
                'not_found' =>  __('Sündmusi ei leitud', 'eka2017')
            ),
            'public' => true,
            'menu_position' => 5,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'trackbacks', 'custom-fields', 'thumbnail', 'comments'),
            'has_archive' => true,
            'show_in_admin_bar' => false,
            'taxonomies' => array('category', 'calendar_tag'),
            'rewrite' => array('slug' => 'kalender')
        )
    );

    register_post_type('eka_contacts',
        array(
            'labels' => array(
                'name' => __( 'Kontaktid' ),
                'singular_name' => __( 'Kontakt', 'eka2017'),
                'add_new' => _x('Lisa uus', 'kontakt'),
                'add_new_item' => __('Lisa uus kontakt', 'eka2017'),
                'edit_item' => __('Muuda kontakti', 'eka2017'),
                'new_item' => __('Uus kontakt', 'eka2017'),
                'all_items' => __('Kõik kontaktid', 'eka2017'),
                'view_item' => __('Vaata kontakto', 'eka2017'),
                'search_items' => __('Otsi kontakti', 'eka2017'),
                'not_found' =>  __('Kontakte ei leitud', 'eka2017')
            ),
            'public' => true,
            'menu_position' => 20,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'thumbnail', 'custom-fields','page-attributes'),
            'has_archive' => false,
            'show_in_admin_bar' => false,
            'taxonomies' => array('category', 'post_tag'),
            'rewrite' => array('slug' => 'inimesed')
        )
    );

    register_post_type('eka_project',
        array(
            'labels' => array(
                'name' => __( 'Galeriid' ),
                'singular_name' => __( 'Galerii', 'eka2017'),
                'add_new' => _x('Lisa uus', 'galerii', 'eka2017'),
                'add_new_item' => __('Lisa uus galerii', 'eka2017'),
                'edit_item' => __('Muuda galeriid', 'eka2017'),
                'new_item' => __('Uus galerii', 'eka2017'),
                'all_items' => __('Kõik galeriid', 'eka2017'),
                'view_item' => __('Vaata galeriid', 'eka2017'),
                'search_items' => __('Otsi galeriid', 'eka2017'),
                'not_found' =>  __('Galeriisid ei leitud', 'eka2017')
            ),
            'public' => true,
            'menu_position' => 5,
            'hierarchical' => false,
            'capability_type' => 'post',
            'supports' => array('title', 'editor', 'custom-fields'),
            'has_archive' => true,
            'show_in_admin_bar' => false,
            'taxonomies' => array('category', 'post_tag', 'show_on_page'),
            'rewrite' => array('slug' => 'galerii')
        )
    );

    register_post_type( 'eka_xpages',
        array(
            'labels' => array(
                'name' => __('Erilehed', 'eka2017'),
                'singular_name' => __( 'Erileht', 'eka2017'),
                'add_new' => _x('Lisa uus', 'erileht'),
                'add_new_item' => __('Lisa uus erileht', 'eka2017'),
                'edit_item' => __('Muuda erilehte', 'eka2017'),
                'new_item' => __('Uus erileht', 'eka2017'),
                'all_items' => __('Kõik erilehed', 'eka2017'),
                'view_item' => __('Vaata erilehte', 'eka2017'),
                'search_items' => __('Otsi erilehti', 'eka2017'),
                'not_found' =>  __('Erilehti ei leitud', 'eka2017')
            ),
            'description' => 'EKA veebi lehed, mida kuvatakse ilma päiseta',
            'public' => true,
            'menu_position' => 20,
            'hierarchical' => true,
            'capability_type' => 'page',
            'supports' => array('title', 'excerpt', 'editor', 'custom-fields', 'page-attributes', 'revisions'),
            'has_archive' => true,
            'show_in_admin_bar' => false,
            'taxonomies' => array('category'),
            'rewrite' => array('slug' => 'x', 'with_front' => true)
        )
    );
});

/*  Register widget areas */

if (function_exists('register_sidebar')){

    register_sidebar(['name'=>'Social Footer','id'=>'social-footer']);
    register_sidebar(['name'=>'Esileht EST','id'=>'sidebar-1']);
    register_sidebar(['name'=>'Esileht ENG','id'=>'sidebar-2']);
    register_sidebar(['name'=>'Jalus EST','id'=>'sidebar-3']);
    register_sidebar(['name'=>'Jalus ENG','id'=>'sidebar-4']);
}

add_filter('wp_nav_menu_items', function ($items, $args) {

    //var_dump($items);

    if($args->theme_location == 'primary_navigation'){
        $items .= '<div class="item">' . get_search_form( false ) . '</div>';
    }
    
    return $items;

}, 10, 2);

/* ==============================================================================================================

Add categories to attachments (media), add parent or user category on save

============================================================================================================== */ 

// Plug into the attachment save hook
// Do the category adding magic
// eka_set_attachment_categories
add_action('add_attachment', function ($attachment_ID) {

    // Check if the post has a parent, get its id and categories if yes
    $parents = get_post_ancestors( $attachment_ID );
    if ( $parents ) {
        
        $postcats = get_the_category( $parents[0] );

        if ( $postcats ) {
            foreach( $postcats as $cat ) { 
                $category_ids[] = $cat->cat_ID; 
            }
        }

    // If theres no parent category for some reason, try to get the user's instead
    } else {
        $category_ids = get_the_author_meta( 'user_categories', get_current_user_id() );
    }

    // If we finally have an id, set it to our beloved media!
    if ( $category_ids )
        wp_set_post_terms( $attachment_ID, $category_ids, 'category' ); 
    
    return;
});

// Add meta boxes to admin area. Uses the Meta Box plugin.

add_filter('rwmb_meta_boxes', function ($meta_boxes) {
    $meta_boxes[] = array(
            'title'      => __( 'Postituse avaldamine',
                'artun2012' ),
            'post_types' => array( 'post', 'eka_calendar' ),
            'context'    => 'side',
            'fields'     => array(
                array(
                    'id'   => 'show_news_ticker',
                    'name' => __( 'Lisa postitus uudisteribale', 'artun2012' ),
                    'type' => 'checkbox'
                ),
                array(
                    'id'   => 'show_front_page',
                    'name' => __( 'Lisa postitus bännerina esilehele', 'artun2012' ),
                    'type' => 'checkbox'
                ),
                array(
                    'id'   => 'show_front_page_enddate',
                    'name' => __( 'Bänneri kestvus', 'artun2012' ),
                    'type' => 'date',
                    'js_options' => array(
                        'dateFormat'      => 'yymmdd'
                    ),
                ),
                array(
                    'id'   => 'show_front_page_url',
                    'name' => __( 'Bänneri link', 'artun2012' ),
                    'type' => 'url'
                )
            )
        );

    $meta_boxes[] = array(
            'title'      => __( 'Sündmuse detailid',
                'artun2012' ),
            'post_types' => 'eka_calendar',
            'fields'     => array(
                array(
                    'id'   => 'event_start_date',
                    'name' => __( 'Alguse kuupäev', 'artun2012' ),
                    'type' => 'date',
                    'js_options' => array(
                        'dateFormat'      => 'yymmdd'
                    ),
                ),
                array(
                    'id'   => 'event_start_time',
                    'name' => __( 'Kellaaeg', 'artun2012' ),
                    'type' => 'text'
                ),
                array(
                    'id'   => 'event_end_date',
                    'name' => __( 'Lõpu kuupäev', 'artun2012' ),
                    'type' => 'date',
                    'js_options' => array(
                        'dateFormat'      => 'yymmdd'
                    ),
                ),
                array(
                    'id'   => 'event_location',
                    'name' => __( 'Asukoht', 'artun2012' ),
                    'type' => 'text'
                )
            )
        );

    $meta_boxes[] = array(
            'title'      => __( 'Kitsas tulp',
                'artun2012' ),
            'post_types' => array( 'page', 'eka_xpages' ),
            'fields'     => array(
                array(
                    'id'   => 'column_right',
                    'name' => __( 'Kitsa tulba sisu', 'artun2012' ),
                    'type' => 'wysiwyg'
                )
            )
        );

    $meta_boxes[] = array(
            'title'      => __( 'Kontakti Detailid',
                'artun2012' ),
            'post_types' => 'eka_contacts',
            'fields'     => array(
                array(
                    'id'   => 'contact_email',
                    'name' => __( 'E-posti aadress', 'artun2012' ),
                    'type' => 'email'
                ),
                array(
                    'id'   => 'contact_phone',
                    'name' => __( 'Telefon', 'artun2012' ),
                    'type' => 'text'
                ),
                array(
                    'id'   => 'contact_mobile',
                    'name' => __( 'Mobiil', 'artun2012' ),
                    'type' => 'text'
                ),
                array(
                    'id'   => 'contact_role',
                    'name' => __( 'Amet', 'artun2012' ),
                    'type' => 'text'
                ),
                array(
                    'id'   => 'contact_role_en',
                    'name' => __( 'Amet (inglise keeles)', 'artun2012' ),
                    'type' => 'text'
                ),
                array(
                    'id'   => 'contact_www',
                    'name' => __( 'Veebileht', 'artun2012' ),
                    'type' => 'url'
                ),
                array(
                    'id'   => 'contact_cv',
                    'name' => __( 'CV', 'artun2012' ),
                    'type' => 'url'
                ),
                array(
                    'id'   => 'divider',
                    'name' => '',
                    'type' => 'divider'
                ),
                array(
                    'id'   => 'contact_list',
                    'name' => __( 'Näita kontaktilehel', 'artun2012' ),
                    'type' => 'checkbox'
                )
            )
        );

    $meta_boxes[] = array(
            'title'      => 'Lehe seaded',
            'post_types' => array( 'page', 'eka_xpages' ),
            'fields'     => array(
                array(
                    'id'   => 'eka_disallow_tagging',
                    'name' => __( 'Eemalda märksõnad', 'artun2012' ),
                    'type' => 'checkbox',
                    'desc' => 'Ära kasuta selle lehe postitustel täägide lisamise funktsiooni'
                ),
                array(
                    'id'   => 'eka_disallow_gallery',
                    'name' => __( 'Eemalda galerii', 'artun2012' ),
                    'type' => 'checkbox',
                    'desc' => 'Ära kuva lehele lisatud pilte galeriina'
                )
            )
        );


    return $meta_boxes;
});

class NavWalker extends \Walker_Nav_Menu {
  private $cpt; // Boolean, is current post a custom post type
  private $archive; // Stores the archive page for current URL

  public function __construct() {
    add_filter('nav_menu_css_class', array($this, 'cssClasses'), 10, 2);
    add_filter('nav_menu_item_id', '__return_null');
    $cpt           = get_post_type();
    $this->cpt     = in_array($cpt, get_post_types(array('_builtin' => false)));
    $this->archive = get_post_type_archive_link($cpt);
  }

  public function checkCurrent($classes) {
    return preg_match('/(current[-_])|active/', $classes);
  }

  // @codingStandardsIgnoreStart
  public function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
    $element->is_subitem = ((!empty($children_elements[$element->ID]) && (($depth + 1) < $max_depth || ($max_depth === 0))));

    if ($element->is_subitem) {
      foreach ($children_elements[$element->ID] as $child) {
        if ($child->current_item_parent || Utils\url_compare($this->archive, $child->url)) {
          $element->classes[] = 'active';
        }
      }
    }

    $element->is_active = (!empty($element->url) && strpos($this->archive, $element->url));

    if ($element->is_active) {
      $element->classes[] = 'active';
    }

    parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
  }
  // @codingStandardsIgnoreEnd

    // @codingStandardsIgnoreStart
    public function start_el(&$output, $item, $depth = 0, $args = [], $id = 0)
    {
        $classes = empty($item->classes) ? array () : (array) $item->classes;
        $class_names = join(' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        !empty ( $class_names ) and $class_names = ' class="'. esc_attr( $class_names ) . '"';
        $output .= "";
        $attributes  = '';
        !empty( $item->attr_title ) and $attributes .= ' title="'  . esc_attr( $item->attr_title ) .'"';
        !empty( $item->target ) and $attributes .= ' target="' . esc_attr( $item->target     ) .'"';
        !empty( $item->xfn ) and $attributes .= ' rel="'    . esc_attr( $item->xfn        ) .'"';
        !empty( $item->url ) and $attributes .= ' href="'   . esc_attr( $item->url        ) .'"';
        $title = apply_filters( 'the_title', $item->title, $item->ID );
        $item_output = $args->before
        . "<a $attributes $class_names>"
        . $args->link_before
        . $title
        . '</a>'
        . $args->link_after
        . $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    // @codingStandardsIgnoreEnd

  public function cssClasses($classes, $item) {
    $slug = sanitize_title($item->title);

    // Fix core `active` behavior for custom post types
    if ($this->cpt) {
      $classes = str_replace('current_page_parent', '', $classes);

      if ($this->archive) {
        if (Utils\url_compare($this->archive, $item->url)) {
          $classes[] = 'active';
        }
      }
    }

    // Remove most core classes
    $classes = preg_replace('/(current(-menu-|[-_]page[-_])(item|parent|ancestor))/', 'active', $classes);
    $classes = preg_replace('/^((menu|page)[-_\w+]+)+/', '', $classes);

    // Re-add core `menu-item` class
    $classes[] = 'item';

    // Re-add core `menu-item-has-children` class on parent elements
    if ($item->is_subitem) {
      // $classes[] = 'menu-item-has-children';
    }

    // Add `menu-<slug>` class
    $classes[] = 'menu-' . $slug;

    $classes = array_unique($classes);
    $classes = array_map('trim', $classes);

    return array_filter($classes);
  }
}

/**
 * Clean up wp_nav_menu_args
 *
 * Remove the container
 * Remove the id="" on nav menu items
 */
function nav_menu_args($args = '') {
  $nav_menu_args = [];
  $nav_menu_args['container'] = false;

  /*
  if (!$args['items_wrap']) {
    $nav_menu_args['items_wrap'] = '<div class="%2$s">%3$s</div>';
  }
  */
  $nav_menu_args['items_wrap'] = '%3$s';

  if (!$args['walker']) {
    $nav_menu_args['walker'] = new NavWalker();
  }

  return array_merge($args, $nav_menu_args);
}
add_filter('wp_nav_menu_args', __NAMESPACE__ . '\\nav_menu_args');
add_filter('nav_menu_item_id', '__return_null');

function create_newsticker_feed() {
    load_template( TEMPLATEPATH . '/feeds/newsticker-feed.php'); // You'll create a your-custom-feed.php file in your theme's directory
}
    
add_action('do_feed_newsticker', 'create_newsticker_feed', 10, 1); // Make sure to have 'do_feed_customfeed'

