<?php
/**
 * Template Name: Kalender
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php 
if ( ICL_LANGUAGE_CODE == 'et') {
	setlocale( LC_ALL, "et_EE" ); 
}
?>

<?php
	$selected_category = '';

	// If we are on a calendar category view page
	if ( isset( $wp_query->query_vars['rubriik'])) {	
		// Gets and sets the selected category slug
		$selected_category = urldecode( $wp_query->query_vars['rubriik'] );
		// Gets this category item
		$catObj = get_term_by( 'slug', $selected_category, 'calendar_category' ); 
		// Sets the page title
		$title = __('Kalender', 'artun2012');
		// Gets and sets the descriptive text
		$description = term_description( $catObj->term_id, 'calendar_category' );
		// Gets all the tags in this category (custom function in functions.php)
		$tags = get_category_tags( array( 
			'categories' 	=> $catObj->term_id, 
			'taxonomy' 		=> 'calendar_category',
			'term' 			=> 'calendar_tag' 
		) );
		$noevents = __('Rubriigis tulevaid sündmuseid pole.', 'artun2012');
	// If we're viewing a calendar for a special department
	} else if ( isset( $wp_query->query_vars['dept_page'] ) ) {  
		// Gets and sets the selected category slug
		$selected_dept = urldecode( $wp_query->query_vars['dept_page'] );
		// Gets this category item
		$deptObj = get_term_by( 'slug', $selected_dept, 'category' ); 
		// Sets the page title
		$title = __('Kalender: ', 'artun2012') . $deptObj->name;
		// Gets and sets the descriptive text
		$description = '';
		// Gets all the tags in this category (custom function in functions.php)
		$tags = get_category_tags( array( 
			'categories'    => $deptObj->term_id, 
			'taxonomy'      => 'category',
			'term'          => 'calendar_tag' 
		) );
		$noevents = __('Osakonnal tulevaid sündmuseid pole.', 'artun2012');
	// If we are on a regular calendar page
	} else {
		$title = __('Kalender', 'artun2012');	
		$description = '';
		$tags = get_terms( 'calendar_tag', array(
			'orderby'    => 'name',
			'hide_empty' => 0
		) );
		$noevents = __('Tulevaid sündmuseid pole.', 'artun2012');
	}

	// If we are browsing for a special date or past events
	if ( isset( $wp_query->query_vars['aeg']) ) {
		$seldate = urldecode( $wp_query->query_vars['aeg'] );
		
		// For past events
		if ( $seldate == 'toimunud' ) {
			$noevents = __('Toimunud sündmuseid pole.', 'artun2012');
			unset($seldate);
			$viewpast = 1;
		// For selected date
		} else {
			$title = __('Kalender: ', 'artun2012') . utf8_encode(strftime('%e. %B %Y', strtotime( $seldate )));
			$noevents = __('Sel kuupäeval sündmuseid pole.', 'artun2012');
		}        
	}
	
	// If a tag is set
	if( $wp_query->query_vars['tag'] != '' ) {
		$selected_tag = $wp_query->query_vars['tag'];
		
		// Gets this tag item
		$tagObj = get_term_by( 'slug', $selected_tag, 'calendar_tag' );
		
		// Update the title
		$title = $title . ': ' . $tagObj->name;
		
		// Override the descriptive text
		$description = term_description( $tagObj->term_id, 'calendar_tag' );

		// Update the $selected_tag variable, for security reasons
		$selected_tag = $tagObj->slug;
	}

?>




<div class="pageheader">
	<h2><?php echo $title; ?></h2>
	<ul class="pagenav clearfix" id="pagenav">
		<li class="<?php echo ( !isset($selected_category) ? 'current_page_item' : '' ) ?>">
			<a href="<?php echo get_permalink(); ?>"><?php echo __('Kõik sündmused', 'artun2012'); ?></a>
		</li>
		<?php
			function cal_current_page_item( $current_page ) {
				global $selected_category;
						
				if( $selected_category == $current_page ) {
					echo 'current_page_item';
				}
			}                
			
			// Gets the list of all calendar categories        
			$categories = get_categories( array(
				'hide_empty'    => 0,
				'taxonomy'		=> 'calendar_category'
			) );
			
			foreach( $categories as $category ) : ?>
					
				<li class="<?php cal_current_page_item( $category->slug ) ?>">
					<a href="<?php echo get_permalink(); ?><?php echo $category->slug; ?>"><?php echo $category->name; ?></a>
				</li>
						
			<?php endforeach; ?>
	</ul><!--/#pagenav-->
</div><!--/.pageheader-->

<div class="content calendarcontent">
	
	<div class="column_left_calendar float">
	
		<aside>
			<div id="calendar_datepicker" class="calendar_datepicker"></div>
		</aside>
		<aside>
			<p <?php echo ( isset($viewpast) ) ? 'class="current_page_item"' : ''; ?> style="margin-bottom: 1em;"><a href="<?php echo get_permalink() . $selected_category; ?>?aeg=toimunud"><?php echo __('TOIMUNUD SÜNDMUSED', 'artun2012'); ?></a></p>
		</aside>
		<aside>
			<ul class="tagslist">
			<?php
			function current_tag( $current_tag ) {
				global $selected_tag;
						
				if( $selected_tag == $current_tag ) {
					echo 'current_page_item';
				}
			} 
			
			foreach ( $tags as $tag ) : ?>
				<li class="<?php current_tag( $tag->slug ) ?>"><a href="<?php echo get_permalink() . $selected_category; ?>?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a></li>
			<?php endforeach; ?>
			</ul><!--/.taglist-->
		</aside>
 
	</div><!--/.column_left_calendar-->
	<div class="column_right_calendar float" id="eka_posts_container">
 
	<div class="column_left">
		<?php echo $description; ?>
		<?php eka_add_event_form(); ?>
	</div>
	
	<?php
		global $post;
		$tmp_post = $post;
	
		// Get the current date
		list( $today_year, $today_month, $today_day, $hour, $minute, $second ) = preg_split( '([^0-9])', current_time('mysql') );
		$current_date = $today_year . $today_month . $today_day;
		$today = $current_date;
		
		// Update the date variable if a new one is specified
		if( isset( $seldate )) {
			$current_date = $seldate;
		} 

		// We have to do two separate queries to get events due to wordpress get_posts restrictions, might want to create a custom query in the future
		
		$todayargs = array( 
			'numberposts' 	=> -1,
			'post_type' 	=> 'eka_calendar',
			'orderby'       => 'title',
			'order'         => 'ASC',
			'suppress_filters' => 0,
			'meta_query' 	=> array(
				'relation' => 'AND',
				array(
					'key' => 'event_start_date',
					'value' => $current_date,
					'compare' => '<'
				),
				array(
					'key' => 'event_end_date',
					'value' => $current_date,
					'compare' => '>='
				)
			)
		);
		
		$futureargs = array( 
			'numberposts' 	=> -1,
			'post_type' 	=> 'eka_calendar',
			'meta_key'  	=> 'event_start_date',
			'orderby'       => 'meta_value_num',
			'order'         => 'ASC',
			'suppress_filters' => 0,
			'meta_query' 	=> array(
				array(
					'key' => 'event_start_date',
					'value' => $current_date,
					'compare' => '>='
				)
			)
		);
		
		if( isset( $seldate ) ) {
			$futureargs['meta_query'] = array(
				array(
					'key' => 'event_start_date',
					'value' => $current_date,
					'compare' => 'IS'
				)
			);
		} 
	
		if( isset( $wp_query->query_vars['rubriik']) ) {
			$todayargs['calendar_category'] = $selected_category;
			$futureargs['calendar_category'] = $selected_category;
		} else if ( isset( $wp_query->query_vars['dept_page'] ) ) {
			$todayargs['category'] = $deptObj->term_id;
			$futureargs['category'] = $deptObj->term_id;
		}
		
		if ( isset( $selected_tag ) ) {
						
			$todayargs['tax_query'] = array(
				array(
					'taxonomy' 	=> 'calendar_tag',
					'field' 	=> 'slug',
					'terms' 	=> $selected_tag
				)
			);
			$futureargs['tax_query'] = array(
				array(
					'taxonomy' 	=> 'calendar_tag',
					'field' 	=> 'slug',
					'terms' 	=> $selected_tag
				)
			);			
			
		}

		// Display past events
		if ( isset( $viewpast ) ) {

			$futureargs['order'] = 'DESC';
			$futureargs['meta_query'] = array(
				'relation' => 'AND',
				array(
					'key' => 'event_start_date',
					'value' => $current_date,
					'compare' => '<'
				),
				array(
					'key' => 'event_end_date',
					'value' => $current_date,
					'compare' => '<'
				)
			);

			$allposts = get_posts( $futureargs );

		// Display today's and upcoming events
		} else {
			$allposts = array_merge( get_posts( $todayargs ), get_posts( $futureargs ) );
		}
		
		the_post();

		foreach( $allposts as $post ) : setup_postdata( $post ); ?>
			<?php
				$start_date = get_post_meta($post->ID, 'event_start_date', true);
				$end_date = get_post_meta($post->ID, 'event_end_date', true);
				$start_time = get_post_meta($post->ID, 'event_start_time', true);
				$location = get_post_meta($post->ID, 'event_location', true);
				
				if ( ( $start_date == $today ) || ( $start_date <= $today && $end_date >= $today ) ) {
					$todayevent = '<span class="calendar_today">' . __('Hetkel', 'artun2012') . '</span> ';	
				} else {
					$todayevent = '';
				}
				
				if ( $end_date == 0 ) {
					$show_dates = utf8_encode(strftime('%e. %B %Y', strtotime($start_date)));
				} else {
					$show_dates = utf8_encode(strftime('%e. %B %Y', strtotime($start_date))) . ' — <br class="breakable">' .utf8_encode(strftime('%e. %B %Y', strtotime($end_date)));
				};
				
				if ( $start_time != 0 ) {
					$show_time = ' <br class="breakable">' . __('Kell', 'artun2012') . ' ' . $start_time;
				} else {
					$show_time = '';
				}
					
			?>
		
			<article class="clearfix">
				<div class="calendar_list_left float">
					<span class="dates"><?php echo $show_dates; ?><?php echo $show_time; ?></span>
					<br /><?php echo $location; ?>
				</div><!--/.calendar_list_left-->
				<div class="calendar_list_right float">
					<h3>
						<?php echo $todayevent; ?>
						<span class="articletitle" data-post="<?php echo $post->ID; ?>"><?php the_title(); ?></span>
						<span class="commentcount"><?php comments_popup_link('', '(1)', '(%)'); ?></span>
					</h3>
					<div class="articlecontent">
						<div id="gallery-<?php echo $post->ID; ?>" class="postrollgallery clearfix"></div>
						<?php the_content(); ?>

						<div class="details">
							<a href="<?php the_permalink(); ?>"><?php echo __('Püsilink', 'artun2012'); ?></a>
							<a href="<?php the_permalink(); ?>"><?php echo __('Lisa kommentaar', 'artun2012'); ?></a>
							<?php if ( eka_current_user_has_rights() ) : ?>
								<div class="adminpost">
									<?php echo getPostViews(get_the_ID()); ?> <?php echo wp_delete_post_link(); ?>
								 </div><!--/.adminpost-->
							<?php endif; ?>
							<?php eka_share( urlencode( get_permalink() ), $post->post_title ); ?>
						</div><!--/.details-->

					</div>
				</div>
			</article>
			
			<?php unset($start_date, $end_date, $start_time, $location, $show_dates, $show_time); ?>
			<?php setPostViews(get_the_ID()); ?>
			
		<?php 
			endforeach;
			wp_reset_postdata(); 
		?>
		
		<?php if (empty($allposts)) : ?>
		<section>
			<?php echo $noevents; ?>
		</section>
		<?php endif; ?>

		<section style="padding-top: 1em;">
			<a href="<?php echo get_permalink() . $selected_category; ?>?aeg=toimunud"><?php echo __('Vaata juba toimunud sündmusi', 'artun2012'); ?></a>
		</section>
		
		<?php $post = $tmp_post; ?> 
		
	</div><!--/.column_right_calendar-->
	<div class="clear"></div>
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>