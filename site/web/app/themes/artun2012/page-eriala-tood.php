<?php
/**
 * Template Name: Eriala, tööd
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title(); ?></h2>
		<?php eka_page_menu(); ?>
	</div><!--/.pageheader-->

	<?php setlocale( LC_ALL, "et_EE" ); ?>

	<div class="content clearfix" id="content">
		<div class="column_left float">
			<h3><?php the_title(); ?></h3>

			<?php
				// Calls the add project form
				eka_add_project_form();
			?>

			<?php the_content(); ?>

			<?php eka_the_posts(); ?>
		</div><!--/.column_left-->

		
<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>