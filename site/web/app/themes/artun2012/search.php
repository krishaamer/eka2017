<?php
/**
 * Search results page
 * 
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php setlocale(LC_ALL, "et_EE"); ?>

<?php if ( have_posts() ): ?>
	<div class="pageheader clearfix">
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<input type="text" class="bigsearchbox" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo __( 'Otsi nime, eriala, dokumenti, ...', 'artun2012' ); ?>" />
			<input type="submit" class="bigsearchbtn" id="searchsubmit" value="<?php echo __( 'Otsi', 'artun2012' ); ?>" />
		</form>
	</div><!--/.pageheader-->
	<div class="content">

		<div class="column_left">

			<?php if (function_exists('relevanssi_didyoumean')) { 
				relevanssi_didyoumean( get_search_query(), "<p>" . __("Kas pidasid silmas: ", "artun2012"), "</p>", 5 );
			} ?>

			<?php while ( have_posts() ) : the_post(); ?>
				<article>
					<?php
						// Results for contacts, calendar, all others
						if ( get_post_type( $post->ID ) == 'eka_contacts' ) : ?>
								
							<h3><a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?> püsilink" rel="bookmark"><?php the_title(); ?></a></h3>
							<p>
								<?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?>, 
								<?php
									$categories = get_the_category();
									$separator = ', ';
									$output = '';
									if( $categories ) {
										foreach($categories as $category) {
											$output .= $category->cat_name.$separator;
										}
									echo trim($output, $separator);
									}
								?>
							</p>
							<p>
								<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>"><?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?></a><br />
								+372 <?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?>
							</p>
						
						<?php elseif ( get_post_type( $post->ID ) == 'eka_calendar' ) : ?>

							<?php
								$start_date = get_post_meta($post->ID, 'event_start_date', true);
								$end_date = get_post_meta($post->ID, 'event_end_date', true);
								$start_time = get_post_meta($post->ID, 'event_start_time', true);
								$location = get_post_meta($post->ID, 'event_location', true);
								
								if ( ( $start_date == $today ) || ( $start_date <= $today && $end_date >= $today ) ) {
									$todayevent = '<span class="calendar_today">' . __( 'Hetkel', 'artun2012' ) . '</span> ';	
								} else {
									$todayevent = '';
								}
								
								if ( $end_date == 0 ) {
									$show_dates = utf8_encode(strftime('%e. %B %Y', strtotime($start_date)));
								} else {
									$show_dates = utf8_encode(strftime('%e. %B %Y', strtotime($start_date))) . ' — ' .utf8_encode(strftime('%e. %B %Y', strtotime($end_date)));
								};
								
								if ( $start_time != 0 ) {
									$show_time = ', kell ' . $start_time;
								} else {
									$show_time = '';
								}
							?>
							<h3><a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?> püsilink" rel="bookmark"><?php the_title(); ?></a></h3>
							<p>
								<span class="dates"><?php echo $show_dates; ?><?php echo $show_time; ?></span>
								<br /><?php echo $location; ?>
							</p>

						<?php else: ?>
							
								<h3><a href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?> püsilink" rel="bookmark"><?php the_title(); ?></a></h3>
								<?php echo str_replace('...', '...', get_the_excerpt()); ?>
								
						<?php endif; ?>
					</article>
			<?php endwhile; ?>
		</div><!--/.column_left-->
	</div><!--/.content-->
<?php else: ?>

	<div class="pageheader clearfix">
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<input type="text" class="bigsearchbox" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo __( 'Otsi nime, eriala, dokumenti, ...', 'artun2012' ); ?>" autofocus />
			<input type="submit" class="bigsearchbtn" id="searchsubmit" value="<?php echo __( 'Otsi', 'artun2012' ); ?>" />
		</form>
	</div><!--/.pageheader-->
	<div class="content">

		<div class="column_left">
			<?php echo __( 'Otsingule', 'artun2012' ); ?> "<?php echo get_search_query(); ?>" <?php echo __('tulemusi ei leitud.', 'artun2012' ); ?><br />
			<?php echo __('Proovi otsingut täpsustada või kasutada teisi märksõnu.', 'artun2012' ); ?>
		</div><!--/.column_left-->
	</div><!--/.content-->		

<?php endif; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>