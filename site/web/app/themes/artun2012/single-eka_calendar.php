<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="pageheader">
	<h2 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h2>
</div><!--/.pageheader-->
<?php
if ( ICL_LANGUAGE_CODE == 'et') {
	setlocale( LC_ALL, "et_EE" ); 
}
?>
<div class="content clearfix" id="content">
	<div class="column_left float">
		<article>
			<div class="postrollgallery clearfix">
				<?php eka_load_postgallery( $post->ID ) ?>
			</div>
			<div class="contentbox editable-<?php echo $post->ID; ?>">
				<?php the_content(); ?>
			</div>
			<?php eka_share( urlencode( get_permalink() ), $post->post_title ); ?>
		</article>
	</div><!--/.column_left-->
	<div class="column_right float">
		<?php if ( current_user_can( 'edit_post' , $post->ID ) ): ?>
		<aside class="adminaside clearfix">
			<p><?php echo getPostViews( $post->ID ) ?> <?php echo __( 'vaatamist', 'artun2012' ); ?></p>

			<?php 
				$updater = get_post_meta($post->ID, 'post_last_editor', true);

				// check if we have an updater and wether this post has been updated
				if ( $updater && ( strtotime( $post->post_date ) - strtotime( $post->post_modified ) !== 0 ) ) {

					$updater_info = get_userdata( $updater );

					echo '<p>' . __( 'Viimati muutis:', 'artun2012' ) . '<br />' . $updater_info->user_firstname . ' ' . $updater_info->user_lastname . ', ' . strftime( '%e. %B %Y', strtotime( $post->post_modified ) ) . '</p>';

				}
			?>
			<?php eka_post_categories(); ?>

			<span class="js_contentsave editpostlink inline primary" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-yes"></span> <?php echo __( 'Salvesta', 'artun2012' ); ?></span>
			<span class="js_contentundo editpostlink inline" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-no-alt"></span> <?php echo __( 'Tühista', 'artun2012' ); ?></span>
			<span class="js_contentedit editpostlink inline" data-edit-id="<?php echo $post->ID; ?>"><span class="dashicons dashicons-edit"></span> <?php echo __( 'Muuda', 'artun2012' ); ?></span>

			<?php if ( current_user_can( 'manage_options' )) {
				echo '<a class="editpostlink inline" href="' . get_edit_post_link( $post->ID ) . '"><span class="dashicons dashicons-admin-settings"></span> ' . __( 'Haldur', 'artun2012' ) . '</a>';
			} ?>

			<span class="js_openedit editpostlink inline"><?php echo __( 'Kuupäev ja seaded', 'artun2012' ); ?></span>
			<?php eka_update_event_form(); ?>

		</aside>
		<?php endif; ?>

			<?php
				
				$start_date = get_post_meta($post->ID, 'event_start_date', true);
				$end_date = get_post_meta($post->ID, 'event_end_date', true);
					
				if ( $end_date == 0 ) {
					$show_dates = utf8_encode(strftime('%e. %B %Y', strtotime($start_date)));
				} else {
					$show_dates = utf8_encode(strftime('%e. %B', strtotime($start_date))) . ' —' .utf8_encode(strftime('%e. %B %Y', strtotime($end_date)));
				};
				
			?>
		<h3><?php echo __('Kuupäevad', 'artun2012'); ?></h3>
		<aside>
			<?php echo $show_dates; ?> <?php editable_post_meta( $post->ID, 'event_start_time', 'input' ); ?>
		</aside>
		<h3><?php echo __('Asukoht', 'artun2012'); ?></h3>
		<aside>
			<a href="https://maps.google.com/maps?q=<?php echo get_post_meta($post->ID, 'event_location', true); ?>" target="_blank"><?php editable_post_meta( $post->ID, 'event_location', 'input' ); ?></a>
		</aside>
		<?php 
		$posttags = get_the_terms( $post->ID, 'calendar_tag' );
		if ( $posttags ) :
			?>
			<h3><?php echo __('Märksõnad', 'artun2012'); ?></h3>
			<aside>
				<ul>
					<?php foreach( $posttags as $term ) : ?>
						<li><a href="<?php echo site_url(); ?>/oppimine/kalender?tag=<?php echo $term->slug; ?>"><?php echo $term->name; ?></a></li>
					<?php endforeach; ?>
				</ul>
			</aside>
		<?php endif; ?>
		<aside>
			<?php comments_template( '', true ); ?>
		</aside>
	</div><!--/.column_right-->

<?php setPostViews(get_the_ID()); ?>
<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>