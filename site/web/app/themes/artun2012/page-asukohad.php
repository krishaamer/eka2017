<?php
/**
 * Template Name: Asukohad
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title(); ?></h2>
		<?php eka_page_menu(); ?>
	</div><!--/.pageheader-->

	<?php setlocale( LC_ALL, "et_EE" ); ?>

	<div class="content clearfix" id="content">
		<div class="column_left float">
			<h3 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h3>

			<?php

			if ( function_exists( 'pronamic_google_maps_mashup' ) ) {
				?>

				<div style="margin-bottom:1em;">

				<?php
					pronamic_google_maps_mashup(
						array(
							'post_type' => 'eka_contacts'
						), 
						array(
							'width'          => 500,
							'height'         => 500,
							'map_options' => array(
								'styles' => array(
									(object) array(
										'featureType' => 'all',
										'stylers' => array(
											(object) array( 'saturation' => -100 )
										)
									),
									(object) array(
										'featureType' => 'road.highway',
										'stylers' => array(
											(object) array( 'hue' => '#ffffff' )
										)
									)
								)
							)
						)
					);
				?>

				</div>
				
				<?php
			}

			?>


			<div class="contentbox editable-<?php echo $post->ID; ?>">
				<?php the_content(); ?>
			</div>

			<?php eka_the_posts(); ?>
		</div><!--/.column_left-->

		<div class="column_right float">
			<?php if ( current_user_can( 'edit_page', $post->ID ) ): ?>
			<aside class="adminaside">
				<p><?php echo getPostViews( $post->ID ) ?> vaatamist</p>
				<?php 
					$updater = get_post_meta($post->ID, 'post_last_editor', true);

					// check if we have an updater and wether this post has been update
					if ( $updater && ( strtotime( $post->post_date ) - strtotime( $post->post_modified ) !== 0 ) ) {

						$updater_info = get_userdata( $updater );

						echo '<p><span class="bg">Lehte muutis viimati:<br />' . $updater_info->user_firstname . ' ' . $updater_info->user_lastname . ', ' . strftime( '%e. %B %Y', strtotime( $post->post_modified ) ) . '</span></p>';

					}
					
					echo '<span class="js_contentsave editpostlink inline primary" data-save-id="' . $post->ID . '" style="display:none;"><span class="dashicons dashicons-yes"></span> Salvesta</span>';
					echo '<span class="js_contentundo editpostlink inline" data-save-id="' . $post->ID . '" style="display:none;"><span class="dashicons dashicons-no-alt"></span> Tühista</span>';
					echo '<span class="js_contentedit editpostlink inline" data-edit-id="' . $post->ID . '"><span class="dashicons dashicons-edit"></span> Muuda</span>';
					if ( current_user_can( 'manage_options' )) {
						echo '<a class="editpostlink inline" href="' . get_edit_post_link( $post->ID ) . '"><span class="dashicons dashicons-admin-settings"></span> Haldur</a>';
					}
				?>
			</aside>
			<?php 
			endif;
			if ( is_user_logged_in() ) : ?>
				<aside>
					<?php editable_post_meta( $post->ID, 'column_right', 'rich' ); ?>
				</aside>
			<?php else: ?>
				<?php $column_right = get_post_meta( $post->ID, 'column_right', true );
				if( $column_right != '' ) : ?>
					<aside>
						<?php echo apply_filters( 'the_content', $column_right ); ?>
					</aside>
				<?php endif; ?> 
			<?php endif; ?>
			<?php

				// Find connected contacts
				$contacts = new WP_Query( array(
				  'connected_type' => 'pages_to_contacts',
				  'connected_items' => $post,
				  'nopaging' => true,
				) );
				
				// Display connected contacts
				if ( $contacts->have_posts() ) :
				?>
				<aside>
					<h3><span class="bg"><?php echo _('Kontakt'); ?></span></h3>
					<ul>
					<?php while ( $contacts->have_posts() ) : $contacts->the_post(); ?>
						<li class="hcard contact_card">
							<h4 class="contact_title">
								<a href="<?php the_permalink(); ?>" class="fn bg"><?php the_title(); ?></a>
							</h4>
							<p>
								<span class="bg"><?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?></span>
							</p>
							<p>
								<span class="tel bg"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span>
								<br />
								<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email bg">
									<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
								</a>
							</p>
							<p class="contact_portrait">
								<?php eka_first_image( $post->ID ); ?>
							</p>
						</li>
					<?php endwhile; ?>
					</ul>
				</aside>

				<?php 
				// Prevent weirdness
				wp_reset_postdata();
				
				endif;
			?>
			
		</div><!--/.column_right-->

		<?php setPostViews(get_the_ID()); ?>

<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>