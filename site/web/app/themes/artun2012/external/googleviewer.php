<?php
	ini_set('display_errors',1); 
?>
<?php
define('WP_USE_THEMES', false);
require('../../../../wp-blog-header.php');
?>
<?php

$fileurl = 'http://www.artun.ee/wp-content/uploads/2012/09/EKA_esitlus_2012_EST.ppt';

$response = wp_remote_request('https://docs.google.com/viewer?url=http://www.artun.ee/wp-content/uploads/2012/09/EKA_esitlus_2012_EST.ppt&embedded=true', array(
				'timeout' => 60,
				'sslverify' => false,
				'method' => 'GET'
			));

if ( is_wp_error( $response ) )
   echo $response->get_error_message();

$code = wp_remote_retrieve_body( $response );
				
	if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) != 200 ) {
		return false;
	} else {
		$result = trim( $response['body'] );
		if ( empty( $result ) && function_exists('curl_version') ) {
			// document returned OK but has no contents
			// (this is rare but seems to happen in some server configs)
			//$result = trim( gde_curl_get_contents( $url ) );
			if ( empty( $result ) ) {
				return false;
			}
		} elseif ( empty( $result ) ) {
			return false;
		}
	}


	
	if ( ! $code ) {
		// blank page was returned
		exit;
	} else {
		// fix js path
		$search[] = "gview/resources_gview/client/js";
		$replace[] = "https://docs.google.com/gview/resources_gview/client/js"; // use this if js not proxied
		//$replace[] = "?jsfile=gview/resources_gview/client/js";	// use this instead to proxy the js
		
		/*	// new toolbar button style
			$newstyles[] = '
				#controlbarControls { padding-top: 3px; margin-left: 5px; right: 5px; }
				#controlbarPageNumber { padding: 4px 5px 0 5px; }
				.toolbar-button-icon { padding-top: 1px; }
				.goog-toolbar-button-outer-box { opacity: 0.60; padding: 1px; }
				.goog-toolbar-button-outer-box:hover { opacity: 1.0; padding: 0; }
				.goog-toolbar-button-outer-box:hover { 
					background-color: #F8F8F8;
					border: 1px solid #CCC;
					background-image: -moz-linear-gradient(center top , #F8F8F8, #F1F1F1);
					box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1) inset;
				}
				.goog-custom-button-disabled, .goog-custom-button-disabled:hover { opacity: 0.3; border: 0; padding: 1px; }
				#openFullScreenButtonIcon {
					background-image: url("img/sprite-fullscr.png");
					background-position: center;
				}
				.view { background-color: inherit; }
			';
			
			// hide open in new window
			if ( strstr( $tb, 'n' ) !== false || ( $profile['tb_fulluser'] == "yes" && ! is_user_logged_in() ) ) {
				$newstyles[] = "#controlbarOpenInViewerButton, #gdeControlbarOpenInViewerButton { display: none !important; }";
			}
			*/
		}
		
		// toolbar flag key
		/*
			h	=>	hide toolbar (all)
			n	=>	hide new window button (embedded only)
			p	=>	hide page numbers (embedded & mobile)
			r	=>	hide next/prev page (all)
			z	=>	hide zoom in/out (all)
		*/
		/*
		// hide toolbar
		if (strstr($tb, 'h')) {
			$newstyles[] = "#controlbar { display: none; }";
		}
		
		// hide page numbers
		if (strstr($tb, 'p')) {
			$newstyles[] = "#controlbarPageNumber { display: none !important; }";
		}
		
		// hide prev/next buttons
		if (strstr($tb, 'r')) {
			$newstyles[] = "#controlbarBackButton { display: none !important; }";
			$newstyles[] = "#controlbarForwardButton { display: none !important; }";
		}
		
		# hide zoom in/out
		if (strstr($tb, 'z')) {
			if ( $mode == "mobile" ) {
				$newstyles[] = ".mobile-button-zoom-out { display: none !important; }";
				$newstyles[] = ".mobile-button-zoom-in { display: none !important; }";
			} else {
				$newstyles[] = "#controlbarZoomOutButton { display: none !important; }";
				$newstyles[] = "#controlbarZoomInButton { display: none !important; }";
			}
		}
		
		// the below viewer styles are applied to all modes
		/*
			b	=>	remove page borders
			f	=>	simulate "full bleed" (minimal margins) (currently not exposed)
			t	=>	transparent background
			v	=>	reduce vertical margins (currently not exposed)
			x	=>	hide selection of text
		*/
		
		// "full bleed" (experimental) - low res and breaks zooming
		/*if (strstr($vw, 'f')) {
			$newstyles[] = "#page-pane { margin: 0 -8px !important; }";
			$newstyles[] = "#gview { padding-bottom: -8px !important; }";
			$newstyles[] = ".page-element { margin: 0 !important; padding: 0 !important; width: 100% !important; }";
			$newstyles[] = ".page-image { width: 99% !important; }";
			$newstyles[] = "#content-pane { overflow-x: hidden; }";
		}
		
		// reduce vertical margins
		if (strstr($vw, 'v')) {
			//$newstyles[] = "#content-pane>div { margin: 0 !important; padding: 0 !important; }";
			$newstyles[] = "#content-pane { text-align: left; }";
			$newstyles[] = ".page-element { padding: 0; }";
			//$newstyles[] = "#content-pane { background-color: transparent; }";
		}
		
		// hide selection of text
		if (strstr($vw, 'x')) {
			$search[] = 'class="page-image';
			$replace[] = 'class="page-image noselect';
			$newstyles[] = ".rubberband { display: none; }";
			$newstyles[] = ".highlight-pane { display: none; }";
			$newstyles[] = ".page-image { cursor: default; }";
		}
		
		// remove page borders or change color
		if ( strstr( $vw, 'b' ) && $mode !== "chrome" ) {
			$newstyles[] = ".page-image { border: none; }";
		} elseif ( $profile['vw_pbcolor'] ) {
			// set page border color
			$newstyles[] = ".page-image { border: 1px solid " . $profile['vw_pbcolor'] . "; }";
		}
		
		// set viewer background color
		if ( strstr( $vw, 't' )) {
			$newstyles[] = "#content-pane { background-color: transparent; }";
		} elseif ( ! empty( $bg ) ) {
			$newstyles[] = "#content-pane { background-color: $bg; }";
		}
		
		if (count($newstyles) > 0) {
			// build new stylesheet
			$styleblock = array();
			$styleblock[] = "\n<!-- GDE STYLE INSERT -->";
			
			$styleblock[] = '<style type="text/css">';
			foreach ($newstyles as $ln) {
				$styleblock[] = "\t $ln";
			}
			$styleblock[] = "</style>";
			
			// insert new styles
			$styleblock = implode("\n", $styleblock)."\n</head>";
			$search[] = "</head>";
			$replace[] = $styleblock;
		}
		
		// override stylesheet
		if ( ! empty( $css ) ) {
			$cssblock = '<link rel="stylesheet" type="text/css" href="'.$css."\">\n</head>";
			$search[] = "</head>";
			$replace[] = $cssblock;
		}
		
		// override new window behavior
		$src = "js/tb-newwin.php";
		if ( $mode == "embedded" && $profile['tb_fullscr'] !== "default" ) {
			if ( $profile['tb_fullwin'] == "same" ) {
				$arg[] = "a=fs";
			}
			if ( $profile['tb_print'] == "yes" ) {
				$arg[] = "p=1";
			}
			if ( isset( $arg ) && is_array( $arg ) ) {
				$src .= "?hl=$lang&" . implode("&", $arg);
			}
			$scriptblock = '<script type="text/javascript" src="' . $src . '"></script>';
			$search[] = "</body>";
			$replace[] = $scriptblock."\n</body>";
		} elseif ( $profile['tb_fullscr'] == "default" && $profile['tb_fullwin'] == "same" ) {
			$src .= "?a=fs&url=" . $_GET['url'] . "&hl=" . $lang;
			$scriptblock = '<script type="text/javascript" src="' . $src . '"></script>';
			$search[] = "</body>";
			$replace[] = $scriptblock."\n</body>";
		}
		*/
		// perform string replacements
		$code = str_replace($search, $replace, $code);

	
	// disable caching of viewer if link is blocked or document cache is off
	/*if ( $profile['link_block'] == "yes" || strstr( urldecode( $_GET['url'] ), "?" ) ) {
		gde_block_caching();
	}
	*/
	// output page
	header('Content-type: text/html; charset=utf-8');
	echo $code;




?>
