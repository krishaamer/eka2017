<?php
/**
 * Template Name: Eriala, esileht NEW DESIGN
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title_new(); ?></h2>
		<?php eka_page_menu_new( "dept" ); ?>
	</div><!--/.pageheader-->

	<?php eka_page_admin(); ?>

	<div class="gallery-home" id="content">
		<div class="column_left">
	        <div class="contentbox editable-<?php echo $post->ID; ?>">
				<?php the_content(); ?>
			</div>
	    </div>
		<div class="gallery-dept clearfix" id="gallery">
			
			<?php eka_the_posts(); ?>
			<?php eka_load_gallery( array( 'show_on_page' => $post->post_parent ) ); ?>
			
		</div>
		<div class="column_left">
			<?php editable_post_meta( $post->ID, 'column_right', 'rich' ); ?>
		</div>
	</div>

<?php setPostViews( get_the_ID() ); ?>

<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>