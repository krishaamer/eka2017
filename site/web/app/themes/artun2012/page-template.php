<?php
/**
 * Template Name: Tühi template
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
    	<h2><?php the_parent_title(); ?></h2>
		<?php // the_title(); ?>
    	<?php eka_page_menu(); ?>
    </div><!--/.pageheader-->
<?php eka_page_menu(); ?>
<?php endwhile; ?>

Lehe template.

<?php setPostViews(get_the_ID()); ?>
               
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>