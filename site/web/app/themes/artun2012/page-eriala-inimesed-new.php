<?php
/**
 * Template Name: Eriala, inimesed NEW DESIGN
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<?php 
		// Gets the category. Necessary, as contacts are currently displayed based on categories.
		$category = get_the_category(); 
		$et_cat_id = icl_object_id( $category[0]->term_id, 'category', true, 'et' );

		function current_tag( $current_tag, $selected_tag ) {
			if( $selected_tag == $current_tag ) {
				echo 'current_page_item';
			}
		} 

		// Gets the tag info, if one is set
		if( $wp_query->query_vars[ 'tag' ] != '' ) {
			$selected_tag = $wp_query->query_vars['tag'];
						
			// Gets this tag item
			$tagObj = get_term_by( 'slug', $selected_tag, 'post_tag' );
						
			// Modify the arguments
			$tag_id = $tagObj->term_id;

			// Update the $selected_tag variable, for security reasons
			$selected_tag = $tagObj->slug;
		} else {
			$selected_tag = 'all';
			$tag_id = '';
		}

		// arguments to and get the posts for contacts			
		$contacts = get_posts( array( 
			'post_type' 		=> 'eka_contacts',
			'numberposts'     	=> -1,
			'tag_id'			=> $tag_id,
			'lang'				=> '',
			'category__in' 		=> array( $et_cat_id ),
			'orderby'         	=> 'menu_order title',
			'order'           	=> 'ASC' 
		) );
	?>

	<div class="pageheader">
		<h2><?php the_parent_title_new(); ?></h2>
		<?php eka_page_menu_new( "dept" ); ?>
	</div><!--/.pageheader-->

	<?php eka_page_admin(); ?>

	<div class="content clearfix">
		<h3 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h3>

		<div class="column_left">
	        <div class="contentbox editable-<?php echo $post->ID; ?>">
				<?php the_content(); ?>
			</div>
	    </div>

		<?php eka_add_contact_form(); ?>

		<div class="column_left">
			<ul class="tagslist" id="tagslist">
				<li class="tagoption <?php current_tag( 'all', $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag=">Kõik</a></li>
				<?php

				// Gets all the tags related to contacts shown on this page

				$tags = get_category_tags( array( 
					'categories' 	=> $et_cat_id, 
					'taxonomy' 		=> 'category',
					'term' 			=> 'post_tag',
					'post_type'		=> 'eka_contacts'
				) );
											
				foreach ( $tags as $tag ) : ?>
				<li class="tagoption <?php current_tag( $tag->slug, $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a></li>
				<?php endforeach; ?>

				<?php
					if ( count( $tags ) >= 10 ) :
				?>
				<li class="moretags" id="moretags"><?php echo __('rohkem silte...', 'artun2012'); ?></li>
				<?php endif; ?>
			</ul><!--/.taglist-->
		</div>
	</div>

	<div class="gallery-home gallery-contacts" id="content">		

		<?php if ( eka_current_user_has_rights() ) : ?>
			<div class="sortable gallery-dept clearfix" id="gallery">
		<?php else : ?>
			<div class="visitor gallery-dept clearfix" id="gallery">
		<?php endif; ?>
		<?php									
			// begin the posts (contacts) loop
			foreach( $contacts as $post ) : setup_postdata( $post ); ?>

				<?php 
					$url = $post->guid;
					$target = "_self";
					$imgargs = array(
						'alt'   => trim(strip_tags( $post->post_title )),
						'title' => trim(strip_tags( $post->post_title )),
					);
			
					// gets the first image associated with this post				
					$showimage = eka_attachment( $post->ID );
					if ( $showimage ):	?>
						<div class="<?php echo $post->ID; ?> vcard imagewrapper contactimage">
							<div>
								<div class="imagebox"> 
									<?php echo eka_get_attachment_image( $showimage, $imgargs ); ?>
									<div class="imagedetails click_link" data-post_guid="<?php echo $url; ?>" target="<?php echo $target; ?>">

										<div class="imagecontent">

											<?php if ( pll_current_language() == 'et' ) : ?>
												<?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?>
											<?php else: ?>
												<?php editable_post_meta( $post->ID, 'contact_role_en', 'input' ); ?>
											<?php endif; ?>
											<br />
											<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email">
												<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
											</a>
											<br />
											<span class="tel"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span><br />
											<?php echo eka_get_the_excerpt(140); ?>
											

										</div><!--/.imagecontent-->

										<div class="readmore">... <?php echo __( 'vaata profiili', 'artun2012' ); ?></div>

										<?php if ( current_user_can('edit_posts') ) : ?>
											<p class="adminimage">
												<span class="sort handle imageedit"><span class="dashicons dashicons-randomize"></span></span> <?php echo wp_delete_post_link(); ?>
											</p>
										<?php endif; ?>

									</div><!--/.imagedetails-->

								</div><!--/.imagebox-->
							</div>
							<div class="contactdetails">
								<h5 class="contact_title"><?php echo trim(strip_tags( $post->post_title )); ?></h5>
							</div>
						</div><!--/.imagewrapper--> 
					<?php else: ?>
						<div class="<?php echo $post->ID; ?> vcard imagewrapper contactimage">
							<h5 class="contact_title">
								<a href="<?php the_permalink(); ?>" class="fn"><?php the_title(); ?></a>
							</h5>
							<p>
								<?php if ( pll_current_language() == 'et' ) : ?>
									<?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?>
								<?php else: ?>
									<?php editable_post_meta( $post->ID, 'contact_role_en', 'input' ); ?>
								<?php endif; ?>
							</p>
							<p>
								<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email">
								<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
								</a><br />
								<span class="tel"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span>
							</p>
							<?php if ( current_user_can('edit_posts') ) : ?>
								<div class="edit">
									<span class="sort handle imageedit"><span class="dashicons dashicons-randomize"></span></span> <?php echo wp_delete_post_link(); ?>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
					
				<?php endforeach;  wp_reset_postdata(); ?>
			</div>
			<div class="column_left">
				<?php editable_post_meta( $post->ID, 'column_right', 'rich' ); ?>
			</div>
		</div>
	</div>
	
<?php setPostViews( get_the_ID() ); ?>

<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>