<?php
add_action( 'init', 'post_type_calendar' );
function post_type_calendar() {
	register_post_type( 'eka_calendar',
		array(
			'labels' => array(
				'name' => __( 'Kalender' ),
				'singular_name' => __( 'Sündmus' ),
				'add_new' => _x('Lisa uus', 'sündmus'),
				'add_new_item' => __('Lisa uus sündmus'),
				'edit_item' => __('Muuda sündmust'),
				'new_item' => __('Uus sündmus'),
				'all_items' => __('Kõik sündmused'),
				'view_item' => __('Vaata sündmust'),
				'search_items' => __('Otsi sündmust'),
				'not_found' =>  __('Sündmusi ei leitud')
			),
			'public' => true,
			'menu_position' => 5,
			'hierarchical' => false,
			'capability_type' => 'post',
			'supports' => array('title', 'editor', 'trackbacks', 'custom-fields', 'thumbnail', 'comments'),
			'has_archive' => true,
			'show_in_admin_bar' => false,
			'taxonomies' => array('category', 'calendar_tag'),
			'rewrite' => array('slug' => 'kalender')
		)
	);
}
