<?php
add_action( 'init', 'post_type_xpages' );
function post_type_xpages() {
	register_post_type( 'eka_xpages',
		array(
			'labels' => array(
				'name' => __( 'Erilehed' ),
				'singular_name' => __( 'Erileht' ),
				'add_new' => _x('Lisa uus', 'erileht'),
				'add_new_item' => __('Lisa uus erileht'),
				'edit_item' => __('Muuda erilehte'),
				'new_item' => __('Uus erileht'),
				'all_items' => __('Kõik erilehed'),
				'view_item' => __('Vaata erilehte'),
				'search_items' => __('Otsi erilehti'),
				'not_found' =>  __('Erilehti ei leitud')
			),
			'description' => 'EKA veebi lehed, mida kuvatakse ilma päiseta',
			'public' => true,
			'menu_position' => 20,
			'hierarchical' => true,
			'capability_type' => 'page',
			'supports' => array('title', 'excerpt', 'editor', 'custom-fields', 'page-attributes', 'revisions'),
			'has_archive' => true,
			'show_in_admin_bar' => false,
			'taxonomies' => array('category'),
			'rewrite' => array('slug' => 'x', 'with_front' => true)
		)
	);
}
