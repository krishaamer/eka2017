<?php
add_action( 'init', 'post_type_projects' );
function post_type_projects() {
	register_post_type( 'eka_project',
		array(
			'labels' => array(
				'name' => __( 'Galeriid' ),
				'singular_name' => __( 'Galerii' ),
				'add_new' => _x('Lisa uus', 'galerii'),
				'add_new_item' => __('Lisa uus galerii'),
				'edit_item' => __('Muuda galeriid'),
				'new_item' => __('Uus galerii'),
				'all_items' => __('Kõik galeriid'),
				'view_item' => __('Vaata galeriid'),
				'search_items' => __('Otsi galeriid'),
				'not_found' =>  __('Galeriisid ei leitud')
			),
			'public' => true,
			'menu_position' => 5,
			'hierarchical' => false,
			'capability_type' => 'post',
			'supports' => array('title', 'editor', 'custom-fields'),
			'has_archive' => true,
			'show_in_admin_bar' => false,
			'taxonomies' => array('category', 'post_tag', 'show_on_page'),
			'rewrite' => array('slug' => 'galerii')
		)
	);
}
