<?php
add_action( 'init', 'post_type_contacts' );
function post_type_contacts() {
	register_post_type( 'eka_contacts',
		array(
			'labels' => array(
				'name' => __( 'Kontaktid' ),
				'singular_name' => __( 'Kontakt' ),
				'add_new' => _x('Lisa uus', 'kontakt'),
				'add_new_item' => __('Lisa uus kontakt'),
				'edit_item' => __('Muuda kontakti'),
				'new_item' => __('Uus kontakt'),
				'all_items' => __('Kõik kontaktid'),
				'view_item' => __('Vaata kontakto'),
				'search_items' => __('Otsi kontakti'),
				'not_found' =>  __('Kontakte ei leitud')
			),
			'public' => true,
			'menu_position' => 20,
			'hierarchical' => false,
			'capability_type' => 'post',
			'supports' => array('title', 'editor', 'thumbnail', 'custom-fields','page-attributes'),
			'has_archive' => false,
			'show_in_admin_bar' => false,
			'taxonomies' => array('category', 'post_tag'),
			'rewrite' => array('slug' => 'inimesed')
		)
	);
}
