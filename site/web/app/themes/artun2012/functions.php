<?php

	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
	 * @package 	WordPress
	 * @subpackage 	Starkers
	 * @since 		Starkers 4.0
	 */

	/* ==============================================================================================================
	
	Required external files
	
	============================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ==============================================================================================================
	
	Theme specific settings
	
	============================================================================================================== */

	add_theme_support('post-thumbnails');


	/* ==============================================================================================================
	
	Disable the default wordpress toolbar in front-end and restrict non-admin users from the admin area
	
	============================================================================================================== */
	
	function eka_restrict_admin() {
		if ( ! current_user_can( 'manage_options' ) && $_SERVER['PHP_SELF'] != '/wp-admin/admin-ajax.php' ) {
			wp_redirect( site_url() ); exit;
		}
	}

	add_action( 'admin_init', 'eka_restrict_admin' ); // Remove for dev
	show_admin_bar( false ); // Remove for dev


	/* ==============================================================================================================
	
	Make the theme translatable, the correct way
	
	============================================================================================================== */

	function eka_theme_setup(){
		load_theme_textdomain( 'artun2012' , get_template_directory() . '/languages' );
	}

	add_action( 'after_setup_theme', 'eka_theme_setup' );
	

	/* ==============================================================================================================
	
	Register a few widget areas
	
	============================================================================================================== */
	
	if ( function_exists('register_sidebar') ){
		register_sidebar(array('name'=>'Esileht EST','id'=>'sidebar-1'));
		register_sidebar(array('name'=>'Esileht ENG','id'=>'sidebar-2'));
		register_sidebar(array('name'=>'Jalus EST','id'=>'sidebar-3'));
		register_sidebar(array('name'=>'Jalus ENG','id'=>'sidebar-4'));
	}

	/* ==============================================================================================================
	
	Actions and Filters
	
	============================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'script_enqueuer' );
	add_filter( 'body_class', 'add_slug_to_body_class' );

	/* ==============================================================================================================
	
	Custom Post Types
	
	============================================================================================================== */
	
	require_once( 'custom-post-types/calendar.php' );

	require_once( 'custom-post-types/contacts.php' );

	require_once( 'custom-post-types/projects.php' );

	require_once( 'custom-post-types/xpages.php' );
	
	/* ==============================================================================================================
	
	Custom Taxonomies
	
	============================================================================================================== */
	
	add_action( 'init', 'create_event_taxonomies', 0 );
	//add_action( 'init', 'create_portfolio_taxonomies', 0 );
	add_action( 'init', 'create_newsposts_taxonomies', 0 );	

	function create_event_taxonomies() {
	  // Add new category taxonomy, make it hierarchical
	  $catlabels = array(
		'name' => _x( 'Ürituste kategooriad', 'taxonomy general name' ),
		'singular_name' => _x( 'Ürituse kategooria', 'taxonomy singular name' ),
		'search_items' =>  __( 'Otsi ürituse kategooriaid' ),
		'all_items' => __( 'Kõik kategooriad' ),
		'parent_item' => __( 'Ülemkategooria' ),
		'parent_item_colon' => __( 'Ülemkategooria:' ),
		'edit_item' => __( 'Muuda kategooriat' ), 
		'update_item' => __( 'Uuenda kategooriat' ),
		'add_new_item' => __( 'Lisa uus ürituste kategooria' ),
		'new_item_name' => __( 'Uue kategooria nimi' ),
		'menu_name' => __( 'Ürituste kategooriad' ),
	  ); 	
	
	  register_taxonomy('calendar_category','eka_calendar', array(
		'hierarchical' => true,
		'labels' => $catlabels,
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true
	  ));
	  
	  // Add new category taxonomy, make it hierarchical
	  $taglabels = array(
		'name' => _x( 'Ürituste märksõnad', 'taxonomy general name' ),
		'singular_name' => _x( 'Ürituse märksõna', 'taxonomy singular name' ),
		'search_items' =>  __( 'Otsi ürituse märksõnu' ),
		'all_items' => __( 'Kõik märksõnad' ),
		'parent_item' => __( 'Ülemmärsõna' ),
		'parent_item_colon' => __( 'Ülemmärsõna:' ),
		'edit_item' => __( 'Muuda märksõna' ), 
		'update_item' => __( 'Uuenda märksõna' ),
		'add_new_item' => __( 'Lisa uus ürituste märksõna' ),
		'new_item_name' => __( 'Uue märksõna nimi' ),
		'menu_name' => __( 'Ürituste märksõnad' ),
	  ); 	
	
	  register_taxonomy('calendar_tag','eka_calendar', array(
		'hierarchical' => false,
		'labels' => $taglabels,
		'show_admin_column' => true,
		'show_ui' => true,
		'query_var' => true
	  ));
	  
	}
	
	/* DEPRECATED */
	/*function create_portfolio_taxonomies() {
	  // Add new taxonomy, make it hierarchical (like categories)
	  $labels = array(
		'name' => _x( 'Galeriide tüübid', 'taxonomy general name' ),
		'singular_name' => _x( 'Galerii tüüp', 'taxonomy singular name' ),
		'search_items' =>  __( 'Otsi galeriide tüüpe' ),
		'all_items' => __( 'Kõik tüübid' ),
		'parent_item' => __( 'Ülemtüüp' ),
		'parent_item_colon' => __( 'Ülemtüüp:' ),
		'edit_item' => __( 'Muuda tüüpi' ), 
		'update_item' => __( 'Uuenda tüüpi' ),
		'add_new_item' => __( 'Lisa uus galerii tüüp' ),
		'new_item_name' => __( 'Uue galerii tüübi nimi' ),
		'menu_name' => __( 'Galeriide tüübid' ),
	); 	
	
	register_taxonomy('portfolio_category','eka_project', array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true
	  ));
	}*/

	// taxonomies to show posts on pages. we use a dedicated taxonomy to ease retrieving tags and other posts related content
	function create_newsposts_taxonomies() {
		register_taxonomy( 'show_on_page', 'post', array(
			'label' => __( 'Uudis lehel' ),
			'public' => true,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'query_var' => true,
			'rewrite' => false
		) );
	}	
	
	/* ==============================================================================================================
	
	Security enhancements
	
	============================================================================================================== */
	
	add_filter('login_errors',create_function('$a', "return null;"));
	remove_action('wp_head', 'wp_generator');
	function wpt_remove_version() {  
		return '';  
	}  
	add_filter('the_generator', 'wpt_remove_version');  
	
	
	/* ==============================================================================================================
	
	Redirect users after login
	
	============================================================================================================== */
	
	/*function eka_login_redirect( $redirect_to, $request, $user ){
		//is there a user to check?
		if( is_array( $user->roles ) ) {
			return home_url();
		}
	}
	add_filter("login_redirect", "eka_login_redirect", 10, 3);*/

	function eka_redirect_home( $redirect_to, $request, $user ) {
		return home_url();
	}

	add_filter( 'login_redirect', 'eka_redirect_home', 10, 3 );

	/* ==============================================================================================================
	
	Scripts
	
	============================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker, Tanel Kärp
	 */

	function script_enqueuer() {
		wp_register_script( 'site-common', get_template_directory_uri().'/js/site-common.js', array( 'jquery' ), '1.2.1', true );
		wp_register_script( 'mediumeditor', get_template_directory_uri().'/js/medium-editor.min.js', array( 'jquery' ), '5.22.1', true );
		wp_register_script( 'mediumeditortables', get_template_directory_uri().'/js/medium-editor-tables.min.js', array( 'mediumeditor' ), '0.5.3', true ); // Styles for medium editor and tables plugin are included in the style.css file!


		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-datepicker' );
		wp_enqueue_script( 'jquery-ui-sortable' );
		wp_enqueue_script( 'suggest' );
				
		wp_enqueue_script( 'site-common' );
		wp_enqueue_script( 'mediumeditor' );
		wp_enqueue_script( 'mediumeditortables' );

		wp_localize_script( 'site-common', 'ekaajax', array( 
			'ajaxurl' 	=> admin_url( 'admin-ajax.php' ),
			'ajaxnonce' => wp_create_nonce( 'ajax_nonce' )
		) );

		wp_register_style( 'screen', get_template_directory_uri().'/style.css', '', '1.3.0', 'screen' );


		wp_enqueue_style( 'dashicons' );		
		wp_enqueue_style( 'screen' );
	}	
	
	/* ==============================================================================================================
	
	Navigation menus
	
	============================================================================================================== */

	function eka_main_menu() {
		global $wp_query;
		
		$pages = get_pages( array(
			'sort_order' 	=> 'ASC',
			'sort_column' 	=> 'menu_order',
			'hierarchical' 	=> false,
			'parent' 		=> 0,
			'post_type' 	=> 'page',
			'post_status' 	=> 'publish'
		) );
		
		$links = '<div class="mainnav clearfix" id="mainnav"><ul>';
		
		$view_type = get_post_type();
		
		if ( $view_type == 'eka_project' ) {
			$pro_cat = get_the_category(); 
			if( empty( $pro_cat ) ) {
				$view_cat = 0;
			} else {
				$view_cat = $pro_cat[0]->term_id;
			}
		}
						
		foreach ( $pages as $page ) {
			
			// highlight main menu
			$current = '';
			if ( is_tree( $page->ID ) ) {
				$current = 'current_page_item';
			} else if ( $view_type == 'post' && $page->ID == 13 ) {
				$current = 'current_page_item';
			} else if ( $view_type == 'eka_calendar' && $page->ID == 11 ) {
				$current = 'current_page_item';
			} else if ( $view_type == 'eka_project' && $page->ID == 5 ) {
				$current = 'current_page_item';
			} else if ( is_home() && $page->ID == 5 ) {
				// don't show anything on front page
			}

			// For departments menu
			if ( $page->ID == 5 || $page->ID == 5642 ) {

				$links .= '<li class="' . $current . '"><span class="nav-' . $page->ID . '">';
				$links .= $page->post_title;
				$links .= '</span><div class="subnav widemenu" id="nav-' . $page->ID . '"><ul>';

				$links .= '<li class="menutitle">' . __( 'Bakalaureus', 'artun2012' ) . '</li>';

				$subba = eka_get_links( $page->ID, 'ba' );
				foreach ( $subba as $subpage ) {
					$subcurrent = '';
					if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
						$subcurrent = ' current_page_item';
					}
					$links .= '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}

				$subbax = eka_get_links( $page->ID, 'bax' );
				if ( $subbax ) {

					$links .= '<li class="menutitle">' . __( 'Tsükliõpe', 'artun2012' ) . '</li>';
					foreach ( $subbax as $subpage ) {
						$subcurrent = '';
						if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
							$subcurrent = ' current_page_item';
						}
						$links .= '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
					}

				}


				$links .= '</ul><ul><li class="menutitle">' . __( 'Magister', 'artun2012' ) . '</li>';
				$subma = eka_get_links( $page->ID, 'ma' );
				foreach ( $subma as $subpage ) {
					$subcurrent = '';
					if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
						$subcurrent = ' current_page_item';
					}
					$links .= '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}

				$links .= '</ul><ul><li class="menutitle">' . __( 'Doktoriõpe', 'artun2012' ) . '</li>';
				$subphd = eka_get_links( $page->ID, 'phd' );
				foreach ( $subphd as $subpage ) {
					$subcurrent = '';
					if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
						$subcurrent = ' current_page_item';
					}
					$links .= '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}

			// For all other menus
			} else {		

				$subpages = eka_get_links( $page->ID );	

				$subsize = count( $subpages );
				if ( $subsize <= 3 ) {
					$linkspercolumn = 3;
					$subclass = ' narrowmenu';
				} else {
					$linkspercolumn = round( $subsize/3 );
					$subclass = ' widemenu';
				}
				
				$links .= '<li class="' . $current . '"><span class="nav-' . $page->ID . '">';
				$links .= $page->post_title;
				$links .= '</span><div class="subnav' . $subclass . '" id="nav-' . $page->ID . '"><ul>';
							
				$i = 0;
				foreach ( $subpages as $subpage ) {
					
					$subcurrent = '';
					if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
						$subcurrent = ' current_page_item';
					}
					if ( isset( $view_cat ) ) {
						if ( in_category( $view_cat, $subpage->ID ) ) {
							$subcurrent = ' current_page_item';
						}
					}
					if ( $view_type == 'eka_calendar' && $subpage->ID == 126 ) {
						$subcurrent = ' current_page_item';	
					}

					$sublinks = '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">';

					$sublinks .= $subpage->post_title;
					$sublinks .= '</a></li>';
					
					$i++;
					
					if ( $i == $linkspercolumn ) {
						$sublinks .= '</ul><ul> ';
						$i = 0;
					}
					
					$links .= $sublinks;
				}

			}
			
			
			
			$links .= '</ul></div></li>';

		}
		
		$links .= '</ul>';
		echo $links;
		echo '</div><!--/#mainnav-->';
	}

	function eka_get_links( $parent, $deptlevel = '' ) {
		global $wpdb;

		if ( $deptlevel ) {
			// For department pages (have to be sorted by level, ordered by name)
			$links = $wpdb->get_results( 
				"	SELECT wposts.ID, wposts.post_name, wposts.post_title
					FROM $wpdb->posts AS wposts
					INNER JOIN $wpdb->postmeta AS wpostmeta
					ON wpostmeta.post_id = wposts.ID
					WHERE wposts.post_status = 'publish' 
						AND wposts.post_parent = $parent 
						AND wpostmeta.meta_key = 'dept_level'
						AND wpostmeta.meta_value = '$deptlevel'
					ORDER BY wposts.post_name ASC
				"
				);
		} else {
			// For all pages
			$links = $wpdb->get_results( 
				"	SELECT ID, post_name, post_title 
					FROM $wpdb->posts
					WHERE post_status = 'publish' 
						AND post_parent = $parent 
						AND post_type = 'page' 
					ORDER BY menu_order ASC
				"
			);
		}

		return $links;
	}
	
	function eka_page_menu( $pagetype = "all" ) {
		global $post;
		
		$parents = count(get_ancestors( $post->ID, 'page' ));

		
		if ( $parents == 2 ) {

			$parenturi = get_page_uri( $post->post_parent );
			
			$links = '<ul class="pagenav clearfix" id="pagenav">';

			$pages = eka_get_links( $post->post_parent );

			/*if ( $pagetype == "dept" ) {
				array_shift( $pages );
			}*/

			foreach ( $pages as $page ) {

				$current = '';
				if ( $page->ID == $post->ID ) {
					$current = ' current_page_item';
				}
							
				$links .= '<li class="nav-' . $page->ID . $current . '"><a href="' . icl_get_home_url() . $parenturi . '/' . $page->post_name . '">' . $page->post_title . '</a></li>';


			}

			$links .= '</ul><!--/.pagenav-->';
			echo $links;
			
		}
	}

	function eka_page_menu_new( $pagetype = "all" ) {
		global $wp_query, $post;

		$ancestors = get_ancestors( $post->ID, 'page' );
		$parents = count( $ancestors );

		if ( $parents == 1 ) {
			$parentid = $post->ID;
		} else if ( $parents == 2 ) {
			$parentid = $post->post_parent;
		} else if ( $parents == 3 ) {
			$parentid = $ancestors[1];
		}
		
		if ( $parents >= 2 ) {
			
			$pages = get_pages( array(
				'sort_order' 	=> 'ASC',
				'sort_column' 	=> 'menu_order',
				'hierarchical' 	=> false,
				'parent' 		=> $parentid,
				'post_type' 	=> 'page',
				'post_status' 	=> 'publish'
			) );

			/*if ( $pagetype == "dept" ) {
				array_shift( $pages );
			}*/

			
			$links = '<div class="pagenavnew clearfix" id="pagenavnew"><ul>';
			
			$view_type = get_post_type();
			
			if ( $view_type == 'eka_project' ) {
				$pro_cat = get_the_category(); 
				$view_cat = $pro_cat[0]->term_id;	
			}
							
			foreach ( $pages as $page ) {
				
				// highlight main menu
				$current = '';
				if ( is_tree( $page->ID ) ) {
					$current = 'current_page_item';
				} else if ( $view_type == 'post' && $page->ID == 13 ) {
					$current = 'current_page_item';
				} else if ( $view_type == 'eka_calendar' && $page->ID == 11 ) {
					$current = 'current_page_item';
				} else if ( $view_type == 'eka_project' && $page->ID == 5 ) {
					$current = 'current_page_item';
				} else if ( is_home() && $page->ID == 5 ) {
					// don't show anything on front page
				}

				// For departments menu
				if ( $page->ID == 5 || $page->ID == 5642 ) {

				// For when there are no children
				} else if ( first_child( $page->ID ) == false ) {

					$links .= '<li class="' . $current . '"><span class="nav-' . $page->ID . '">';
					$links .= '<a href="' . get_page_link( $page->ID ) . '">' . $page->post_title . '</a>';
					$links .= '</span></li>';

				// For all other menus
				} else {		

					$subpages = eka_get_links( $page->ID );	

					$subsize = count( $subpages );
					if ( $subsize <= 3 ) {
						$linkspercolumn = 3;
						$subclass = ' narrowmenu';
					} else {
						$linkspercolumn = round( $subsize/3 );
						$subclass = ' widemenu';
					}
					
					$links .= '<li class="' . $current . '"><span class="nav-' . $page->ID . '">';
					$links .= $page->post_title;
					$links .= '</span><div class="subnav' . $subclass . '" id="nav-' . $page->ID . '"><ul>';
								
					$i = 0;
					foreach ( $subpages as $subpage ) {
						
						$subcurrent = '';
						if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
							$subcurrent = ' current_page_item';
						}
						if ( isset( $view_cat ) ) {
							if ( in_category( $view_cat, $subpage->ID ) ) {
								$subcurrent = ' current_page_item';
							}
						}


						$sublinks = '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . get_page_link( $subpage->ID ) . '">';

						$sublinks .= $subpage->post_title;
						$sublinks .= '</a></li>';
						
						$i++;
						
						if ( $i == $linkspercolumn ) {
							$sublinks .= '</ul><ul> ';
							$i = 0;
						}
						
						$links .= $sublinks;
					}

				$links .= '</ul></div></li>';

				}
			}
			
			$links .= '</ul>';
			echo $links;
			echo '</div><!--/#pagenavnew-->';
		}
	}

	function eka_xpage_menu() {
		global $post;
		
		$parents = count( get_ancestors( $post->ID, 'eka_xpages' ));
		
		if ( $parents == 1 ) {
			$parentid = $post->post_parent;
		} else {
			$parentid = $post->ID;
		}
			
		$links = '<ul class="pagenav clearfix" id="pagenav">';
		$links .= wp_list_pages( array(
			'depth' 	=> 1,
			'child_of' 	=> $parentid,
			'title_li' 	=> '',
			'echo' 		=> 0,
			'post_type'	=> 'eka_xpages'	
		) );
		$links .= '</ul><!--/.pagenav-->';

		// Temporary dirty hack, remove!
		$links .= '<style>#un-button { display: none; }</style>';

		echo $links;
			
	}
	
	/* ==============================================================================================================
	
	Special title functions.
	
	============================================================================================================== */
	
	function the_parent_title() {
		global $post;
		
		$parents = count(get_ancestors( $post->ID, 'page' ));
		
		if ( $parents == 2 ) {
			$title = get_the_title($post->post_parent);
		} else {
			$title = get_the_title($post->ID);
		};
		
		echo $title;
	}

	function the_xpage_title() {
		global $post;
		
		$parents = count(get_ancestors( $post->ID, 'eka_xpages' ));

		if ( $parents == 1 ) {
			$title = '<a href="' . get_permalink($post->post_parent) . '">' . get_the_title( $post->post_parent ) . '</a>';
		} else {
			$title = get_the_title($post->ID);
		};
		
		echo $title;
	}

	function the_parent_title_new() {
		global $post;
		
		$ancestors = get_ancestors( $post->ID, 'page' );
		$parents = count( $ancestors );
		
		if ( $parents == 1 ) {
			$title = get_the_title( $post->ID );
			$link = get_permalink( $post->ID );
		} else if ( $parents == 2 ) {
			$title = get_the_title( $post->post_parent );
			$link = get_permalink( $post->post_parent );
		} else if ( $parents == 3 ) {
			$title = get_the_title( $ancestors[1] );
			$link = get_permalink( $ancestors[1] );
		}
		
		echo '<a href="' . $link . '">' . $title . '</a>';
	}
	
	/* ==============================================================================================================
	
	Global checks
	
	============================================================================================================== */
	
	// check if the page is in a parent tree
	function is_tree( $pid ) {  
		global $post; 
		
		$parentpost = get_post( $post->post_parent );
		
		if( is_page() && ( $post->post_parent == $pid || is_page($pid) || $parentpost->post_parent == $pid )) 
			return true;
		else 
			return false;
	};
	
	// gets first child id. returns false if no children exist
	function first_child( $pid ) {		
		$children = get_pages( array( 
			'parent' 		=> $pid,
			'hierarchical' 	=> false,
			'sort_column'	=> 'menu_order',
			'sort_order' 	=> 'ASC',
			'number'		=> 1
		) );
		if ($children) {
			$firstchild = $children[0];
			return $firstchild->ID;
		} else {
			return false;
		}
	}

	/* ==============================================================================================================
	
	Comments
	
	============================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php comment_text() ?>
				— <?php comment_author_link() ?>, <time><?php comment_date() ?></time>    
			</article>
		<?php endif; ?>
		</li>
		<?php 
	}
	
	/* ==============================================================================================================
	
	Wordpress fixes
	
	============================================================================================================== */
	
	function eka_css_class( $css_class, $page ) {
		global $post;
		if ( $post->ID == $page->ID ) {
			$css_class[] = 'current_page_item';
		}
		return $css_class;
	}
	add_filter( 'page_css_class', 'eka_css_class', 10, 2 );
	
	/* ==============================================================================================================
	
	If current page has children, show the first child content instead
	
	============================================================================================================== */ 
	
	
	add_action( 'pre_get_posts', 'replace_content_with_child' );
	
	function replace_content_with_child( $query ) {
		
		if( $query->is_main_query() && !is_admin() && is_page() && !$query->is_home() ) {					
			$firstchild = first_child($query->queried_object_id);
		
			if( $firstchild ) {		
				$query->query_vars['page_id'] = $firstchild;
			}
		}
		
	}

	
	/* ==============================================================================================================
	
	Add new query variables
	
	============================================================================================================== */ 
	
	function eka_add_query_vars($aVars) {
		//$aVars[] = "dept_page"; // for displaying department pages
		$aVars[] = "rubriik"; // for displaying calendar categories
		$aVars[] = "aeg"; // for displaying calendar days
		$aVars[] = "tag"; // for getting tags
		return $aVars;
	};
	add_filter('query_vars', 'eka_add_query_vars');

	
	/* ==============================================================================================================
	
	Add new htaccess rewrite rules
	
	============================================================================================================== */ 
	
	function eka_add_rewrite_rules($aRules) {
		$aNewRules = array();
		//$aNewRules['eriala/([^/]*)/([^/]*)/?'] = 'index.php?eka_departments=$matches[1]&dept_page=$matches[2]'; // department pages
		$aNewRules['oppimine/kalender/([^/]*)/?'] = 'index.php?pagename=oppimine/kalender&rubriik=$matches[1]'; // calendar categories
		$aRules = $aNewRules + $aRules;
		return $aRules;
	};
	// modify the array. this will only be read when Wordpress updates permalink options in Admin
	add_filter('rewrite_rules_array', 'eka_add_rewrite_rules');
	
	
	/* ==============================================================================================================
	
	Adding new feeds
	
	============================================================================================================== */ 
	
	function create_newsticker_feed() {
		load_template( TEMPLATEPATH . '/feeds/newsticker-feed.php'); // You'll create a your-custom-feed.php file in your theme's directory
	}
	
	add_action('do_feed_newsticker', 'create_newsticker_feed', 10, 1); // Make sure to have 'do_feed_customfeed'



	/* ==============================================================================================================
	
	Initializing and handling all our custom front-end non-AJAX form submissions
	
	============================================================================================================== */ 

	// Check and initialize!
	function eka_forms_submit() {
		if( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['eka_submit_type'] )) {

			switch ( $_POST['eka_submit_type'] ) {
				case "update_user_category":
					eka_add_user_cat();
					break;
				case "add_new_contact":
					eka_add_contact();
					break;
				case "add_new_post":
					eka_add_post();
					break;
				case "add_new_event":
					eka_add_event();
					break;
				case "add_new_project":
					eka_add_project();
					break;
				case "add_new_image":
					eka_add_images_to_post();
					break;
				case "update_post":
					eka_update_post();
					break;
				case "update_event":
					eka_update_event();
					break;
				case "update_contact":
					eka_update_contact();
					break;	
				case "update_project":
					eka_update_project();
					break;		
				/*case "":
					echo "i equals 2";
					break;*/
			}

		}

		if ( !empty( $_GET['trashed'] ) && $_GET['trashed'] == 1 ) {
			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-thumbs-up"></span></span>' . __( 'Sisu eemaldatud!', 'artun2012' );
		}
	}

	add_action('init', 'eka_forms_submit', 10);

	// Adding categories to users (called upon login when user has no cats set)
	function eka_add_user_cat() {
		global $current_user;

		if ( isset( $_POST['user_nonce_field'] ) && wp_verify_nonce( $_POST['user_nonce_field'], 'user_nonce' ) && current_user_can( 'edit_user', $current_user->ID ) ) {

			if ( update_user_meta( $current_user->ID, 'user_categories', $_POST['cats'] ) ) {

				delete_transient( 'cat-' . $current_user->user_login );
				$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Registreerumine õnnestus, teretulemast!', 'artun2012');
			}		
		}
	}
	
	/* ==============================================================================================================
	
	Showing news and different post types
	
	============================================================================================================== */ 
	
	// Displays calendar events where prompted
	function eka_the_calendar() {
		global $post;
		
		$show_cal = get_post_meta($post->ID, 'page_add_calendar', true); 
		
		// checks wether showing calendar is enabled on this page
		if ( $show_cal == 1 ) {
		
			$category = get_the_category(); 
			$category_id = $category[0]->term_id;
			
			$category_childs = get_categories( 'child_of='.$category_id );
			
			$categories = $category_id;
			
			foreach ($category_childs as $cat) {
				$categories .= ','.$category->cat_ID;
			}
						
			$tmp_post = $post;		
			
			setlocale(LC_ALL, "et_EE");	
			
			$current_datetime = current_time('mysql');
			list( $today_year, $today_month, $today_day, $hour, $minute, $second ) = preg_split( '([^0-9])', $current_datetime );
			$current_date = $today_year . $today_month . $today_day;
			
			// we have to do two separate queries here due to wordpress get_posts restrictions
			
			if( isset( $wp_query->query_vars['aeg'])) {
				$current_date = urldecode($wp_query->query_vars['aeg']);
			} 
			
			$todayargs = array( 
				'numberposts' 	=> -1,
				'post_type' 	=> 'eka_calendar',
				'cat' 			=> $categories,
				'orderby'       => 'title',
				'order'         => 'ASC',
				'suppress_filters' => 0,
				'meta_query' 	=> array(
					'relation' => 'AND',
					array(
						'key' => 'event_start_date',
						'value' => $current_date,
						'compare' => '<'
					),
					array(
						'key' => 'event_end_date',
						'value' => $current_date,
						'compare' => '>='
					)
				)
			);
			
			$futureargs = array( 
				'numberposts' 	=> -1,
				'post_type' 	=> 'eka_calendar',
				'cat' 			=> $categories,
				'meta_key'  	=> 'event_start_date',
				'orderby'       => 'meta_value_num',
				'order'         => 'ASC',
				'suppress_filters' => 0,
				'meta_query' 	=> array(
					array(
						'key' => 'event_start_date',
						'value' => $current_date,
						'compare' => '>='
					)
				)
			);
			
			$allposts = array_merge( get_posts( $todayargs ), get_posts( $futureargs ) );
			
			?>
				<h3><span class="bg"><?php echo __( 'Sündmused', 'artun2012' ); ?></span></h3>
			<?php
			
			foreach( $allposts as $post ) : setup_postdata($post); ?>
			
				<?php

					$details = get_post_custom($post->ID);

					$start_date = $details['event_start_date'][0];
					$end_date = $details['event_end_date'][0];
					$start_time = $details['event_start_time'][0];
					$location = $details['event_location'][0];
					
					if ( $end_date == 0 ) {
						$show_dates = utf8_encode(strftime('%e.%m.%Y', strtotime($start_date)));
					} else {
						$show_dates = utf8_encode(strftime('%e.%m', strtotime($start_date))) . ' — ' .utf8_encode(strftime('%e.%m.%Y', strtotime($end_date)));
					};
					
					if ( $start_time != 0 ) {
						$show_time = ' kell ' . $start_time;
					} else {
						$show_time = '';
					}
						
				?>
			
				<aside class="caledaraside">
					<h4 class="caledartitle">
						<span class="bg"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>
					</h4>
					<p>
						<span class="bg">
						<?php echo $show_dates; ?>
						<?php echo $show_time; ?>
						<br /><?php echo $location; ?>
						</span>
					</p>
				</aside>
			
				<?php unset($start_date, $end_date, $start_time, $location, $show_dates, $show_time); ?>
			
			<?php endforeach;
				$post = $tmp_post;
				wp_reset_postdata();
			?>
				<aside>
					<a href="<?php echo home_url() . '/oppimine/kalender/?dept_page=' . $category[0]->slug; ?>&aeg=toimunud" class="bg"><?php echo __( 'Vaata toimunud sündmusi', 'artun2012'); ?></a>
				</aside>
			<?php
		}
	}
	
	
	// Displays posts for specific pages
	function eka_the_posts() {
		global $post, $wp_query;
				
		// checks wether adding/showing posts is enabled on this page
		if ( get_post_meta($post->ID, 'page_add_posts', true) != 1 ) {

			?>

			<div class="postscontent">

				<?php

				// The "add a new post" form
				

				if ( get_page_template_slug( $post->ID ) == 'page-eriala-esileht-new.php' ) {
					echo '<div class="column_left">';
						eka_add_post_form();
						eka_add_event_form();
					echo '</div>';
				} else {
					eka_add_post_form();
				}

				// Arguments for getting the posts
				$args = array(
					'show_on_page'		=> $post->ID,
					'posts_per_page'	=> 11 // only 10 will be displayed, +1 is to check wether we need the "see more" link
				);

				// Check if tagging is enabled on the page
				if ( get_post_meta( $post->ID, 'eka_disallow_tagging', true ) != 1 ):

					// Gets the tag info, if one is set
					if( $wp_query->query_vars['tag'] != '' ) {
						$selected_tag = $wp_query->query_vars['tag'];
							
						// Gets this tag item
						$tagObj = get_term_by( 'slug', $selected_tag, 'post_tag' );
							
						// Modify the arguments
						$args['tag_id'] = $tagObj->term_id;

						// Update the $selected_tag variable, for security reasons
						$selected_tag = $tagObj->slug;
					} else {
						$selected_tag = '';
						$args['tag_id'] = '';
					}

					function current_tag( $current_tag, $selected_tag ) {
						if( $selected_tag == $current_tag ) {
							echo 'current_page_item';
						}
					} 

					?>

					<ul class="tagslist" id="tagslist">
						<?php

						// Gets all the tags related to posts shown on this page

						// First checks if there are any posts at all
						$this_page = term_exists( (string)$post->ID, 'show_on_page' );
						if ( $this_page ): 

							$tags = get_category_tags( array( 
								'categories' 	=> $this_page['term_id'], 
								'taxonomy' 		=> 'show_on_page',
								'term' 			=> 'post_tag',
								'post_type' 	=> 'post' 
							) );

							if ( !empty( $tags ) ):	?>

								<li class="tagoption <?php current_tag( 'all', $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag="><?php echo __( 'Kõik', 'artun2012' ); ?></a></li>
							
							<?php endif;
											
							foreach ( $tags as $tag ) : ?>
								<li class="tagoption <?php current_tag( $tag->slug, $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a></li>
							<?php endforeach; ?>

							<?php
								if ( count( $tags ) >= 10 ) :
							?>
								<li class="moretags" id="moretags"><?php echo __('rohkem silte...', 'artun2012'); ?></li>
							<?php endif; ?>
						<?php endif; ?>
					</ul><!--/.taglist-->

				<?php endif; ?>

				<div id="eka_posts_container" class="clearfix">

					<?php 
						// Load and display the posts
						$showmore = eka_load_posts( $args );
					?>

				</div><!--/#eka_posts_container-->
				<?php if ( $showmore ) : ?>
					<span data-page="<?php echo $post->ID; ?>" data-tag="<?php echo $args['tag_id']; ?>" class="load_more_posts" id="load_more_posts"><?php echo __('Lae rohkem...', 'artun2012'); ?></span>
				<?php endif; ?>

			</div><!--/.postscontent-->

			<?php
			
		} // if posts enabled
	}

	function eka_load_posts( $args = array() ) {
		global $post;

		$args['posts_per_page'] = 11;

		// Get arguments from the ajax request, if there is one
		if ( isset( $_POST['offset'] ) ) {
			$is_ajax = 1;
			$args['offset'] = $_POST['offset'];
			$args['posts_per_page'] = 10;
			$postID = $_POST['page'];
		} else {
			$is_ajax = 0;
			$postID = $post->ID;
		}

		if ( isset( $_POST['page'] ) ) {
			$args['show_on_page'] = $_POST['page'];
		}

		if ( isset( $_POST['tag'] ) ) {
			$args['tag_id'] = $_POST['tag'];
		}

		// Store the default post to prevent weirdness
		$tmp_post = $post;

		if( function_exists( 'pll_current_language' ) ){	
			$args['lang'] = pll_current_language();
		}


		if ( get_page_template_slug( $postID ) == 'page-eriala-esileht-new.php' ) {
			$args['post_type'] = 'any';
		}

		$showposts = get_posts( $args );

		if ( count( $showposts ) > 10 ) {
			$showmore = 1;
			array_pop( $showposts );
		} else {
			$showmore = 0;
		}

		if ( get_page_template_slug( $postID ) == 'page-eriala-esileht-new.php' ) {

			foreach( $showposts as $post ) : setup_postdata( $post );
		
				$url = get_post_meta($post->ID, 'show_front_page_url', true);
				
				if ($url) { 
					// checks if the link has a http:// at start, adds it if not
					if( strpos($url, "http") === false ) {
						$url = 'http://'.$url;
					}
					$target = "_blank";
				} else {
					$url = $post->guid;
					$target = "_self";
				}
				if ( $post->post_type == 'eka_calendar' ) {
					$start_date = get_post_meta($post->ID, 'event_start_date', true);
					$end_date = get_post_meta($post->ID, 'event_end_date', true);
					$start_time = get_post_meta($post->ID, 'event_start_time', true);
					$location = get_post_meta($post->ID, 'event_location', true);
					
					if ( $end_date == 0 ) {
						$content = utf8_encode(strftime('%e. %B %Y', strtotime($start_date)));
					} else {
						$content = utf8_encode(strftime('%e. %B %Y', strtotime($start_date))) . ' — <br class="breakable">' .utf8_encode(strftime('%e. %B %Y', strtotime($end_date)));
					};
					
					if ( $start_time != 0 ) {
						$content .= ' <br class="breakable">' . __('Kell', 'artun2012') . ' ' . $start_time;
					} 
					$content .= get_post_meta($post->ID, 'event_location', true);

				} else {
					$content = eka_get_the_excerpt(140);
				}

				$imgargs = array(
					'alt'   => trim(strip_tags( $post->post_title )),
					'title' => trim(strip_tags( $post->post_title )),
				);
			
				
				// gets the first image associated with this post				
				$showimage = eka_attachment( $post->ID );
				if ( $showimage ):	?>
					<div class="imagewrapper deptimage">
						<div>
							<div class="imagebox"> 
								<?php echo eka_get_attachment_image( $showimage, $imgargs ); ?>
								<div class="imagedetails click_link" data-post_guid="<?php echo $url; ?>" target="<?php echo $target; ?>">
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">... <?php echo __( 'loe edasi', 'artun2012' ); ?></div>
								</div><!--/.imagedetails-->
							</div><!--/.imagebox-->
						</div>
						<h5><?php echo trim(strip_tags( $post->post_title )); ?></h5>
					</div><!--/.imagewrapper--> 
				<?php else: ?>
					<div class="imagewrapper">
						<div>
							<div class="imagebox"> 
								<div class="textimage">
									<h5>
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo trim(strip_tags( $post->post_title )); ?></a>
									</h5>
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>">... <?php echo __( 'loe edasi', 'artun2012' ); ?>&nbsp;</a>
									</div>
								</div><!--/.textimage-->
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->

				<?php endif; ?>
			<?php endforeach;

		} else {

			foreach( $showposts as $post ) : setup_postdata( $post ); ?>
				<article class="clearfix">
					<div class="articlethumb" data-post="<?php echo $post->ID; ?>">
						<?php eka_first_image($post->ID); ?>
					</div>
					<h3 class="titleholder">
						<span class="articletitle titlebox editable-<?php echo $post->ID; ?>" data-post="<?php echo $post->ID; ?>"><?php the_title(); ?></span>
						<span class="commentcount">
							<?php comments_popup_link('', '(1)', '(%)', '', ''); ?>
						</span>
					</h3>
					<div class="articlecontent clearfix">

						<div id="gallery-<?php echo $post->ID; ?>" class="postrollgallery clearfix"></div>

						<div class="contentbox editable-<?php echo $post->ID; ?>">
							<?php the_content(); ?>
						</div>

						<div class="details">
							<?php echo __('Lisatud', 'artun2012'); ?> <time datetime="<?php the_time( 'Y-m-D' ); ?>" pubdate><?php the_time('j.m.Y'); ?></time> 
							<a href="<?php the_permalink(); ?>"><?php echo __('Püsilink', 'artun2012'); ?></a> 
							<?php if ( eka_current_user_has_rights() ) : ?>
								<div class="adminpost">

									<?php echo getPostViews(get_the_ID()); ?>

									<?php eka_post_categories(); ?>

									<span class="js_contentsave editpostlink inline primary" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-yes"></span> <?php echo __( 'Salvesta', 'artun2012' ); ?></span>
									<span class="js_contentundo editpostlink inline" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-no-alt"></span> <?php echo __( 'Tühista', 'artun2012' ); ?></span>
									<span class="js_contentedit editpostlink inline" data-edit-id="<?php echo $post->ID; ?>"><span class="dashicons dashicons-edit"></span></span>

									<?php if ( current_user_can( 'manage_options' ) ) : ?>
										<a class="editpostlink inline" href="<?php echo get_edit_post_link( $post->ID ); ?>"><span class="dashicons dashicons-admin-settings"></span></a>
									<?php endif; ?>

									<?php echo wp_delete_post_link(); ?>
								</div><!--/.adminpost-->
							<?php endif; ?>
							<?php eka_share( urlencode( get_permalink() ), $post->post_title ); ?>
						</div><!--/.details-->

					</div>
				</article>
				<?php setPostViews(get_the_ID()); ?>
			<?php endforeach;

		}

		wp_reset_postdata();

		$post = $tmp_post;

		if ( $is_ajax ) {
			die();	
		}

		// Returns 1 in case more than 10 posts were loaded (for read more button)
		return $showmore;
		
	} 

	function eka_load_contacts() {
		global $post;
		
		if ( isset( $_POST['cat'] ) ) {

			//$et_cat_id = icl_object_id( (int)$_POST['cat'], 'category', true, 'et' );
			$et_cat_id = pll_get_term( (int)$_POST['cat'], 'et' );

			$contacts = get_posts( array( 
				'post_type' 	=> 'eka_contacts',
				'numberposts'   => -1,
				'category__in' 	=> array( $et_cat_id ),
				'orderby'       => 'menu_order',
				'order'         => 'ASC',
				'meta_query'    => array(
					array(
						'key'       => 'contact_list',
						'value'     => 1,
						'compare'   => '=='
						)
					)) 
				);

			if ( current_user_can('edit_posts') ) {
				echo '<ul class="list_contact_row sortable clearfix">';
			} else {
				echo '<ul class="list_contact_row visitor clearfix">';
			}

			// begin the posts (contacts) loop
			foreach( $contacts as $post ) : setup_postdata( $post ); ?>
				<li class="<?php echo $post->ID; ?> hcard contact_card">
					<h4 class="contact_title">
						<a href="<?php the_permalink(); ?>" class="fn bg"><?php the_title(); ?></a>
					</h4>
					<p>
						<?php if ( pll_current_language() == 'et' ) : ?>
							<span class="bg"><?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?></span>
						<?php else: ?>
							<span class="bg"><?php editable_post_meta( $post->ID, 'contact_role_en', 'input' ); ?></span>
						<?php endif; ?>
					</p>
					<p>
						<span class="tel bg"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span>
						<br />
						<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email bg">
							<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
						</a>
					</p>
					<p class="contact_portrait">
						<a href="<?php the_permalink(); ?>"><?php eka_first_image( $post->ID ); ?></a>
					</p>
					<?php if ( current_user_can('edit_posts') ) : ?>
						<div class="edit">										
							<span class="bg"><span class="sort handle"><?php echo __( 'Sorteeri', 'artun2012' ); ?></span> <?php echo wp_delete_post_link(); ?></span>
						</div>
					<?php endif; ?>
				</li>
			<?php endforeach; // posts (contacts) loop

			echo '</ul>';

		} // if cat is set	

		die();
	}

	function eka_load_frontpage() {
		global $post;

		// Store the default post to prevent weirdness
		$tmp_post = $post;

		list( $today_year, $today_month, $today_day ) = preg_split( '([^0-9])', current_time('mysql') );
			
		$showposts = get_posts( array(
			'posts_per_page'	=> 10,
			'post_type'			=> 'any',
			'offset'			=> (int) $_POST['offset'],
			'lang' 				=> pll_current_language(), 
			'meta_query'		=> array(
				array(
					'key' => 'show_front_page',
					'value' => '1'
				),
				array(
					'key' => 'show_front_page_enddate',
					'value' => $today_year . $today_month . $today_day,
					'compare' => '>='
				), 
				array(
					'key' => 'show_sticky',
					'value' => '1',
					'compare' => '!='
				)
			)
		) );

		foreach ( $showposts as $post ) { 

			setup_postdata( $post );
			
			$imgargs = array(
				'alt'   => trim( strip_tags( $post->post_title ) ),
				'title' => trim( strip_tags( $post->post_title ) )
			);

			// gets the first image associated with this post				
			$showimage = eka_attachment( $post->ID );

			if ( $showimage ) {
				$image = eka_get_attachment_image( $showimage, $imgargs );
			}

			/*if ( $post->post_content == 'Lisa sisutekst' ) {
				$content = '';
			} else {
				$content = $post->post_content;
			}*/

			?>
			<div class="imagewrapper">
				<div>
					<div class="imagebox"> 
						<?php

						if ( $post->post_type == 'post' || $post->post_type == 'eka_calendar' ) {
							
							$url = get_post_meta( $post->ID, 'show_front_page_url', true );
							
							if ( $url ) { 
								// checks if the link has a http:// at start, adds it if not
								if( strpos( $url, "http" ) === false ) {
									$url = 'http://' . $url;
								}
								$target = "_blank";
							} else {
								$url = $post->guid;
								$target = "_self";
							}

							if ( $showimage ) :	?>
								<?php echo $image; ?>
								<div class="imagedetails click_link" data-post_guid="<?php echo $url; ?>" target="<?php echo $target; ?>">
									<h5><?php echo trim(strip_tags( $post->post_title )); ?></h5>
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">... <?php echo __( 'loe edasi', 'artun2012' ); ?></div>	
								</div>
							<?php else: ?>
								<div class="textimage">
									<h5><a href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo trim(strip_tags( $post->post_title )); ?></a></h5>
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>">... <?php echo __( 'loe edasi', 'artun2012' ); ?>&nbsp;</a>
									</div>
								</div>
							<?php endif; // if $showimage

						}  ?>

					</div><!--/.imagebox-->
				</div>
			</div><!--/.imagewrapper-->

		<?php } // foreach $showposts
		
		$post = $tmp_post;

		die();

	}

	function eka_load_postgallery( $parentpost = 0 ) {
		global $post;

		// Get arguments from the ajax request, if there is one
		if ( isset( $_POST[ 'parentpost' ] ) ) {
			$is_ajax = 1;
			$parentpost = $_POST[ 'parentpost' ];
		} else {
			$is_ajax = 0;
			$tmp_post = $post;
		}
		
		// Gets the images related to this post
		$images = get_posts( array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_parent' => $parentpost,
			'orderby' => 'menu_order title',
			'order' => 'ASC'
		) );
		
		if ( !empty( $images ) ) {
									
			foreach ( $images as $post ) { 

				
				setup_postdata( $post );
					
				$imgargs = array(
					'alt'   => trim(strip_tags( $post->post_title )),
					'title' => trim(strip_tags( $post->post_title )),
				);

				$class = "imagewrapper";

				// Check if its a gif. If it is, get the full size image instead (to retain animation).
				if ( $post->post_mime_type == 'image/gif') {
					$gifimage = wp_get_attachment_image_src( $post->ID, 'full', 0 );
					$img = '<img src="' . $gifimage[0] . '" alt="' . $imgargs['alt'] . '">';
				// Check if there is only one image to display. Get large image instead in this case.
				} else if ( count( $images ) == 1 ) {
					$largeimage = wp_get_attachment_image_src( $post->ID, 'large', 0 );
					$img = '<img src="' . $largeimage[0] . '" alt="' . $imgargs['alt'] . '">';
					$class = "imagewrapperlarge";
				} else {
					$img = eka_get_attachment_image( $post, $imgargs );
				}    	

				if ( $img ): ?>

					<div class="<?php echo $class; ?>" id="<?php echo $post->ID ?>">
						<div>
							<div class="imagebox">
									
								<?php echo $img; ?>            
								<div class="imagedetails click_attachment" data-post_id="<?php echo $post->ID ?>">
									<p class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true"><?php the_title(); ?></p>
									<?php if ( eka_current_user_has_rights() ) : ?>
										<p class="adminimage">
											<?php wp_edit_attachment_link(); ?>
											<?php wp_delete_attachment_link(); ?>
										</p>
									<?php endif; ?>                        
								</div><!--/.imagedetails-->
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->  
				<?php endif;

			}

		}

		if ( $is_ajax ) {
			die();	
		} else {
			wp_reset_postdata();
			$post = $tmp_post;
		}

	}

	// For loading items in the curated content section
	function eka_load_curated() {
		global $post;

		$cat = implode( ',', get_the_author_meta( 'user_categories', get_current_user_id() ) );

		$args = array(
			'posts_per_page'   => 500,
			'offset'           => 0,
			'category'         => $cat,
			'suppress_filters' => 0 // for WPML
		);

		if ( isset( $_POST['type'] ) ) {
			$args['post_type'] = $_POST['type'];
		}

		if ( isset( $_POST['orderby'] ) ) {
			//$args['orderby'] = $_POST['orderby'];
			$args['orderby'] = 'post_title';
		}

		$showposts = get_posts( $args );

		if ( ICL_LANGUAGE_CODE == 'et') {
			setlocale( LC_ALL, "et_EE.UTF-8" ); 
		}

		$tmp_post = $post; 

		foreach( $showposts as $post ) : setup_postdata( $post ); 

			$updater = get_post_meta( $post->ID, 'post_last_editor', true );

			// check if we have an updater and wether this post has been updated
			if ( $updater && ( strtotime( $post->post_date ) - strtotime( $post->post_modified ) !== 0 ) ) {
				$updater_info = get_userdata( $updater );
				$updater_name = $updater_info->user_firstname . ' ' . $updater_info->user_lastname;
			} else {
				$updater_info = get_userdata( $post->post_author );
				$updater_name = $updater_info->user_firstname . ' ' . $updater_info->user_lastname;
			}

			?>

			<li class="clearfix">
				<div style="width:35%">
					<a href="<?php the_permalink(); ?>"><?php 

						if ( $post->post_type == 'page' ) {
							echo get_the_title( $post->post_parent ) . ' / ';
						}
						echo $post->post_title;

					?></a>
				</div>
				<div style="width:10%">
					<?php echo getPostViews( $post->ID ); ?>
				</div>
				<div style="width:20%">
					<?php echo strftime( '%e. %B %Y', strtotime( $post->post_modified ) ); ?>
				</div>
				<div style="width:%">
					<?php echo $updater_name; ?>
				</div>
			</li>
		<?php endforeach;

		$post = $tmp_post; 

		die();	
		
	} 


	function eka_load_gallery( $args = array() ) {
		global $post;

		$args['posts_per_page'] = -1;
		$args['post_type'] = "eka_project";
		$args['orderby'] = array( 'menu_order' => 'ASC', 'date' => 'DESC' );

		if ( !isset( $args['show_on_page'] ) ) {
			$args['show_on_page'] = $post->ID;
		}

		if ( isset( $_POST['tag'] ) ) {
			$args['tag_id'] = $_POST['tag'];
		} 
		

		// Store the default post to prevent weirdness
		$tmp_post = $post;

		if( function_exists( 'pll_current_language' ) ){	
			$args['lang'] = pll_current_language();
		}

		$showposts = get_posts( $args );

		foreach( $showposts as $post ) : setup_postdata( $post );
			
			// gets the first image associated with this post				
			$showimage = eka_attachment( $post->ID );
			$imgargs = array(
				'alt'   => trim(strip_tags( $post->post_title )),
				'title' => trim(strip_tags( $post->post_title )),
			);
			
			if ( $showimage ):	?>
				<div class="<?php echo $post->ID; ?> imagewrapper deptimage">
					<div>
						<div class="imagebox"> 
							<?php echo eka_get_attachment_image( $showimage, $imgargs ); ?>
							<div class="imagedetails click_link" data-post_guid="<?php echo $post->guid; ?>" target="_self">
								<div class="imagecontent">
									<?php echo eka_get_the_excerpt(140); ?>
								</div>
								<div class="readmore">... <?php echo __( 'loe edasi', 'artun2012' ); ?></div>
							
								<?php if ( eka_current_user_has_rights() ) : ?>
									<p class="adminimage">
										<span class="sort handle imageedit"><span class="dashicons dashicons-randomize"></span></span>
										<?php echo wp_delete_post_link(); ?>
									</p>
								<?php endif; ?>

							</div><!--/.imagedetails-->
						</div><!--/.imagebox-->
					</div>
					<h5><?php echo trim(strip_tags( $post->post_title )); ?></h5>
				</div><!--/.imagewrapper--> 
			<?php else: ?>
				<div class="<?php echo $post->ID; ?> imagewrapper">
					<div>
						<div class="imagebox"> 
							<div class="textimage">
								<h5>
									<a href="<?php echo $post->guid; ?>" target="_self"><?php echo trim(strip_tags( $post->post_title )); ?></a>
								</h5>
								<div class="imagecontent"><?php echo eka_get_the_excerpt(140); ?></div>
								<div class="readmore">
									<a href="<?php echo $post->guid; ?>" target="_self">... <?php echo __( 'loe edasi', 'artun2012' ); ?>&nbsp;</a>
								</div>
							</div><!--/.textimage-->
						</div><!--/.imagebox-->
					</div>
				</div><!--/.imagewrapper-->
			<?php endif; ?>

		<?php endforeach;
		wp_reset_postdata();

		$post = $tmp_post;

	}


	function eka_first_image( $id ) {
		$images = get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $id );
		 
		if ( $images ) {
		 
			$all_images = array_keys( $images );
			 
			// Get the first image attachment from post
			$image = $all_images[0];
			 
			// Get the thumbnail url for the attachment
			$thumb_url = wp_get_attachment_thumb_url( $image );
			 
			// Build the <img> string
			echo '<img src="' . $thumb_url . '" alt="Pilt" />';

		}
	}


	/* ==============================================================================================================
	
	Using Wordpress walker for creating a hierarchical category select menu.
	
	============================================================================================================== */ 

	function eka_cat_select( $showcurrent = 1, $cats = array() ) {
		
		// modify the walker		
		if ( !class_exists('Walker_Cat_Selection') ) {
			class Walker_Cat_Selection extends Walker_Category {
				function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
					extract($args);
					
					$selected = '';
					if ( !empty( $current_cats ) ) {
						if( in_array( $category->term_id, $current_cats ) ) {
							$selected = ' checked="checked"';
						}
					}
			
					$cat_name = esc_attr( $category->name );
					$cat_name = apply_filters( 'list_cats', $cat_name, $category );
					$link = '<input type="checkbox" name="cats[]" value="' . $category->term_id . '" id="cat-sel-' . $category->term_id . '"' . $selected . ' > <label for="cat-sel-' . $category->term_id . '" class="display">' . $cat_name . '</label>';
					$output .= "\t<li>$link\n";
				}
			}
		}		
		
		
		// if we want to show currently selected categories, get their ids and send those as well to wp_list_categories
		if ( $showcurrent == 1 ) {
			foreach( (get_the_category()) as $category ) { 
				$cats[] = $category->cat_ID; 
			}
		}
			
		return '<div class="catselect"><ul>' . wp_list_categories( array(
			'exclude'		=> 1,
			'hide_empty'	=> 0,
			'title_li'		=> '',
			'walker'		=> new Walker_Cat_Selection(),
			'current_cats' 	=> $cats,
			'echo'			=> 0
		) ) . '</ul></div>';
				
	}


	/* ==============================================================================================================
	
	Gets tags by category
	
	============================================================================================================== */ 

	function get_category_tags($args) {
		global $wpdb;			
		
		return $wpdb->get_results( $wpdb->prepare( "
			SELECT DISTINCT terms2.term_id as term_id, terms2.name as name, terms2.slug as slug
			FROM
				$wpdb->posts as p1
				LEFT JOIN $wpdb->term_relationships as r1 ON p1.ID = r1.object_ID
				LEFT JOIN $wpdb->term_taxonomy as t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
				LEFT JOIN $wpdb->terms as terms1 ON t1.term_id = terms1.term_id,
	
				$wpdb->posts as p2
				LEFT JOIN $wpdb->term_relationships as r2 ON p2.ID = r2.object_ID
				LEFT JOIN $wpdb->term_taxonomy as t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
				LEFT JOIN $wpdb->terms as terms2 ON t2.term_id = terms2.term_id
			WHERE
				t1.taxonomy = '%s' AND p1.post_status = 'publish' AND terms1.term_id IN (".$args['categories'].") AND
				t2.taxonomy = '".$args['term']."' AND p2.post_status = 'publish' AND p2.post_type = '".$args['post_type']."'
				AND p1.ID = p2.ID
			ORDER by name
		", array( $args['taxonomy'] ) ) );

	}


	/* ==============================================================================================================
	
	Handles front-end file uploading
	
	============================================================================================================== */ 
	
	// individual image upload
	function insert_attachment($file_handler,$post_id='') {
 
		// check to make sure its a successful upload
		// if ($_FILES[$file_handler]['error'] !== UPLOAD_ERR_OK) __return_false();
		 
		require_once(ABSPATH . "wp-admin" . '/includes/image.php');
		require_once(ABSPATH . "wp-admin" . '/includes/file.php');
		require_once(ABSPATH . "wp-admin" . '/includes/media.php');
		 
		$attach_id = media_handle_upload( $file_handler, $post_id );
		 
		return $attach_id;
	}
	
	// sanitize estonian characters in file names
	add_filter('sanitize_file_name', 'eka_sanitize_estonian_chars', 10);

	function eka_sanitize_estonian_chars( $filename ) {
		$estonian_chars = array( '/õ/', '/ä/', '/ö/', '/ü/', '/Õ/', '/Ä/', '/Ö/', '/Ü/' );
		$sanitized_chars = array( 'o', 'a', 'o', 'u', 'O', 'A', 'O', 'U' );
		$filename = preg_replace( $estonian_chars, $sanitized_chars, $filename );
		return preg_replace("/[^A-Za-z0-9 \-\.]/", '', $filename);
	}	


	/* ==============================================================================================================
	
	Adding posts and attachments from front-end
	
	============================================================================================================== */ 

	// Creates the ADD IMAGE TO PAGE form and takes care of submission.
	
	function eka_add_images_to_post() {
		
		if( isset( $_POST['new-image-nonce'] ) && wp_verify_nonce( $_POST['new-image-nonce'], 'new-image' ) ) {

			$parent = (int) $_POST['eka_parent'];
						
			if ( $_POST['eka_dnd_files'] ) {
				$attachments = json_decode(base64_decode($_POST['eka_dnd_files']), true);
								
				foreach ($attachments as $attach_id) {
					wp_update_post( array( 'ID' => $attach_id, 'post_parent' => $parent ));
				}
								
			} else if ( $_FILES ) {
				$files = $_FILES['upload_attachment'];
				
				foreach ($files['name'] as $key => $value) {
					if ($files['name'][$key]) {
						$file = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						 
						$_FILES = array("upload_attachment" => $file);
						 
						foreach ($_FILES as $file => $array) {
							insert_attachment( $file, $parent );
						}
					}
				}
			}

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, uued pildid lisatud!', 'artun2012');
			
		}
				 
	} // eka_add_images_to_post

	function eka_add_images_form() {
		global $post;		

		if ( eka_current_user_has_rights() ) : ?>
			<div class="addimage">
				<span class="addimagelink" id="addImageLink"><span class="dashicons dashicons-images-alt2"></span> <?php echo __( 'Lisa lehele pilte', 'artun2012' ); ?></span>
				<div class="addimageform" id="addImageForm">
				<form id="new_image" name="new_image" method="post" action="" enctype="multipart/form-data">
					<div class="dropArea fileuploading">
						<label for="addImages" class="addupload"><span class="dashicons dashicons-format-gallery"></span><br/><?php echo __( 'Piltide lisamiseks lohista nad kasti või kliki siia.', 'artun2012' ); ?><span class="small"><br /><?php echo __( 'JPG, PNG või GIF failid', 'artun2012' ); ?></span></label><br />
						<input id="addImages" type="file" name="upload_attachment[]" multiple style="visibility: hidden;">
						<div id="listAddImages"></div>
					</div><!--/#drop-area-->
					<div style="padding-top:20px;" id="new_image_submit">
						<input type="submit" name="eka_submit" value="<?php echo __( 'Lisa pildid', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton" id="delImageLink"><?php echo __( 'tühista', 'artun2012' ); ?></span>
						<input type="hidden" name="eka_submit_type" value="add_new_image" />
						<input type="hidden" name="eka_dnd_files" value="" class="eka_dnd_files" />
						<input type="hidden" name="eka_parent" value="<?php echo $post->ID; ?>" /> 
						<?php wp_nonce_field( 'new-image', 'new-image-nonce' ); ?>
					</div>
					<div class="new_post_loading" id="new_image_loading">
						<?php echo __( 'Laen', 'artun2012' ); ?>...
					</div>
				</form>
				</div><!--/#addImageForm-->
			</div><!--/.addimage-->
		<?php endif;		
	} // eka_add_images_form


	// Creates the ADD CONTACT form and takes care of submission.
	
	function eka_add_contact() {

		if( isset( $_POST['new-contact-nonce'] ) && wp_verify_nonce( $_POST['new-contact-nonce'], 'new-contact' ) ) {
		 
			$tags = $_POST['eka_post_tags'];
			$cats = $_POST['cats'];

			if( isset( $_POST['contact_list'] ) && $_POST['contact_list'] == 1 ) {
				$contact_list = 1;
			} else {
				$contact_list = 0;
			}; 
		 
			// Add the content of the form to $post as an array
			$post = array(
				'post_title'	=> wp_strip_all_tags( $_POST['contact_title'] ),
				'post_content'	=> $_POST['eka_content'],
				'tags_input'	=> $tags,
				'post_status'	=> 'publish',
				'post_type'		=> 'eka_contacts'
			);
			$new_post_id = wp_insert_post( $post );
			
			wp_set_post_terms( $new_post_id, $cats, 'category'); // adds categories
			add_post_meta($new_post_id, 'contact_email', wp_strip_all_tags( $_POST['contact_email'] ));
			add_post_meta($new_post_id, 'contact_phone', wp_strip_all_tags( $_POST['contact_phone'] ));
			add_post_meta($new_post_id, 'contact_mobile', wp_strip_all_tags( $_POST['contact_mobile'] ));
			add_post_meta($new_post_id, 'contact_role', wp_strip_all_tags( $_POST['contact_role'] ));
			add_post_meta($new_post_id, 'contact_role_en', wp_strip_all_tags( $_POST['contact_role_en'] ));
			//add_post_meta($new_post_id, 'contact_info', wp_strip_all_tags( $_POST['contact_info'] ));

			add_post_meta($new_post_id, 'contact_www', wp_strip_all_tags( $_POST['contact_www'] ));
			add_post_meta($new_post_id, 'contact_list', $contact_list );
			 
			if ( $_POST['eka_dnd_files'] ) {
				$attachments = json_decode(base64_decode($_POST['eka_dnd_files']), true);
								
				foreach ($attachments as $attach_id) {
					wp_update_post( array( 'ID' => $attach_id, 'post_parent' => $new_post_id ));
				}
								
			} else if ( $_FILES ) {
				$files = $_FILES['upload_attachment'];
				foreach ($files['name'] as $key => $value) {
					if ($files['name'][$key]) {
						$file = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						 
						$_FILES = array("upload_attachment" => $file);
						 
						foreach ($_FILES as $file => $array) {
							$newupload = insert_attachment($file,$new_post_id);
						}
					}
				}
			}

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, uus inimene lisatud!', 'artun2012');
			
		}
	
	} // eka_add_contact


	function eka_add_contact_form( $in_contact_list = 0 ) {
		global $post;
		
		if ( eka_current_user_has_rights() ) : ?>
			<div class="addpost column_left">
			<span class="addpostlink" id="addPostLink"><span class="dashicons 
dashicons-id-alt"></span> <?php echo __( 'Lisa uus inimene', 'artun2012' ); ?></span>
			<div class="addpostform" id="addPostForm">
			<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data">
				<p>
					<input type="text" name="contact_title" id="addpost_title" class="addpost_title required" placeholder="<?php echo __( 'Sisesta nimi*', 'artun2012' ); ?>">
				</p>
				<div class="dropArea fileuploading">
					<label for="addFiles" class="display addupload"><span class="dashicons dashicons-id"></span><br/><?php echo __( 'Pildi lisamiseks lohista see kasti või kliki siia', 'artun2012' ); ?></label><br />
					<input id="addFiles" type="file" name="upload_attachment[]" multiple style="visibility: hidden;">
					<div id="listAddFiles"></div>
				</div>
				<div class="addpost_content notclicked editable-newpost" id="addpost_content" style="margin-bottom: 1em"><p><?php echo __( 'Lisa kirjeldus', 'artun2012' ); ?></p></div>
				<?php 
					if ( $in_contact_list == 1 ) {
						echo '<div>' . eka_cat_select( 0 ) . '</div>';
					} else {
						$category = get_the_category(); 
						echo '<input type="hidden" name="cats" id="cats" value=" ' . $category[0]->term_id . '"/>';
					}
				?>
				<p>
					<input type="text" name="contact_role" id="contact_role" placeholder="<?php echo __( 'Amet', 'artun2012' ); ?>" class="addpost_field" />
					<input type="text" name="contact_role_en" id="contact_role_en" placeholder="<?php echo __( 'Amet inglise keeles', 'artun2012' ); ?>" class="addpost_field" />
				</p>
				<p>
					<input type="text" name="contact_email" id="contact_email" class="addpost_field" placeholder="<?php echo __( 'E-posti aadress', 'artun2012' ); ?>" />
					<input type="text" name="contact_phone" id="contact_phone" class="addpost_field" placeholder="<?php echo __( 'Telefoninumber', 'artun2012' ); ?>" />
					<input type="text" name="contact_mobile" id="contact_mobile" placeholder="<?php echo __( 'Mobiiltelefoni number', 'artun2012' ); ?>" class="addpost_field" />
				</p>
				<p>
					<input type="text" name="contact_www" id="contact_www" placeholder="<?php echo __( 'Veebileht', 'artun2012' ); ?>" class="addpost_field" />
				</p>

				<p class="addpost_details">
					<input type="text" name="eka_post_tags" id="post_tags" class="addpost_field" placeholder="<?php echo __( 'Lisa silte (kasutatakse alammenüü loomiseks)', 'artun2012' ); ?>" />
				</p>

				<?php if ( $in_contact_list == 1 ) : ?>
						<input type="hidden" name="contact_list" id="contact_list" value="1" />
				<?php else : ?>
					<p>
						<input type="checkbox" name="contact_list" id="contact_list" value="1" />
						<label for="contact_list" class="display"><?php echo __( 'Näita EKA veebi üldisel kontaktilehel', 'artun2012' ); ?></label>
					</p>
				<?php endif; ?>

				<div style="padding-top:20px;" id="new_post_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Lisa kontakt', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton" id="delPostLink"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="add_new_contact" />
					<input type="hidden" name="eka_dnd_files" value="" class="eka_dnd_files" />
					<textarea name="eka_content" id="addpost_submit_content"  style="display:none;"></textarea>
					<?php wp_nonce_field( 'new-contact', 'new-contact-nonce' ); ?>
				</div>
				<div class="new_post_loading" id="new_post_loading">
					<?php echo __( 'Laen', 'artun2012' ); ?>...
				</div>
			</form>
			</div><!--/#addPostForm-->
			</div><!--/.addpost-->
		<?php endif;		
	} // eka_add_contact_form



	// Creates the ADD PROJECT form and takes care of submission.
	
	function eka_add_project() {

		if( isset( $_POST['new-project-nonce'] ) && wp_verify_nonce( $_POST['new-project-nonce'], 'new-project' ) ) {
		 
			$tags = $_POST['eka_post_tags'];
			$cats = array_map( 'intval', explode( ',', $_POST['eka_cat'] ) ); // cat IDs into array of integers (for wp_set_terms)
			$pages = array( (string)$_POST['eka_on_page'] );

			// Gets and checks wether user wants to show the post on front page and ticker
			if( isset( $_POST['eka_deptpage_show'] ) && $_POST['eka_deptpage_show'] == 1 ) {

				// Find out which ancestor is the dept front page and get its ID. Add it to the $pages array.
				$ancestors = get_ancestors( $_POST['eka_on_page'], 'page' );
				foreach ( $ancestors as $ancestor ) {
					if ( get_page_template_slug( $ancestor ) == 'page-eriala-esileht-new.php' ) {

						$pages[] = (string)$ancestor; 

					}
				}
			}
		 
			// Add the content of the form to $post as an array
			$new_post_id = wp_insert_post( array(
				'post_title'	=> wp_strip_all_tags( $_POST['eka_title'] ),
				'post_content'	=> $_POST['eka_content'],
				'tags_input'	=> $tags,
				'post_status'	=> 'publish',
				'post_type'		=> 'eka_project'
			) );
			
			wp_set_post_terms( $new_post_id, $cats, 'category' ); // adds categories
			wp_set_post_terms( $new_post_id, $pages, 'show_on_page' ); // makes this post appear on certain page

			// Polylang
			if ( function_exists( 'pll_set_post_language' ) ) {
				pll_set_post_language( $new_post_id, pll_current_language() );
			}
			 
			if ( $_POST['eka_dnd_files'] ) {
				$attachments = json_decode( base64_decode( $_POST['eka_dnd_files'] ), true );
								
				foreach ($attachments as $attach_id) {
					wp_update_post( array( 'ID' => $attach_id, 'post_parent' => $new_post_id ));
				}
								
			} else if ( $_FILES ) {

				$files = $_FILES['upload_attachment'];

				foreach ($files['name'] as $key => $value) {
					if ($files['name'][$key]) {
						$file = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						 
						$_FILES = array("upload_attachment" => $file);
						 
						foreach ($_FILES as $file => $array) {
							$newupload = insert_attachment($file,$new_post_id);
						}
					}
				}
			}

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, uus galerii lisatud!', 'artun2012');
			
		}

	} // eka_add_project


	function eka_add_project_form() {
		global $post;
		 
		if ( eka_current_user_has_rights() ) :

			$category_id = '';
			foreach( (get_the_category() ) as $category ) { 
				$category_id .= $category->cat_ID . ','; 
			}
			
			if( empty( $category ) ) {
				$category_id = 0;
			    echo '<div class="error"><span class="dashicons dashicons-warning"></span> NB! Sellele lehele pole lisatud muutmisõiguseid, esineda võib probleeme sisu muutmisega!</div>';
			} else {
				$category_id = rtrim( $category_id, ',' );
			}

			?>
		   
			<div class="addpost column_left">
				<span class="addpostlink" id="addGalleryLink"><span class="dashicons dashicons-grid-view"></span> <?php echo __( 'Lisa uus galerii', 'artun2012' ); ?></span>
				<div class="addpostform" id="addGalleryForm">
					<form id="new_gallery" name="new_gallery" method="post" action="" enctype="multipart/form-data">
						<p>
							<input type="text" name="eka_title" id="addgallery_title" class="addpost_title required" placeholder="<?php echo __( 'Sisesta pealkiri*', 'artun2012' ); ?>">
						</p>
						<div class="dropArea fileuploading">
							<label for="addGalleryFiles" class="display addupload"><span class="dashicons dashicons-format-gallery"></span><br/><?php echo __( 'Piltide lisamiseks lohista nad kasti või kliki siia', 'artun2012' ); ?><span class="small"><br /><?php echo __( 'JPG, PNG või GIF failid', 'artun2012' ); ?></span></label><br />
							<input id="addGalleryFiles" type="file" name="upload_attachment[]" multiple style="visibility: hidden;">
							<div id="listAddGalleryFiles"></div>
						</div>
						<div class="addpost_content editable-newgallery notclicked" id="addgallery_content"></div>
						<p class="addpost_details">
							<input type="text" name="eka_post_tags" id="post_tags" class="addpost_field" placeholder="<?php echo __( 'Lisa silte (kasutatakse alammenüü loomiseks)', 'artun2012' ); ?>" />
						</p>
						<p>
							<input type="checkbox" name="eka_deptpage_show" id="eka_deptpage_show" value="1" />
							<label for="eka_deptpage_show"><?php echo __( 'Lisa galerii osakonna esilehe voogu', 'artun2012' ); ?></label>
						</p>
						<div style="padding-top:20px;" id="new_gallery_submit">
							<input type="submit" name="eka_submit" value="<?php echo __( 'Postita', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton" id="delGalleryLink"><?php echo __( 'tühista', 'artun2012' ); ?></span>
							<input type="hidden" name="eka_on_page" value="<?php echo $post->ID; ?>">
							<input type="hidden" name="eka_submit_type" value="add_new_project" />
							<input type="hidden" name="eka_dnd_files" value="" class="eka_dnd_files" />
							<textarea name="eka_content" id="addgallery_submit_content" style="display:none;"></textarea>
							<input type="hidden" name="eka_cat" id="cat" value="<?php echo $category_id; ?>"/>
							<?php wp_nonce_field( 'new-project', 'new-project-nonce' ); ?>
						</div>
						<div class="new_post_loading" id="new_gallery_loading">
							<?php echo __( 'Laen', 'artun2012' ); ?>...
						</div>
					</form>
				</div><!--/#addGalleryForm-->
			</div><!--/.addpost-->
		<?php endif;		

	} // eka_add_project_form

	
	// Creates the ADD POST form and takes care of submission.
	
	function eka_add_post() {

		if( isset( $_POST['new-post-nonce'] ) && wp_verify_nonce( $_POST['new-post-nonce'], 'new-post' ) ) {
		 
			$tags = $_POST['eka_post_tags'];
			$cats = array_map( 'intval', explode( ',', $_POST['eka_cat'] ) ); // cat IDs into array of integers (for wp_set_terms)
			$pages = array( (string)$_POST['eka_on_page'] );
			
			// Gets and checks wether user wants to show the post on front page and ticker
			if( isset( $_POST['eka_newspage_show'] ) && $_POST['eka_newspage_show'] == 1 ) {
				$newspage = 1;
				$newsticker = 1;
				$pages[] = "74"; // Adds "news" page (ID 74) to the list of pages this post has to appear on
				$comments = 'open';
			} else {
				$newspage = 0;
				$newsticker = 0;
				$comments = 'closed';
			}; 

			if( isset( $_POST['eka_frontpage_show'] ) && $_POST['eka_frontpage_show'] == 1 ) {
				$frontpage = 1;
			} else {
				$frontpage = 0;
			}; 
			if( isset( $_POST['eka_sticky_post'] ) && $_POST['eka_sticky_post'] == 1 ) {
				$stickypost = 1;
			} else {
				$stickypost = 0;
			};

			// Add the content of the form to $post as an array
			$new_post_id = wp_insert_post( array(
				'post_title'		=> wp_strip_all_tags( $_POST['eka_title'] ),
				'post_content'		=> $_POST['eka_content'],
				'tags_input'		=> $tags,
				'post_status'		=> 'publish',
				'post_type'			=> 'post',
				'comment_status'	=> $comments,
				'ping_status'		=> $comments
			) );

			// Polylang
			if ( function_exists( 'pll_set_post_language' ) ) {
				pll_set_post_language( $new_post_id, pll_current_language() );
			}
			
			wp_set_post_terms( $new_post_id, $cats, 'category' ); 
			wp_set_post_terms( $new_post_id, $pages, 'show_on_page' ); // makes this post appear on certain page

			add_post_meta( $new_post_id, 'show_news_ticker', $newsticker );
			add_post_meta( $new_post_id, 'show_news_page', $newspage ); 
			add_post_meta( $new_post_id, 'show_front_page', $frontpage ); 
			add_post_meta( $new_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ) );
			add_post_meta( $new_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) );
			add_post_meta( $new_post_id, 'show_sticky', $stickypost );


			// If we should also add the post in English... (Polylang)
			if( isset( $_POST['eka_en_title'] ) && $_POST['eka_en_title'] != '' ) {

				// Add the content of the form to $post as an array
				$new_en_post_id = wp_insert_post( array(
					'post_title'		=> wp_strip_all_tags( $_POST['eka_en_title'] ),
					'post_content'		=> $_POST['eka_en_content'],
					'tags_input'		=> $tags,
					'post_status'		=> 'publish',
					'post_type'			=> 'post',
					'comment_status'	=> $comments,
					'ping_status'		=> $comments
				) );

				// Polylang
				// Sets the new post language
				if ( function_exists( 'pll_set_post_language' ) ) {
					pll_set_post_language( $new_en_post_id, 'en' );
				} 
				// Sets the ENG post as a translationg of the EST one.
				if ( function_exists( 'pll_save_post_translations' ) ) {
					pll_save_post_translations( array( 'en' => $new_en_post_id, 'et' => $new_post_id ) );
				} 
				// Sets the post to appear on the English equivalents of marked pages
				$en_pages = array();
				if ( function_exists( 'pll_get_post' ) ) {
					foreach ( $pages as $pageid ) {
						$en_pages[] = (string)pll_get_post( $pageid, 'en' );
					}
				}
				
				wp_set_post_terms( $new_en_post_id, $cats, 'category' );
				wp_set_post_terms( $new_en_post_id, $en_pages, 'show_on_page' );

				add_post_meta( $new_en_post_id, 'show_news_ticker', $newsticker );
				add_post_meta( $new_en_post_id, 'show_news_page', $newspage ); 
				add_post_meta( $new_en_post_id, 'show_front_page', $frontpage ); 
				add_post_meta( $new_en_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST[ 'eka_frontpage_enddate' ] ));
				add_post_meta( $new_en_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST[ 'eka_frontpage_url' ] ) );
				add_post_meta( $new_en_post_id, 'show_sticky', $stickypost );
				 
			} // if en


			// Handle images
			if ( $_POST['eka_dnd_files'] ) {
				$attachments = json_decode( base64_decode($_POST['eka_dnd_files']), true );
								
				foreach ( $attachments as $attach_id ) {
					wp_update_post( array( 'ID' => $attach_id, 'post_parent' => $new_post_id ));

					if ( $new_en_post_id ) {
						
						$est_attach = get_post( $attach_id );
						$filename = get_attached_file( $attach_id );

						$eng_attach = array(
							'post_content'   => $est_attach->post_content,
							'post_title'     => $est_attach->post_title,
							'post_status'    => $est_attach->post_status,
							'post_mime_type' => $est_attach->post_mime_type,
						);
								 
						$attach_id = wp_insert_attachment( $eng_attach, $filename, $new_en_post_id );

						// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
						require_once( ABSPATH . 'wp-admin/includes/image.php' );

						// Generate the metadata for the attachment, and update the database record.
						$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
						wp_update_attachment_metadata( $attach_id, $attach_data );

					}
				}
								
			} else if ( $_FILES ) {
				$files = $_FILES['upload_attachment'];
				foreach ($files['name'] as $key => $value) {
					if ($files['name'][$key]) {
						$file = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						 
						$_FILES = array("upload_attachment" => $file);
						 
						foreach ($_FILES as $file => $array) {

							$newupload = insert_attachment( $file, $new_post_id );

							if ( $new_en_post_id ) {
								$est_attach = get_post( $newupload );
								$filename = get_attached_file( $newupload );

								$eng_attach = array(
									'post_content'   => $est_attach->post_content,
									'post_title'     => $est_attach->post_title,
									'post_status'    => $est_attach->post_status,
									'post_mime_type' => $est_attach->post_mime_type,
								);
								 
								$attach_id = wp_insert_attachment( $eng_attach, $filename, $new_en_post_id );

								// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
								require_once( ABSPATH . 'wp-admin/includes/image.php' );

								// Generate the metadata for the attachment, and update the database record.
								$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
								wp_update_attachment_metadata( $attach_id, $attach_data );

							}
						}
					}
				}
			}

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, uus postitus lisatud!', 'artun2012');
			
		}
		 
	} // eka_add_post


	function eka_add_post_form() {	
		global $post;

		if ( eka_current_user_has_rights() ) : 

			$category_id = '';
			foreach( (get_the_category() ) as $category ) { 
				$category_id .= $category->cat_ID . ','; 
			}

			if( empty( $category ) ) {
				$category_id = 0;
			    echo '<div class="error"><span class="dashicons dashicons-warning"></span> NB! Sellele lehele pole lisatud muutmisõiguseid, esineda võib probleeme sisu muutmisega!</div>';
			} else {
				$category_id = rtrim( $category_id, ',' );
			}
			
			?>

			<div class="addpost clearfix">
				<span class="addpostlink" id="addPostLink"><span class="dashicons 
dashicons-list-view"></span> <?php echo __( 'Lisa uus postitus', 'artun2012' ); ?></span>
				<div class="addpostform" id="addPostForm">
					<form id="new_post" name="new_post" method="post" action="" enctype="multipart/form-data">
						<p>
							<input type="text" name="eka_title" id="addpost_title" class="addpost_title required" placeholder="<?php echo __( 'Sisesta pealkiri*', 'artun2012' ); ?>">
						</p>
						<div class="dropArea fileuploading">
							<label for="addFiles" class="display addupload"><span class="dashicons dashicons-format-gallery"></span><br/><?php echo __( 'Piltide lisamiseks lohista nad kasti või kliki siia.', 'artun2012' ); ?><span class="small"><br /><?php echo __( 'JPG, PNG või GIF failid', 'artun2012' ); ?></span></label><br />
							<input id="addFiles" type="file" name="upload_attachment[]" multiple style="visibility: hidden;">
							<div id="listAddFiles"></div>
						</div>
						<div class="addpost_content editable-newpost notclicked" id="addpost_content"></div>
						<p class="addpost_details">
							<input type="text" name="eka_post_tags" id="post_tags" class="addpost_field" placeholder="<?php echo __( 'Lisa silte (kasutatakse alammenüü loomiseks)', 'artun2012' ); ?>" />
						</p>
						<?php if ( pll_current_language() != 'en' ) : ?>
						<p>
							<input type="checkbox" name="eka_english_show" id="eka_english_show" value="1" />
							<label for="eka_english_show"><?php echo __( 'Lisa inglise keelne kokkuvõte', 'artun2012' ); ?></label>
						</p>
						<div id="english_ad" style="padding-left: 1.1em; display: none;">
							<input type="text" name="eka_en_title" id="en_title" class="addpost_field" placeholder="<?php echo __( 'Inglise keelne pealkiri', 'artun2012' ); ?>" />
							<textarea class="addpost_field" name="eka_en_content" id="en_content" placeholder="<?php echo __( 'Inglise keelne kokkuvõte', 'artun2012' ); ?>"></textarea>
						</div><!--/#english_ad-->
						<?php endif; ?>
						<p>
							<input type="checkbox" name="eka_newspage_show" id="eka_newspage_show" value="1">
							<label for="eka_newspage_show"><?php echo __( 'Kuva postitust UUDISED lehel ja uudisteribal', 'artun2012' ); ?></label>
						</p>
						<p>
							<input type="checkbox" name="eka_frontpage_show" id="eka_frontpage_show" value="1" />
							<label for="eka_frontpage_show"><?php echo __( 'Lisa postitus bännerina EKA veebi esilehele', 'artun2012' ); ?></label>
						</p>
						<div id="addpost_ad" style="padding-left: 1.1em; display: none;">
							<input type="text" id="frontpage_datepicker" placeholder="<?php echo __( 'Kuupäev milleni bännerit näidata*', 'artun2012' ); ?>" class="addpost_field" />
							<input type="hidden" name="eka_frontpage_enddate" id="frontpage_datepicker_submit" />
							<input type="text" name="eka_frontpage_url" placeholder="<?php echo __( 'Bänneri link, kui saadetakse spetsiaalsele lehele', 'artun2012' ); ?>" class="addpost_field" />
							<?php if( current_user_can( 'manage_options' ) ) : ?>
								<p>
									<input type="checkbox" name="eka_sticky_post" id="eka_sticky_post" value="1" />
									<label for="eka_sticky_post" class="display"><?php echo __( 'Muuda postitus kleepsuks (näita teiste kohal)', 'artun2012' ); ?></label>
								</p>
							<?php endif; ?>
						</div><!--/#addpost_ad-->
						<div style="padding-top:20px;" id="new_post_submit">
							<input type="submit" name="eka_submit" value="<?php echo __( 'Postita', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton" id="delPostLink"><?php echo __( 'tühista', 'artun2012' ); ?></span>
							<input type="hidden" name="eka_on_page" value="<?php echo $post->ID; ?>">
							<input type="hidden" name="eka_submit_type" value="add_new_post" />
							<input type="hidden" name="eka_dnd_files" value="" class="eka_dnd_files" />
							<textarea name="eka_content" id="addpost_submit_content" style="display:none;"></textarea>
							<input type="hidden" name="eka_cat" id="cat" value="<?php echo $category_id; ?>"/>
							<?php wp_nonce_field( 'new-post', 'new-post-nonce' ); ?>
						</div>
						<div class="new_post_loading" id="new_post_loading">
							<?php echo __( 'Laen', 'artun2012' ); ?>...
						</div>
					</form>
				</div><!--/#addPostForm-->
			</div><!--/.addpost-->
		<?php endif;		
	} // eka_add_post_form
	

	// Creates the ADD EVENT form and takes care of submission.
	
	function eka_add_event() {
			
		if( isset( $_POST['new-event-nonce'] ) && wp_verify_nonce( $_POST['new-event-nonce'], 'new-event' ) ) {

			if( isset( $_POST['eka_newsticker_show'] ) && $_POST['eka_newsticker_show'] == 1 ) {
				$newsticker = 1;
			} else {
				$newsticker = 0;
			}; 
			if( isset( $_POST['eka_frontpage_show'] ) && $_POST['eka_frontpage_show'] == 1 ) {
				$frontpage = 1;
			} else {
				$frontpage = 0;
			}; 
			if( isset( $_POST['eka_sticky_post'] ) && $_POST['eka_sticky_post'] == 1 ) {
				$stickypost = 1;
			} else {
				$stickypost = 0;
			}; 

			//$_POST[ 'icl_post_language' ] = ICL_LANGUAGE_CODE; // WPML
		 
			// Add the content of the form to $post as an array
			$new_post_id = wp_insert_post( array(
				'post_title'	=> wp_strip_all_tags( $_POST['eka_title'] ),
				'post_content'	=> $_POST['eka_content'],
				'post_status'	=> 'publish',
				'post_type'		=> 'eka_calendar'
			) );

			if ( function_exists( 'pll_set_post_language' ) ) {
				pll_set_post_language( $new_post_id, pll_current_language() );
			} 

			$cats = array_map( 'intval', explode( ',', $_POST['eka_cat'] ) ); // cat IDs into array of integers (for wp_set_terms)
			$pages = array( (string)$_POST['eka_on_page'] );
						
			wp_set_post_terms( $new_post_id, $cats, 'category' ); // adds categories
			wp_set_post_terms( $new_post_id, $_POST['types'], 'calendar_category' ); // adds calendar categories
			wp_set_post_terms( $new_post_id, $_POST['eka_post_tags'], 'calendar_tag' ); // adds tags
			wp_set_post_terms( $new_post_id, $pages, 'show_on_page' ); // makes this post appear on certain page			
			
			// displaying news specific input
			add_post_meta( $new_post_id, 'show_news_ticker', $newsticker); 
			add_post_meta( $new_post_id, 'show_front_page', $frontpage); 
			add_post_meta( $new_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] )); 
			add_post_meta( $new_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] )); 
			add_post_meta( $new_post_id, 'show_sticky', $stickypost);
			
			// calendar specific input
			add_post_meta( $new_post_id, 'event_start_date', wp_strip_all_tags( $_POST['event_start_date'] )); 
			add_post_meta( $new_post_id, 'event_end_date', wp_strip_all_tags( $_POST['event_end_date'] )); 
			add_post_meta( $new_post_id, 'event_start_time', wp_strip_all_tags( $_POST['event_start_time'] )); 
			add_post_meta( $new_post_id, 'event_location', wp_strip_all_tags( $_POST['event_location'] )); 
			
			// If we should also add the event in English... (Polylang)
			if( isset( $_POST['eka_en_title'] ) && $_POST['eka_en_title'] != '' ) {

				// Add the content of the form to $post as an array
				$new_en_post_id = wp_insert_post( array(
					'post_title'		=> wp_strip_all_tags( $_POST['eka_en_title'] ),
					'post_content'		=> $_POST['eka_en_content'],
					'post_status'		=> 'publish',
					'post_type'			=> 'eka_calendar'
				) );


				// Polylang
				// Sets the new post language
				if ( function_exists( 'pll_set_post_language' ) ) {
					pll_set_post_language( $new_en_post_id, 'en' );
				} 
				// Sets the ENG post as a translationg of the EST one.
				if ( function_exists( 'pll_save_post_translations' ) ) {
					pll_save_post_translations( array( 'en' => $new_en_post_id, 'et' => $new_post_id ) );
				} 
				// Sets the post to appear on the English equivalents of marked pages
				$en_pages = array();
				if ( function_exists( 'pll_get_post' ) ) {
					foreach ( $pages as $pageid ) {
						$en_pages[] = (string)pll_get_post( $pageid, 'en' );
					}
				}
				
				wp_set_post_terms( $new_en_post_id, $cats, 'category' );
				wp_set_post_terms( $new_en_post_id, $en_pages, 'show_on_page' );
				wp_set_post_terms( $new_en_post_id, $_POST[ 'types' ], 'calendar_category' ); // adds calendar categories
				wp_set_post_terms( $new_en_post_id, $_POST[ 'eka_post_tags' ], 'calendar_tag' ); // adds tags

				add_post_meta( $new_en_post_id, 'show_news_ticker', $newsticker );
				add_post_meta( $new_en_post_id, 'show_front_page', $frontpage ); 
				add_post_meta( $new_en_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ));
				add_post_meta( $new_en_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) );
				add_post_meta( $new_en_post_id, 'show_sticky', $stickypost );

				// calendar specific input
				add_post_meta( $new_en_post_id, 'event_start_date', wp_strip_all_tags( $_POST['event_start_date'] )); 
				add_post_meta( $new_en_post_id, 'event_end_date', wp_strip_all_tags( $_POST['event_end_date'] )); 
				add_post_meta( $new_en_post_id, 'event_start_time', wp_strip_all_tags( $_POST['event_start_time'] )); 
				add_post_meta( $new_en_post_id, 'event_location', wp_strip_all_tags( $_POST['event_location'] )); 
				 
			} // if en


			// Handle images
			if ( $_POST['eka_dnd_files'] ) {
				$attachments = json_decode( base64_decode( $_POST['eka_dnd_files'] ), true );
								
				foreach ( $attachments as $attach_id ) {
					wp_update_post( array( 'ID' => $attach_id, 'post_parent' => $new_post_id ));
				}
								
			} else if ( $_FILES ) {

				$files = $_FILES['upload_attachment'];

				foreach ($files['name'] as $key => $value) {
					if ( $files['name'][$key] ) {
						$file = array(
							'name' => $files['name'][$key],
							'type' => $files['type'][$key],
							'tmp_name' => $files['tmp_name'][$key],
							'error' => $files['error'][$key],
							'size' => $files['size'][$key]
						);
						 
						$_FILES = array( "upload_attachment" => $file );
						 
						foreach ( $_FILES as $file => $array ) {
							$newupload = insert_attachment( $file, $new_post_id );

							if ( $new_en_post_id ) {
								$est_attach = get_post( $newupload );
								$filename = get_attached_file( $newupload );

								$eng_attach = array(
									'post_content'   => $est_attach->post_content,
									'post_title'     => $est_attach->post_title,
									'post_status'    => $est_attach->post_status,
									'post_mime_type' => $est_attach->post_mime_type,
								);
								 
								$attach_id = wp_insert_attachment( $eng_attach, $filename, $new_en_post_id );

								// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
								require_once( ABSPATH . 'wp-admin/includes/image.php' );

								// Generate the metadata for the attachment, and update the database record.
								$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
								wp_update_attachment_metadata( $attach_id, $attach_data );

							}

						}
					}
				}
			}

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, uus sündmus lisatud!', 'artun2012');
			
		}
		 
	} // eka_add_event


	function eka_add_event_form() {
		global $post;

		if ( current_user_can('edit_posts') ) : 

			$category_id = get_the_author_meta( 'user_categories', get_current_user_id() );
			if ( $category_id )
				$category_id = implode ( ", ", $category_id );
			?>

			<div class="addpost clearfix">
			<span class="addpostlink" id="addEventLink"><span class="dashicons dashicons-calendar-alt"></span> <?php echo __( 'Lisa uus sündmus', 'artun2012' ); ?></span>
			<div class="addpostform" id="addEventForm">
			<form id="new_event" name="new_event" method="post" action="" enctype="multipart/form-data">
				<p>
					<input type="text" name="eka_title" id="addpost_title" class="addpost_title required" placeholder="<?php echo __( 'Sisesta pealkiri*', 'artun2012' ); ?>">
				</p>
				<div class="dropArea fileuploading">
					<label for="addEventFiles" class="display addupload"><span class="dashicons dashicons-format-gallery"></span><br/><?php echo __( 'Piltide  lisamiseks lohista nad kasti või kliki siia', 'artun2012' ); ?><span class="small"><br /><?php echo __( 'JPG, PNG või GIF failid', 'artun2012' ); ?></span></label><br />
					<input id="addEventFiles" type="file" name="upload_attachment[]" multiple style="visibility: hidden;">
					<div id="listAddEventFiles"></div>
				</div>
				<div class="addpost_content notclicked editable-newevent" id="addevent_content"></div>
				<p style="margin-top: 1em;">
					<input type="text" id="startdate_datepicker" placeholder="<?php echo __( 'Lisa kuupäev (vajalik)', 'artun2012' ); ?>" class="addpost_field required" />
					<input type="text" id="enddate_datepicker" placeholder="<?php echo __( 'Lisa lõpukuupäev', 'artun2012' ); ?>" class="addpost_field" />
					<input type="text" name="event_start_time" placeholder="<?php echo __( 'Lisa kellaaeg', 'artun2012' ); ?>" class="addpost_field" /> 
				</p>
				<p>
					<input type="text" name="event_location" placeholder="<?php echo __( 'Lisa asukoht, aadressiga', 'artun2012' ); ?>" class="addpost_field" /> 		
				</p>
				<div class="addpost_details">
				<?php 
					// Show cat select if user is admin, use user's cat id otherwise
					if ( current_user_can('manage_options') ) {
						echo '<p>';
						wp_dropdown_categories( array(
							'show_option_none'	=> __( 'Vali osakond (halduri valik)', 'artun2012' ),
							'orderby'			=> 'name', 
							'hide_empty'		=> 0, 
							'exclude'			=> '1',
							'name'				=> 'eka_cat',
							'id'				=> 'cat'
						) );
						echo '</p>';
					} else {
						echo '<input type="hidden" name="eka_cat" id="cat" value="' . $category_id . '"/>';
					}
				?>
				</div>
				<div class="catselect">
					<ul>
						<?php 
						// get the terms and create selectboxes for multi cat select
						$cal_cats = get_terms( 'calendar_category', array(
							'orderby'    => 'name',
							'hide_empty' => 0
						) );
						foreach( $cal_cats as $cat ) : ?>
					
							<li>
								<input type="checkbox" name="types[]" value="<?php echo $cat->term_id; ?>" id="cal-cat-sel-<?php echo $cat->term_id; ?>"/>
								<label for="cal-cat-sel-<?php echo $cat->term_id; ?>" class="display"><?php echo $cat->name; ?></label>
							</li>
									
						<?php endforeach; ?>
					</ul>
				</div><!--/.catselect-->		 
				<p>
					<input type="text" name="eka_post_tags" id="event_tags" class="addpost_field" placeholder="<?php echo __( 'Lisa märksõnu (kasutatakse alamnavigatsioonis kalendris)', 'artun2012' ); ?>" />
				</p>
				<?php if ( pll_current_language() != 'en' ) : ?>
				<p>
					<input type="checkbox" name="eka_english_show" id="eka_english_event_show" value="1" />
					<label for="eka_english_event_show"><?php echo __( 'Lisa inglise keelne kokkuvõte' ); ?></label>
				</p>
				<div id="english_event_ad" style="padding-left: 1.1em; display: none;">
					<input type="text" name="eka_en_title" id="en_event_title" class="addpost_field" placeholder="<?php echo __( 'Inglise keelne pealkiri', 'artun2012' ); ?>" />
					<textarea class="addpost_field" name="eka_en_content" id="en_event_content" placeholder="<?php echo __( 'Inglise keelne kokkuvõte', 'artun2012' ); ?>"></textarea>
				</div><!--/#english_ad-->
				<?php endif; ?>
				<p>
					<input type="checkbox" name="eka_newsticker_show" id="eka_newsticker_event_show" value="1">
					<label for="eka_newsticker_event_show" class="display"><?php echo __( 'Lisa sündmus uudisteribale', 'artun2012' ); ?></label>
				</p>
				<p>
					<input type="checkbox" name="eka_frontpage_show" id="eka_frontpage_event_show" value="1" />
					<label for="eka_frontpage_event_show" class="display"><?php echo __( 'Lisa sündmus bännerina esilehele', 'artun2012' ); ?></label>
				</p>
				<div id="addevent_ad" style="padding-left: 1.1em; display: none;">
					<input type="text" id="frontpage_event_datepicker" placeholder="Kuupäev milleni bännerit näidata*" class="addpost_field" />
					<input type="hidden" name="eka_frontpage_enddate" id="frontpage_event_datepicker_submit" />
					<input type="text" name="eka_frontpage_url" placeholder="Bänneri link, kui saadetakse spetsiaalsele lehele" class="addpost_field" />
					<?php if ( current_user_can('manage_options') ) : ?>
						<p>
							<input type="checkbox" name="eka_sticky_post" id="eka_sticky_event" value="1" />
							<label for="eka_sticky_post" class="display"><?php echo _( 'Muuda sündmus kleepsuks (näita teiste kohal)', 'artun2012' ); ?></label>
						</p>
					<?php endif; ?>
				</div><!--/#addpost_ad-->
				<div style="padding-top:20px;" id="new_event_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Postita', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton" id="delEventLink"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="add_new_event" />
					<input type="hidden" name="eka_on_page" value="<?php echo $post->ID; ?>">
					<input type="hidden" name="eka_dnd_files" value="" class="eka_dnd_files" />
					<input type="hidden" name="event_start_date" id="startdate_datepicker_submit" />
					<input type="hidden" name="event_end_date" id="enddate_datepicker_submit" />
					<textarea name="eka_content" id="addevent_submit_content" style="display:none;"></textarea>
					<?php wp_nonce_field( 'new-event', 'new-event-nonce' ); ?>
				</div>
			</form>
			</div><!--/#addPostForm-->
			</div><!--/.addpost-->
		<?php endif;		
	} // eka_add_event_form


	// Displays the post quick edit form and updates posts
	function eka_update_post() {

		if( isset( $_POST['edit-post-nonce'] ) && wp_verify_nonce( $_POST['edit-post-nonce'], 'edit-post' ) ) {

			$tags = $_POST['eka_post_tags'];

			if ( $_POST['eka_on_page'] ) {
				$exist = (string) $_POST['eka_on_page'];
				$pages = explode ( ',' , $exist );
			} else {
				$pages = array();
			}
			
			// Gets and checks wether user wants to show the post on front page and ticker
			if( isset( $_POST['eka_newsticker_show'] ) && $_POST['eka_newsticker_show'] == 1 ) {
				$newspage = 1;
				$newsticker = 1;
				if ( !in_array( "74", $pages ) ) {
					$pages[] = "74"; // Adds "news" page (ID 74) to the list of pages this post has to appear on
				}
			} else {
				$newspage = 0;
				$newsticker = 0;
				if( ( $key = array_search( '74', $pages ) ) !== false ) {
					unset( $pages[ $key ] );
				}
			}; 

			if( isset( $_POST['eka_frontpage_show'] ) && $_POST['eka_frontpage_show'] == 1 ) {
				$frontpage = 1;
			} else {
				$frontpage = 0;
			}; 
			if( isset( $_POST['eka_sticky_post'] ) && $_POST['eka_sticky_post'] == 1 ) {
				$stickypost = 1;
			} else {
				$stickypost = 0;
			};

			$post_id = (int) $_POST['eka_check'];
			
			wp_set_post_tags( $post_id, $tags, false );
			wp_set_post_terms( $post_id, $pages, 'show_on_page', false );

			update_post_meta( $post_id, 'show_news_ticker', $newsticker );
			update_post_meta( $post_id, 'show_news_page', $newspage );
			update_post_meta( $post_id, 'show_front_page', $frontpage );
			update_post_meta( $post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ) );
			update_post_meta( $post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) );
			update_post_meta( $post_id, 'show_sticky', $stickypost );

			update_post_meta ( $post_id, 'post_last_edit', current_time( 'mysql' ) );
			update_post_meta ( $post_id, 'post_last_editor', get_current_user_id() );

			// If we should also update the post in English... (Polylang)
			//$en_post_id = icl_object_id( $post_id, 'post', false, 'en' );
			$en_post_id = pll_get_post( $post_id, 'en' );
			if( $en_post_id ) {
				
				$en_pages = array();
				foreach ( $pages as $pageid ) {
					$en_pages[] = icl_object_id( $pageid, 'page', true, 'en' );
				}
				
				wp_set_post_terms( $en_post_id, $cats, 'category', false );
				wp_set_post_terms( $en_post_id, $en_pages, 'show_on_page', false );

				update_post_meta( $en_post_id, 'show_news_ticker', $newsticker );
				update_post_meta( $en_post_id, 'show_news_page', $newspage );
				update_post_meta( $en_post_id, 'show_front_page', $frontpage );
				update_post_meta( $en_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ) );
				update_post_meta( $en_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) );
				update_post_meta( $en_post_id, 'show_sticky', $stickypost );

				update_post_meta ( $en_post_id, 'post_last_edit', current_time( 'mysql' ) );
				update_post_meta ( $en_post_id, 'post_last_editor', get_current_user_id() );
				 
			} // if en

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, postitus on uuendatud!', 'artun2012');
			
		}

	}

	function eka_update_post_form() {
		global $post;
		
		if ( eka_current_user_has_rights() ) : 

			// Get the current post data
			// Tags
			$posttags = get_the_tags( $post->ID );
			$curtags = '';
			if ( $posttags ) {
				foreach( $posttags as $tag ) {
					$curtags .= $tag->name . ', '; 
				}
				$curtags = rtrim( $curtags, ',' );
			}

			$onpages = implode ( ',' , wp_get_post_terms( $post->ID, 'show_on_page', array( "fields" => "names" ) ) );

			$custom_fields = get_post_custom( $post->ID );

			//print_a($custom_fields);

			// Show news page
			$ticker = '';
			if ( $custom_fields['show_news_ticker'][0] ) {
				$ticker = ' checked="checked"';
			}

			// Frontpage banner
			$banner = '';
			$display = ' display:none;';
			if ( $custom_fields['show_front_page'][0] ) {
				$banner = ' checked="checked"';
				$display = ' display:block;';
			}
			
			// Sticky banner
			$sticky = '';
			if ( $custom_fields['show_sticky'][0] ) {
				$sticky = ' checked="checked"';
			}
			
			?>

			<form name="edit_post" class="editpost js_edit_post clearfix" method="post" action="" enctype="multipart/form-data">
				<p>
					<label for="post_tags_<?php echo $post->ID; ?>"><?php echo __( 'Sildid', 'artun2012' ); ?></label>
					<input type="text" name="eka_post_tags" id="post_tags_<?php echo $post->ID; ?>" class="editpost_field" placeholder="<?php echo __( 'Lisa silte', 'artun2012' ); ?>" value="<?php echo $curtags; ?>" />
				</p>
				<p>
					<input type="checkbox" name="eka_newsticker_show" id="eka_newsticker_show_<?php echo $post->ID; ?>" value="1"<?php echo $ticker; ?> />
					<label for="eka_newsticker_show_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Kuva postitust UUDISED lehel', 'artun2012' ); ?></label>
				</p>
				<p>
					<input type="checkbox" name="eka_frontpage_show" id="eka_frontpage_show_<?php echo $post->ID; ?>" value="1" class="js_frontpage_show"<?php echo $banner; ?> />
					<label for="eka_frontpage_show_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Lisa postitus bännerina esilehele', 'artun2012' ); ?></label>
				</p>
				<div class="js_editpost_ad" style="padding-left: 1.1em;<?php echo $display; ?>">

					<?php
						if ( $custom_fields['show_front_page_enddate'][0] ) { 
							$fped = utf8_encode(strftime('%e. %B %Y', strtotime( $custom_fields['show_front_page_enddate'][0] ))); 
						} else { 
							$fped = '';
						}
					?>

					<input type="text" placeholder="<?php echo __( 'Kuupäev milleni bännerit näidata*', 'artun2012' ); ?>" id="frontpage_datepicker" class="editpost_field" value="<?php echo $fped; ?>" />
					<input type="hidden" name="eka_frontpage_enddate" id="frontpage_datepicker_submit" value="<?php echo $custom_fields['show_front_page_enddate'][0]; ?>" />

					<input type="text" name="eka_frontpage_url" placeholder="<?php echo __( 'Bänneri link', 'artun2012' ); ?>" class="editpost_field" value="<?php echo $custom_fields['show_front_page_url'][0]; ?>" />

					<?php if( current_user_can( 'manage_options' ) ) : ?>
						<p>
							<input type="checkbox" name="eka_sticky_post" id="eka_sticky_post_<?php echo $post->ID; ?>" value="1"<?php echo $sticky; ?> />
							<label for="eka_sticky_post_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Muuda bänner kleepsuks', 'artun2012' ); ?></label>
						</p>
					<?php endif; ?>

				</div><!--/.js_editpost_ad-->
				<div style="padding-top:20px; float:right;"><?php echo wp_delete_post_link(); ?></div>
				<div style="padding-top:20px;" class="js_post_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Uuenda', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton js_closeedit"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="update_post" />
					<input type="hidden" name="eka_check" value="<?php echo $post->ID; ?>" />
					<input type="hidden" name="eka_on_page" value="<?php echo $onpages; ?>" />
					<?php wp_nonce_field( 'edit-post', 'edit-post-nonce' ); ?>
				</div>
				<div class="new_post_loading js_post_loading">
					<?php echo __( 'Laen', 'artun2012' ); ?>...
				</div>

			</form>


		<?php endif;
	} // eka_edit_post_form


	// Displays the contact quick edit form and updates posts
	function eka_update_contact() {

		if( isset( $_POST['edit-contact-nonce'] ) && wp_verify_nonce( $_POST['edit-contact-nonce'], 'edit-contact' ) ) {

			$tags = $_POST['eka_post_tags'];

			if( isset( $_POST['contact_list'] ) && $_POST['contact_list'] == 1 ) {
				$contact_list = 1;
			} else {
				$contact_list = 0;
			}; 


			$post_id = (int) $_POST['eka_check'];
			
			wp_set_post_tags( $post_id, $tags, false );

			update_post_meta( $post_id, 'contact_list', $contact_list );

			update_post_meta( $post_id, 'post_last_edit', current_time( 'mysql' ) );
			update_post_meta( $post_id, 'post_last_editor', get_current_user_id() );

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, kontakt on uuendatud!', 'artun2012');
			
		}

	}
	function eka_update_contact_form() {
		global $post;
		
		if ( eka_current_user_has_rights() ) : 

			// Get the current post data
			// Tags
			$posttags = get_the_tags( $post->ID );
			$curtags = '';
			if ( $posttags ) {
				foreach( $posttags as $tag ) {
					$curtags .= $tag->name . ', '; 
				}
				$curtags = rtrim( $curtags, ',' );
			}

			$custom_fields = get_post_custom( $post->ID );

			// Show on contacts page
			$contact_list = '';
			if ( $custom_fields['contact_list'][0] ) {
				$contact_list = ' checked="checked"';
			}
			
			?>

			<form name="edit_contact" class="editpost js_edit_post clearfix" method="post" action="" enctype="multipart/form-data">
				<p>
					<label for="post_tags_<?php echo $post->ID; ?>"><?php echo __( 'Sildid', 'artun2012' ); ?></label>
					<input type="text" name="eka_post_tags" id="post_tags_<?php echo $post->ID; ?>" class="editpost_field" placeholder="<?php echo __( 'Lisa silte', 'artun2012' ); ?>" value="<?php echo $curtags; ?>" />
				</p>
				<p>
					<input type="checkbox" name="contact_list" id="contact_list_<?php echo $post->ID; ?>" value="1"<?php echo $contact_list; ?> />
					<label for="contact_list_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Näita EKA veebi üldisel kontaktilehel', 'artun2012' ); ?></label>
				</p>

				<div style="padding-top:20px; float:right;"><?php echo wp_delete_post_link(); ?></div>
				<div style="padding-top:20px;" class="js_post_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Uuenda', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton js_closeedit"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="update_contact" />
					<input type="hidden" name="eka_check" value="<?php echo $post->ID; ?>" />
					<?php wp_nonce_field( 'edit-contact', 'edit-contact-nonce' ); ?>
				</div>
				<div class="new_post_loading js_post_loading">
					<?php echo __( 'Laen', 'artun2012' ); ?>...
				</div>

			</form>


		<?php endif;
	} // eka_edit_post_form	

	// Displays the gallery quick edit form and updates posts
	function eka_update_project() {

		if( isset( $_POST['edit-project-nonce'] ) && wp_verify_nonce( $_POST['edit-project-nonce'], 'edit-project' ) ) {

			$tags = $_POST['eka_post_tags'];

			$post_id = (int) $_POST['eka_check'];
			
			wp_set_post_tags( $post_id, $tags, false );

			update_post_meta( $post_id, 'post_last_edit', current_time( 'mysql' ) );
			update_post_meta( $post_id, 'post_last_editor', get_current_user_id() );

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, galerii on uuendatud!', 'artun2012');
			
		}

	}
	function eka_update_project_form() {
		global $post;
		
		if ( eka_current_user_has_rights() ) : 

			// Get the current post data
			// Tags
			$posttags = get_the_tags( $post->ID );
			$curtags = '';
			if ( $posttags ) {
				foreach( $posttags as $tag ) {
					$curtags .= $tag->name . ', '; 
				}
				$curtags = rtrim( $curtags, ',' );
			}

			$custom_fields = get_post_custom( $post->ID );
			
			?>

			<form name="edit_project" class="editpost js_edit_post clearfix" method="post" action="" enctype="multipart/form-data">
				<p>
					<label for="post_tags_<?php echo $post->ID; ?>"><?php echo __( 'Sildid', 'artun2012' ); ?></label>
					<input type="text" name="eka_post_tags" id="post_tags_<?php echo $post->ID; ?>" class="editpost_field" placeholder="<?php echo __( 'Lisa silte', 'artun2012' ); ?>" value="<?php echo $curtags; ?>" />
				</p>

				<div style="padding-top:20px; float:right;"><?php echo wp_delete_post_link(); ?></div>
				<div style="padding-top:20px;" class="js_post_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Uuenda', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton js_closeedit"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="update_project" />
					<input type="hidden" name="eka_check" value="<?php echo $post->ID; ?>" />
					<?php wp_nonce_field( 'edit-project', 'edit-project-nonce' ); ?>
				</div>
				<div class="new_post_loading js_post_loading">
					<?php echo __( 'Laen', 'artun2012' ); ?>...
				</div>

			</form>

		<?php endif;
	} // eka_edit_post_form	


	// Displays the event quick edit form and updates event
	function eka_update_event() {

		if( isset( $_POST['edit-event-nonce'] ) && wp_verify_nonce( $_POST['edit-event-nonce'], 'edit-event' ) ) {

			if( isset( $_POST['eka_newsticker_show'] ) && $_POST['eka_newsticker_show'] == 1 ) {
				$newsticker = 1;
			} else {
				$newsticker = 0;
			}; 
			if( isset( $_POST['eka_frontpage_show'] ) && $_POST['eka_frontpage_show'] == 1 ) {
				$frontpage = 1;
			} else {
				$frontpage = 0;
			}; 
			if( isset( $_POST['eka_sticky_post'] ) && $_POST['eka_sticky_post'] == 1 ) {
				$stickypost = 1;
			} else {
				$stickypost = 0;
			}; 

			$post_id = (int) $_POST['eka_check'];

			$cats = array_map( 'intval', explode( ',', $_POST['eka_cat'] ) ); // cat IDs into array of integers (for wp_set_terms)
			$cal_cats = array_map( 'intval', $_POST['types'] );
						
			wp_set_object_terms( $post_id, $cats, 'category', false ); // adds categories
			wp_set_object_terms( $post_id, $cal_cats, 'calendar_category', false ); // adds calendar categories
			wp_set_object_terms( $post_id, $_POST['eka_post_tags'], 'calendar_tag', false ); // adds tags
			
			// displaying news specific input
			update_post_meta( $post_id, 'show_news_ticker', $newsticker ); 
			update_post_meta( $post_id, 'show_front_page', $frontpage ); 
			update_post_meta( $post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ) ); 
			update_post_meta( $post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) ); 
			update_post_meta( $post_id, 'show_sticky', $stickypost );
			
			// calendar specific input
			update_post_meta( $post_id, 'event_start_date', wp_strip_all_tags( $_POST['event_start_date'] ) ); 
			update_post_meta( $post_id, 'event_end_date', wp_strip_all_tags( $_POST['event_end_date'] ) ); 
			update_post_meta( $post_id, 'event_start_time', wp_strip_all_tags( $_POST['event_start_time'] ) ); 
			
			// If we should also update the event in English... (Polylang)
			//$en_post_id = icl_object_id( $post_id, 'eka_calendar', false, 'en' );
			$en_post_id = pll_get_post( $post_id, 'en' );

			if( $en_post_id ) {

				wp_set_post_terms( $en_post_id, $cats, 'category' ); // adds categories
				wp_set_post_terms( $en_post_id, $_POST['types'], 'calendar_category' ); // adds calendar categories
				wp_set_post_terms( $en_post_id, $_POST['eka_post_tags'], 'calendar_tag' ); // adds tags

				update_post_meta( $en_post_id, 'show_news_ticker', $newsticker );
				update_post_meta( $en_post_id, 'show_front_page', $frontpage ); 
				update_post_meta( $en_post_id, 'show_front_page_enddate', wp_strip_all_tags( $_POST['eka_frontpage_enddate'] ));
				update_post_meta( $en_post_id, 'show_front_page_url', wp_strip_all_tags( $_POST['eka_frontpage_url'] ) );
				update_post_meta( $en_post_id, 'show_sticky', $stickypost );

				// calendar specific input
				update_post_meta( $en_post_id, 'event_start_date', wp_strip_all_tags( $_POST['event_start_date'] )); 
				update_post_meta( $en_post_id, 'event_end_date', wp_strip_all_tags( $_POST['event_end_date'] )); 
				update_post_meta( $en_post_id, 'event_start_time', wp_strip_all_tags( $_POST['event_start_time'] )); 
				 
			} // if en

			$GLOBALS['eka_message'] = '<span class="dashicons dashicons-heart"></span>' . __('Suurepärane, sündmus on uuendatud!', 'artun2012');
			
		}

	}

	function eka_update_event_form() {
		global $post;
		
		if ( eka_current_user_has_rights() ) : 

			// Get the current post data
			// Tags
			$posttags = get_the_tags( $post->ID );
			$curtags = '';
			if ( $posttags ) {
				foreach( $posttags as $tag ) {
					$curtags .= $tag->name . ', '; 
				}
				$curtags = rtrim( $curtags, ',' );
			}

			$custom_fields = get_post_custom( $post->ID );

			//print_a($custom_fields);

			// Show news ticker
			$ticker = '';
			if ( $custom_fields['show_news_ticker'][0] ) {
				$ticker = ' checked="checked"';
			}

			// Frontpage banner
			$banner = '';
			$display = ' display:none;';
			if ( $custom_fields['show_front_page'][0] ) {
				$banner = ' checked="checked"';
				$display = ' display:block;';
			}
			
			// Sticky banner
			$sticky = '';
			if ( $custom_fields['show_sticky'][0] ) {
				$sticky = ' checked="checked"';
			}
			
			?>

			<form name="edit_event" class="editpost js_edit_post clearfix" method="post" action="" enctype="multipart/form-data">

				<p>
					<?php
						if ( $custom_fields['event_start_date'][0] ) { 
							$starttime = utf8_encode(strftime('%e. %B %Y', strtotime( $custom_fields['event_start_date'][0] ))); 
						} else { 
							$starttime = '';
						}
						
						if ( $custom_fields['event_end_date'][0] ) { 
							$endtime = utf8_encode(strftime('%e. %B %Y', strtotime( $custom_fields['event_end_date'][0]))); 
						} else { 
							$endtime = ''; 
						}
					?>

					<input type="text" id="startdate_datepicker" placeholder="<?php echo __( 'Lisa kuupäev (vajalik)', 'artun2012' ); ?>" class="addpost_field required" value="<?php echo $starttime; ?>" />
					<input type="text" name="event_start_time" placeholder="<?php echo __( 'Lisa kellaaeg', 'artun2012' ); ?>" class="addpost_field" value="<?php echo $custom_fields['event_start_time'][0]; ?>" /> 
					<input type="text" id="enddate_datepicker" placeholder="<?php echo __( 'Lisa lõpukuupäev', 'artun2012' ); ?>" class="addpost_field" value="<?php echo $endtime; ?>" />
					
					<input type="hidden" name="event_start_date" id="startdate_datepicker_submit" value="<?php echo $custom_fields['event_start_date'][0]; ?>" />
					<input type="hidden" name="event_end_date" id="enddate_datepicker_submit" value="<?php echo $custom_fields['event_end_date'][0]; ?>" />
				</p>

				<?php 
					// Show cat select if user is admin, use user's cat id otherwise

					$category = get_the_category(); 
					$selcat = ( $category ) ? $category[0]->term_id : '' ;

					if ( current_user_can('manage_options') ) {
						echo '<p>';
						wp_dropdown_categories( array(
							'show_option_none'	=> 'Vali osakond (halduri valik)',
							'orderby'			=> 'name', 
							'hide_empty'		=> 0,
							'selected'			=> $selcat,
							'exclude'			=> '1',
							'name'				=> 'eka_cat',
							'id'				=> 'cat'
						) );
						echo '</p>';
					} 
				?>

				<div class="catselect">
					<ul>
						<?php 
						// get the terms and create selectboxes for multi cat select
						$post_cats = array();
						$post_cats_req = get_the_terms( $post->ID, 'calendar_category' );
						if ( $post_cats_req ) {
							foreach ( $post_cats_req as $post_cat ) {
								$post_cats[] = $post_cat->slug;
							}
						}												

						$cal_cats = get_terms( 'calendar_category', array(
							'orderby'    => 'name',
							'hide_empty' => 0
						) );
						foreach( $cal_cats as $cat ) : 
							$checked = '';
							if ( in_array( $cat->slug, $post_cats ) ) {
								$checked = 'checked="checked" ';
							}

							?>
							<li>
								<input type="checkbox" name="types[]" value="<?php echo $cat->term_id; ?>" id="cal-cat-sel-<?php echo $cat->term_id; ?>" <?php echo $checked; ?>/>
								<label for="cal-cat-sel-<?php echo $cat->term_id; ?>" class="display"><?php echo $cat->name; ?></label>
							</li>
									
						<?php endforeach; ?>
					</ul>
				</div><!--/.catselect-->	


				<p>
					<label for="post_tags_<?php echo $post->ID; ?>"><?php echo __( 'Sildid', 'artun2012' ); ?></label>
					<input type="text" name="eka_post_tags" id="post_tags_<?php echo $post->ID; ?>" class="editpost_field" placeholder="<?php echo __( 'Lisa silte', 'artun2012' ); ?>" value="<?php echo $curtags; ?>" />
				</p>
				<p>
					<input type="checkbox" name="eka_newsticker_show" id="eka_newsticker_show_<?php echo $post->ID; ?>" value="1"<?php echo $ticker; ?> />
					<label for="eka_newsticker_show_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Kuva sündmust uudisteribal', 'artun2012' ); ?></label>
				</p>
				<p>
					<input type="checkbox" name="eka_frontpage_show" id="eka_frontpage_show_<?php echo $post->ID; ?>" value="1" class="js_frontpage_show"<?php echo $banner; ?> />
					<label for="eka_frontpage_show_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Lisa sündmus bännerina esilehele', 'artun2012' ); ?></label>
				</p>
				<div class="js_editpost_ad" style="padding-left: 1.1em;<?php echo $display; ?>">

					<?php
						if ( $custom_fields['show_front_page_enddate'][0] ) { 
							$fped = utf8_encode(strftime('%e. %B %Y', strtotime( $custom_fields['show_front_page_enddate'][0] ))); 
						} else { 
							$fped = '';
						}
					?>

					<input type="text" placeholder="<?php echo __( 'Kuupäev milleni bännerit näidata*', 'artun2012' ); ?>" id="frontpage_datepicker" class="editpost_field" value="<?php echo $fped; ?>" />
					<input type="hidden" name="eka_frontpage_enddate" id="frontpage_datepicker_submit" value="<?php echo $custom_fields['show_front_page_enddate'][0]; ?>" />

					<input type="text" name="eka_frontpage_url" placeholder="<?php echo __( 'Bänneri link', 'artun2012' ); ?>" class="editpost_field" value="<?php echo $custom_fields['show_front_page_url'][0]; ?>" />

					<?php if( current_user_can( 'manage_options' ) ) : ?>
						<p>
							<input type="checkbox" name="eka_sticky_post" id="eka_sticky_post_<?php echo $post->ID; ?>" value="1"<?php echo $sticky; ?> />
							<label for="eka_sticky_post_<?php echo $post->ID; ?>" class="display"><?php echo __( 'Muuda bänner kleepsuks', 'artun2012' ); ?></label>
						</p>
					<?php endif; ?>

				</div><!--/.js_editpost_ad-->
				<div style="padding-top:20px; float:right;"><?php echo wp_delete_post_link(); ?></div>

				<div style="padding-top:20px;" class="js_post_submit">
					<input type="submit" name="eka_submit" value="<?php echo __( 'Uuenda', 'artun2012' ); ?>"> <?php echo __( 'või', 'artun2012' ); ?> <span class="offbutton js_closeedit"><?php echo __( 'tühista', 'artun2012' ); ?></span>
					<input type="hidden" name="eka_submit_type" value="update_event" />
					<input type="hidden" name="eka_check" value="<?php echo $post->ID; ?>" />
					<?php wp_nonce_field( 'edit-event', 'edit-event-nonce' ); ?>
				</div>

				<div class="new_post_loading js_post_loading">
					<?php echo __( 'Laen', 'artun2012' ); ?>...
				</div>

			</form>


		<?php endif;
	} // eka_edit_event_form


	function editable_post_meta( $postid, $metaname, $form, $formating = true, $placeholder = '' ) {

		if ( $form == 'input' ) {
			$element = 'span';
		} else {
			$element = 'div';
		}

		if ( $formating == true ) {
			$formatoptions = '';
		} else {
			$formatoptions = 'data-disable-toolbar="true" data-disable-return="true"';
		}

		if ( $placeholder != '' ) {
			$placeholder = 'data-placeholder="' . $placeholder . '"';
		}

		echo '<' . $element . ' class="editable-' . $postid . ' editable-meta" data-meta-name="' . $metaname . '" ' . $formatoptions . ' ' . $placeholder . '>' . get_post_meta( $postid, $metaname, true ) . '</' . $element . '>';
	}


	/* ==============================================================================================================
	
	Displaying news and excerpts...
	
	============================================================================================================== */ 
	
	// News ticker in the header
	function eka_news_ticker() {	
		$showposts = get_posts( array(
			'post_type' 	=> array( 'post', 'eka_calendar' ),
			'numberposts' 	=> 5,
			'suppress_filters' => 0,
			'meta_query' 	=> array(
				array(
					'key' 	=> 'show_news_ticker',
					'value'	=> '1'
				)
			)
		) );
		
		echo '<div class="newsticker"><ul id="newsticker">';
			foreach( $showposts as $post ) {	 
				echo '<li><a href="' . $post->guid . '">' . $post->post_title . '</a></li>';
			}
			
			if ( ICL_LANGUAGE_CODE == 'en') {
				echo '</ul><div class="tickerlinks"><a href="' . site_url() . '/en/academy/news/">News</a> &nbsp; <a href="' . site_url() . '/en/studying/calendar/">Calendar</a></div>';
			} else {
				echo '</ul><div class="tickerlinks"><a href="' . site_url() . '/akadeemia/uudised/">Uudised</a> &nbsp; <a href="' . site_url() . '/oppimine/kalender/">Kalender</a></div>';
			}
			
		echo '</div>';
	}
	
	// Improve the Wordpress excerpt with our own function
	// Variable & intelligent excerpt length.
	function eka_the_excerpt($length) { // Max excerpt length. Length is set in characters
		global $post;
		$text = $post->post_excerpt;
		if ( '' == $text ) {
			$text = get_the_content('');
			$text = apply_filters('the_content', $text);
			$text = str_replace(']]>', ']]>', $text);
		}
		$text = strip_shortcodes($text); // optional, recommended
		$text = strip_tags($text); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags
	
		$text = substr($text,0,$length);
		$excerpt = reverse_strrchr($text, '.', 1);
		if( $excerpt ) {
			$excerpt = $excerpt.'<span>..</span>';
			echo apply_filters('the_excerpt',$excerpt);
		} else {
			$text = $text.'.<span>..</span>';
			echo apply_filters('the_excerpt',$text);
		}
	}

	function eka_get_the_excerpt($length) { // Max excerpt length. Length is set in characters
		global $post;
		$text = $post->post_excerpt;
		if ( '' == $text ) {
			$text = get_the_content('');
			$text = apply_filters('the_content', $text);
			$text = str_replace(']]>', ']]>', $text);
		}
		$text = strip_shortcodes($text); // optional, recommended
		$text = strip_tags($text); // use ' $text = strip_tags($text,'<p><a>'); ' if you want to keep some tags
	
		$text = substr($text,0,$length);
		$excerpt = reverse_strrchr($text, '.', 1);

		if( $excerpt ) {
			$excerpt = $excerpt.'<span>..</span>';
			return apply_filters('the_excerpt',$excerpt);
		} else {
			$text = $text.'.<span>..</span>';
			return apply_filters('the_excerpt',$text);
		}
	}
	
	// Returns the portion of haystack which goes until the last occurrence of needle
	function reverse_strrchr($haystack, $needle, $trail) {
		return strrpos($haystack, $needle) ? substr($haystack, 0, strrpos($haystack, $needle) + $trail) : false;
	}


	/* ==============================================================================================================
	
	Social Share Buttons
	
	============================================================================================================== */ 

	function eka_share( $url, $title ) {
		echo '<div class="post-share">
			<a class="socialicons dashicons dashicons-twitter" href="https://twitter.com/home?status=' . $url . '" target="_blank"></a>
			<a class="socialicons dashicons dashicons-facebook-alt" href="https://www.facebook.com/sharer/sharer.php?u=' . $url . '" target="_blank"></a>
			<a class="socialicons dashicons dashicons-googleplus" href="https://plus.google.com/share?url=' . $url . '" target="_blank"></a>
			<a class="socialicons dashicons dashicons-email-alt" href="mailto:?&subject=' . $title . '&body='. $url . '"></a>
		</div>';
	}

	
	/* ==============================================================================================================
	
	Posts-to-posts connections for displaying bits of information throughout the page (i.e contact info)
	
	============================================================================================================== */ 
	
	function eka_connection_types() {
		p2p_register_connection_type( array(
			'name' => 'pages_to_contacts',
			'from' => 'page',
			'to' => 'eka_contacts',
			'title' => array( 'from' => __( 'Contacts', 'artun2012' ), 'to' => __( 'Pages', 'artun2012' ) ),
			'from_labels' => array(
				'singular_name' => __( 'Page', 'artun2012' ),
				'search_items' => __( 'Search pages', 'artun2012' ),
				'not_found' => __( 'No pages found.', 'artun2012' ),
				'create' => __( 'Add Pages', 'artun2012' )
			),
			'to_labels' => array(
				'singular_name' => __( 'Contact', 'artun2012' ),
				'search_items' => __( 'Search contacts', 'artun2012' ),
				'not_found' => __( 'No contacts found.', 'artun2012' ),
				'create' => __( 'Add Contacts', 'artun2012' )
			),
			'sortable' => 'to'
		) );
	}
	add_action( 'p2p_init', 'eka_connection_types' );
	
	/* ==============================================================================================================
	
	Assign categories to pages.
	
	============================================================================================================== */ 
	
	function attach_category_to_page() {
		register_taxonomy_for_object_type('category','page');
	}
	
	add_action('admin_init','attach_category_to_page');
	
	/* ==============================================================================================================
	
	Show and increase posts view counts
	
	============================================================================================================== */ 
	
	// function to display number of posts.
	function getPostViews($postID){
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
			return '<span class="dashicons dashicons-visibility"></span> 0';
		}
		return '<span class="dashicons dashicons-visibility"></span> ' . $count;
	}
	
	// function to count views.
	function setPostViews($postID) {
		$count_key = 'post_views_count';
		$count = get_post_meta($postID, $count_key, true);
		if($count==''){
			$count = 0;
			delete_post_meta($postID, $count_key);
			add_post_meta($postID, $count_key, '0');
		}else{
			$count++;
			update_post_meta($postID, $count_key, $count);
		}
	}

	/* ==============================================================================================================
	
	Front-end Admin bar for pages
	
	============================================================================================================== */ 

	function eka_page_admin() {
		global $post, $user;

		if ( current_user_can( 'edit_page', $post->ID ) ): ?>
			<div class="adminbar">
				<div class="admin-left">
					<?php echo getPostViews( $post->ID ) ?> <?php echo __( 'vaatamist', 'artun2012' ); ?>
				</div>
				<div class="admin-right">
					<?php 
					$updater = get_post_meta($post->ID, 'post_last_editor', true);
					if ( $updater ): $ui = get_userdata( $updater ); ?>

						<?php echo __( 'Viimati muutis:', 'artun2012' ); ?> <?php echo $ui->user_firstname . ' ' . $ui->user_lastname . ', ' . strftime( '%e.%m.%Y', strtotime( $post->post_modified ) ); ?>
					
					<?php endif; ?>

					<?php eka_post_categories(); ?>

					<span class="js_contentsave editpostlink inline primary" data-save-id="<?php echo $post->ID; ?>" style="display:none;">
						<span class="dashicons dashicons-yes"></span> <?php echo __( 'Salvesta', 'artun2012' ); ?>
					</span>
					
					<span class="js_contentundo editpostlink inline" data-save-id="<?php echo $post->ID; ?>" style="display:none;">
						<span class="dashicons dashicons-no-alt"></span> <?php echo __( 'Tühista', 'artun2012' ); ?>
					</span>
					
					<span class="js_contentedit editpostlink inline" data-edit-id="<?php echo $post->ID; ?>">
						<span class="dashicons dashicons-edit"></span> <?php echo __( 'Muuda', 'artun2012' ); ?>
					</span>
					
					<?php if ( current_user_can( 'manage_options' )) : ?>
						<a class="editpostlink inline" href="<?php echo get_edit_post_link( $post->ID ); ?>">
							<span class="dashicons dashicons-admin-settings"></span> <?php echo __( 'Muuda halduris', 'artun2012' ); ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		<?php endif;
	}

	/* ==============================================================================================================
	
	Display a list of categories a post belongs to
	
	============================================================================================================== */ 

	function eka_post_categories() {
		global $post;

		$categories = get_the_category( $post->ID );
		$categories_list = '';
		if ( $categories ) {
			foreach( $categories as $category ) {
			    $categories_list .= $category->name . ', ';
			}
			$categories_list = rtrim( $categories_list, ', ' );
		} else {
			$categories_list = __( 'Pole määratud. Sisu muutmisel võib esineda tõrkeid!', 'artun2012' );
		}

		?>

		<span class="tooltip" data-tip="<?php echo __( 'Muutmisõigused:', 'artun2012' ); ?> <?php echo $categories_list; ?>">
			<span class="dashicons dashicons-businessman"></span>
		</span>

		<?php
	}

	
	/* ==============================================================================================================
	
	Delete posts from the front-end and delete image link
	
	============================================================================================================== */ 
	
	// Delete posts TODO: do this with ajax as well?
	function wp_delete_post_link() {
		global $post;
		
		if ( !eka_current_user_has_rights() )
			return;

		return '<a href="' . get_delete_post_link( $post->ID ) . '" onclick="javascript:if(!confirm(\'Oled kindel et soovid ' . $post->post_name  . ' EKA veebilt eemaldada?\')) return false;" title="Kustuta see sisu" class="editpostlink inline"><span class="dashicons dashicons-trash"></span></a>';
	}	
	
	// Delete image link
	
	function wp_delete_attachment_link() {
		global $post;
		echo '<a data-post_id="' . $post->ID . '" class="js-deleteLink"><span class="dashicons dashicons-trash"></span></a>';
	}

	// Edit image link
	
	function wp_edit_attachment_link() {
		global $post;
		
		echo '<span class="js_contentedit imageedit" data-edit-id="' . $post->ID . '"><span class="dashicons dashicons-edit"></span></span>';

		echo '<span class="js_contentsave inline primary imageedit" data-save-id="' . $post->ID . '" style="display:none;"><span class="dashicons dashicons-yes"></span> ' . __( 'Salvesta', 'artun2012' ) . '</span>';

		echo '<span class="js_contentundo editpostlink inline" data-save-id="' . $post->ID . '" style="display:none;"><span class="dashicons dashicons-no-alt"></span> ' . __( 'Tühista', 'artun2012' ) . '</span>';
		
		if ( current_user_can( 'manage_options' )) {
			echo ' <a href="' . get_edit_post_link( $post->ID ) . '"><span class="dashicons dashicons-admin-settings"></span></a>';
		}
	}

	// Add meta boxes to admin area. Uses the Meta Box plugin.
	add_filter( 'rwmb_meta_boxes', 'eka_add_meta_boxes' );
	function eka_add_meta_boxes( $meta_boxes ) {
	    $meta_boxes[] = array(
				'title'      => __( 'Postituse avaldamine',
					'artun2012' ),
				'post_types' => array( 'post', 'eka_calendar' ),
				'context'	 => 'side',
				'fields'     => array(
					array(
						'id'   => 'show_news_ticker',
						'name' => __( 'Lisa postitus uudisteribale', 'artun2012' ),
						'type' => 'checkbox'
					),
					array(
						'id'   => 'show_front_page',
						'name' => __( 'Lisa postitus bännerina esilehele', 'artun2012' ),
						'type' => 'checkbox'
					),
					array(
						'id'   => 'show_front_page_enddate',
						'name' => __( 'Bänneri kestvus', 'artun2012' ),
						'type' => 'date',
						'js_options' => array(
							'dateFormat'      => 'yymmdd'
						),
					),
					array(
						'id'   => 'show_front_page_url',
						'name' => __( 'Bänneri link', 'artun2012' ),
						'type' => 'url'
					)
				)
			);

	    $meta_boxes[] = array(
				'title'      => __( 'Sündmuse detailid',
					'artun2012' ),
				'post_types' => 'eka_calendar',
				'fields'     => array(
					array(
						'id'   => 'event_start_date',
						'name' => __( 'Alguse kuupäev', 'artun2012' ),
						'type' => 'date',
						'js_options' => array(
							'dateFormat'      => 'yymmdd'
						),
					),
					array(
						'id'   => 'event_start_time',
						'name' => __( 'Kellaaeg', 'artun2012' ),
						'type' => 'text'
					),
					array(
						'id'   => 'event_end_date',
						'name' => __( 'Lõpu kuupäev', 'artun2012' ),
						'type' => 'date',
						'js_options' => array(
							'dateFormat'      => 'yymmdd'
						),
					),
					array(
						'id'   => 'event_location',
						'name' => __( 'Asukoht', 'artun2012' ),
						'type' => 'text'
					)
				)
			);

	    $meta_boxes[] = array(
				'title'      => __( 'Kitsas tulp',
					'artun2012' ),
				'post_types' => array( 'page', 'eka_xpages' ),
				'fields'     => array(
					array(
						'id'   => 'column_right',
						'name' => __( 'Kitsa tulba sisu', 'artun2012' ),
						'type' => 'wysiwyg'
					)
				)
			);

	    $meta_boxes[] = array(
				'title'      => __( 'Kontakti Detailid',
					'artun2012' ),
				'post_types' => 'eka_contacts',
				'fields'     => array(
					array(
						'id'   => 'contact_email',
						'name' => __( 'E-posti aadress', 'artun2012' ),
						'type' => 'email'
					),
					array(
						'id'   => 'contact_phone',
						'name' => __( 'Telefon', 'artun2012' ),
						'type' => 'text'
					),
					array(
						'id'   => 'contact_mobile',
						'name' => __( 'Mobiil', 'artun2012' ),
						'type' => 'text'
					),
					array(
						'id'   => 'contact_role',
						'name' => __( 'Amet', 'artun2012' ),
						'type' => 'text'
					),
					array(
						'id'   => 'contact_role_en',
						'name' => __( 'Amet (inglise keeles)', 'artun2012' ),
						'type' => 'text'
					),
					array(
						'id'   => 'contact_www',
						'name' => __( 'Veebileht', 'artun2012' ),
						'type' => 'url'
					),
					array(
						'id'   => 'contact_cv',
						'name' => __( 'CV', 'artun2012' ),
						'type' => 'url'
					),
					array(
						'id'   => 'divider',
						'name' => '',
						'type' => 'divider'
					),
					array(
						'id'   => 'contact_list',
						'name' => __( 'Näita kontaktilehel', 'artun2012' ),
						'type' => 'checkbox'
					)
				)
			);

		$meta_boxes[] = array(
				'title'      => 'Lehe seaded',
				'post_types' => array( 'page', 'eka_xpages' ),
				'fields'     => array(
					array(
						'id'   => 'eka_disallow_tagging',
						'name' => __( 'Eemalda märksõnad', 'artun2012' ),
						'type' => 'checkbox',
						'desc' => 'Ära kasuta selle lehe postitustel täägide lisamise funktsiooni'
					),
					array(
						'id'   => 'eka_disallow_gallery',
						'name' => __( 'Eemalda galerii', 'artun2012' ),
						'type' => 'checkbox',
						'desc' => 'Ära kuva lehele lisatud pilte galeriina'
					)
				)
			);


	    return $meta_boxes;
	}

	/* ==============================================================================================================
	
	Returns first image associated with a post ($post_id)
	
	============================================================================================================== */ 
	
	function eka_attachment( $post_id ) {
		$result = get_children( array(
			'numberposts' => 1,
			'orderby' => 'menu_order title',
			'order'=> 'ASC',
			'post_mime_type' => 'image',
			'post_parent' => $post_id,
			'post_type' => 'attachment'
		) );
		return current( $result );
	}

	
	/* ==============================================================================================================
	
	Adding AJAX functionality.
	
	============================================================================================================== */ 
	
	// For loading large images
	add_action('wp_ajax_load_large_image', 'eka_large_image');
	add_action('wp_ajax_nopriv_load_large_image', 'eka_large_image');

	// For deleting attachments
	add_action('wp_ajax_delete_attachment', 'eka_delete_attachment');

	// For uploading files
	add_action('wp_ajax_return_attachment', 'eka_return_attachment');
	
	// For sorting items
	add_action('wp_ajax_item_sort', 'eka_save_item_order');

	// For loading more posts to the infinite scroll
	add_action("wp_ajax_nopriv_load_more","eka_load_posts");
	add_action("wp_ajax_load_more","eka_load_posts");

	// For loading contacts on the contacts list page
	add_action("wp_ajax_nopriv_load_contacts","eka_load_contacts");
	add_action("wp_ajax_load_contacts","eka_load_contacts");

	// For loading news, calendar images and projects on front page
	add_action("wp_ajax_nopriv_load_frontpage","eka_load_frontpage");
	add_action("wp_ajax_load_frontpage","eka_load_frontpage");

	// For loading images and galleries next to posts
	add_action("wp_ajax_nopriv_load_postgallery","eka_load_postgallery");
	add_action("wp_ajax_load_postgallery","eka_load_postgallery");

	// For loading curated content
	add_action("wp_ajax_load_curated","eka_load_curated");

	// For saving content edits
	add_action("wp_ajax_save_changes","eka_save_changes");

	// Gets large images to show in the overlay, or calls for the preview script if it's not an image we're seeing
	function eka_large_image(){
		check_ajax_referer('ajax_nonce');

		$post = get_post( $_REQUEST[ "post_id" ] ); 
		$post_mime = $post->post_mime_type;
		
		// Gets the id of a preview image (generated by our custom function for PDF and other mysc files)
		$post_preview_id = get_post_meta( $_REQUEST["post_id"], 'preview_image_id', true );

		// If we have a preview id, a.k.a it's not an image
		if ( $post_preview_id ) {
			
			$url = wp_get_attachment_url( $post->ID );
			
			$output = '<div class="preview-file">' . do_shortcode('[gview file="' . $url . '"]') . '</div>';
			
			$output .= '<p><a href="' . $url . '">Lae alla '.$post->post_title.'</a> (' . pathinfo($url, PATHINFO_EXTENSION) . ')</p>';
		
		// Otherwise, if it's a regular image
		} else if ( $post_mime == 'image/jpeg' || $post_mime == 'image/gif' || $post_mime == 'image/png' ) {

			$output = '<img src="' . wp_get_attachment_image_url( $_REQUEST[ "post_id" ], 'large' ) . '" alt="' . $post->post_title . '">';
			$output .= '<p>' . $post->post_title . '</p>';

		// Otherwise, if it's not a regular image
		} else {
				
			$url = wp_get_attachment_url( $post->ID );

			$output = '<p><a href="' . $url . '">Lae alla '.$post->post_title.'</a> (' . pathinfo($url, PATHINFO_EXTENSION) . ')</p>';		
			
		}

		echo $output;	   
		die();
	}	
	
	// deletes attachment images
	function eka_delete_attachment() {
		check_ajax_referer('ajax_nonce');
				
		wp_delete_attachment( $_REQUEST['att_id'], true );
		  
		die();
	}	
	
	// sorts draggable content
	function eka_save_item_order() {
		global $wpdb;
	
		$order = explode(',', $_POST['order']);
		$counter = 0;
		foreach ($order as $item_id) {
			$wpdb->update($wpdb->posts, array( 'menu_order' => $counter ), array( 'ID' => $item_id) );
			$counter++;
		}
		
		die(1);
	}
	
	// inserts files uploaded with DnD and sends back their IDs
	function eka_return_attachment(){
		check_ajax_referer('ajax_nonce');
		
		if ( $_FILES ) {
			$files = $_FILES['upload_attachment'];
			$attachments = array();
							
			foreach ($files['name'] as $key => $value) {
				if ($files['name'][$key]) {
					$file = array(
						'name' => $files['name'][$key],
						'type' => $files['type'][$key],
						'tmp_name' => $files['tmp_name'][$key],
						'error' => $files['error'][$key],
						'size' => $files['size'][$key]
					);
						 
					$_FILES = array("upload_attachment" => $file);
						 
					foreach ($_FILES as $file => $array) {
						$attachments[] = insert_attachment($file);
					}
				}
			}
			
			echo base64_encode(json_encode($attachments));
			
		} 
		  
		die();
	}
	
	// Retrieves the category form
	function eka_return_catselect() {
		$cats = array();
		$cats = explode(" ", $_POST['cats']);
		echo eka_cat_select( 1, $cats );
		die();
	}

	// Saves changes made to post
	function eka_save_changes() {
		//if( isset( $_POST['edit-post-nonce'] ) && wp_verify_nonce( $_POST['edit-post-nonce'], 'edit-post' ) ) {

		$metas = $_REQUEST[ "metas" ];
		$postid = (int) $_REQUEST["post"];

		foreach ( $metas as $key => $value ) {
			update_post_meta( $postid, $key, $value );
		}

		//add_filter('wp_insert_post_data', 'update_post_slug');
		echo wp_update_post( array(
			'ID'			=> $postid,
			'post_title'	=> $_REQUEST[ "title" ],
			'post_content'	=> $_REQUEST[ "content" ]
		) );
		//remove_filter('wp_insert_post_data', array(&$this, 'update_post_slug')); 

		//}

		die();
	}
	
	/* ==============================================================================================================
	
	Generate images thumbnails to non-image files
	
	============================================================================================================== */ 
	
	function make_attachment_preview( $attachment_ID ) {
	
		// Check if this file type can be used for a preview
		if ( !in_array( get_post_mime_type( $attachment_ID ), array(
			'pdf' 	=> 'application/pdf',
			'txt' 	=> 'text/plain',
			'doc' 	=> 'application/msword',
			'docx' 	=> 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
			'xls'	=> 'application/vnd.ms-excel',
			'xlsx' 	=> 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
			'ppt' 	=> 'application/vnd.ms-powerpoint',
			'pptx' 	=> 'application/vnd.openxmlformats-officedocument.presentationml.presentation',
			'ai' 	=> 'application/postscript',
			'psd' 	=> 'image/vnd.adobe.photoshop'
		) ) ) {
			return;
		}
		
		// Check if this attachment already has images
		if ( get_post_meta( $attachment_ID, 'preview_image_id', true ) )
			return;
	
		// Require admin files in case the function is called from front-end
		require_once(ABSPATH . 'wp-admin/includes/media.php');
		require_once(ABSPATH . 'wp-admin/includes/file.php');
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		
		// Get the file from Google Drive
		$url = 'https://docs.google.com/viewer?url=' . wp_get_attachment_url( $attachment_ID ) . '&a=bi&pagenumber=1&w=800'; 
		$tmp = download_url( $url );
		
		// Check for download errors
		if ( is_wp_error( $tmp ) ) {
			@unlink( $file_array[ 'tmp_name' ] );
			return;
		} 
		
		// Generate an array for media_handle_sideload that would resemble a regular @_FILES array
		$file_array = array(
			'name' => preg_replace('/\.(pdf|txt|doc|docx|xls|xlsx|ppt|pptx|ai|psd).*/i', '.png', basename( $url )),
			'tmp_name' => $tmp
		);
	
		// Chop-chop! Do the Wordpress magic to generate different image files and a Media Library post
		$preview_ID = media_handle_sideload( $file_array, 0 );
		
		// Check for handle sideload errors.
		if ( is_wp_error( $preview_ID ) ) {
			@unlink( $file_array['tmp_name'] );		
			return;
		} 
		
		// Tell Wordpress our beloved attachment's preview post ID
		update_post_meta( $attachment_ID, 'preview_image_id', $preview_ID );
		
		return;
	}
	
	add_action("add_attachment", "make_attachment_preview");
	add_action("edit_attachment", "make_attachment_preview");
	
	// Add support to new uploadable file types
	add_filter('upload_mimes', 'eka_upload_mimes');
	
	function eka_upload_mimes( $existing_mimes=array() ) {
		$existing_mimes['psd'] = 'image/vnd.adobe.photoshop';
		$existing_mimes['ai'] = 'application/postscript';
		return $existing_mimes;
	}
	
	// Checks wether attachment has a preview image before returning the correct image
	/*function eka_get_attachment_image( $attachment_id, $size, $icon, $attr ) {
		$attachment_preview_id = get_post_meta( $attachment_id, 'preview_image_id', true );
		if ( $attachment_preview_id ) {
			return wp_get_attachment_image( $attachment_preview_id, $size, $icon, $attr );
		} else {
			return wp_get_attachment_image( $attachment_id, $size, $icon, $attr );
		}
	}*/

	// Performs a couple of checks and returns the correct image url
	function eka_get_attachment_image( $attachment, $imgargs = array() ) {

		// Cheks if this attachment has a separate preview file (i.e a PDF, AI, PSD etc.)
		$attachment_preview_id = get_post_meta( $attachment->ID, 'preview_image_id', true );
		if ( $attachment_preview_id ) {

			$imagesrc = wp_get_attachment_image_src( $attachment_preview_id, 'medium', 0 );
			return '<img src="' . $imagesrc[0] . '" alt="' . $imgargs['alt'] . '" title="' . $imgargs['title'] . '">';

		} else {

			// Checks if it's gif. Gets the full size image if is, to retain animation.
			if ( $attachment->post_mime_type == 'image/gif') {
				$imagesrc = wp_get_attachment_image_src( $attachment->ID, 'full', 0 );
				return '<img src="' . $imagesrc[0] . '" alt="' . $imgargs['alt'] . '" title="' . $imgargs['title'] . '">';
			} else {
				$imagesrc = wp_get_attachment_image_src( $attachment->ID, 'medium', 0 );
				return '<img src="' . $imagesrc[0] . '" alt="' . $imgargs['alt'] . '" title="' . $imgargs['title'] . '">';
			}

		}
	}
	
	
	function add_iframe($arr) {
		$arr['extended_valid_elements'] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]";
		return $arr;
	}
	add_filter('tiny_mce_before_init', 'add_iframe');
	 
	global $allowedposttags;
	$allowedposttags["iframe"] = array(
			"id" => array(),
			"class" => array(),
			"title" => array(),
			"style" => array(),
			"align" => array(),
			"frameborder" => array(),
			"longdesc" => array(),
			"marginheight" => array(),
			"marginwidth" => array(),
			"name" => array(),
			"scrolling" => array(),
			"src" => array(),
			"height" => array(),
			"width" => array()
	);


	/* ==============================================================================================================
	
	Add categories to attachments (media), add parent or user category on save
	
	============================================================================================================== */ 

	// Enable categories on media
	function eka_register_taxonomy_for_media() {
		register_taxonomy_for_object_type( 'category', 'attachment' );
	}
	add_action( 'init', 'eka_register_taxonomy_for_media' );

	// Plug into the attachment save hook
	add_action( 'add_attachment', 'eka_set_attachment_categories' );

	// Do the category adding magic
	function eka_set_attachment_categories( $attachment_ID ) {

		// Check if the post has a parent, get its id and categories if yes
		$parents = get_post_ancestors( $attachment_ID );
		if ( $parents ) {
			
			$postcats = get_the_category( $parents[0] );

			if ( $postcats ) {
				foreach( $postcats as $cat ) { 
					$category_ids[] = $cat->cat_ID; 
				}
			}

		// If theres no parent category for some reason, try to get the user's instead
		} else {
			$category_ids = get_the_author_meta( 'user_categories', get_current_user_id() );
		}

		// If we finally have an id, set it to our beloved media!
		if ( $category_ids )
			wp_set_post_terms( $attachment_ID, $category_ids, 'category' ); 
		
		return;
	}



	/* ==============================================================================================================
	
	Custom functions upon saving and editing posts
	
	============================================================================================================== */ 
	
	function eka_save_post( $post_id )  {
	   
		// Check it's not an auto save routine
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return;

		// Perform permission checks! For example:
		if ( !current_user_can('edit_post', $post_id) ) 
			return;
			
		// Get saved post content
		$the_post = get_post( $post_id );
		$content = $the_post->post_content;

		//$content = apply_filters( 'the_content', $content );

		/*global $wp_embed;
		add_filter( 'my_media_filter', array( $wp_embed, 'autoembed' ), 8 );

		$content = apply_filters( "my_media_filter", $content );*/
	   

		// CONTENT CONVERSIONS

		// Youtube video links to embedded player
		$content = preg_replace(
			'#<a(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*<\/a>#x', 
			'<iframe width="100%" height="400px" src="http://www.youtube.com/embed/$2" frameborder="0" allowfullscreen></iframe>', 
			$content 
		);
		
		// Vimeo links to embedded player
		$content = preg_replace(
			'#<a(.*?)(?:href="https?://)?(?:www\.)?(?:vimeo\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*<\/a>#x', 
			'<iframe title="Vimeo video player" width="100%" height="400px" src="http://player.vimeo.com/video/$1" frameborder="0" allowfullscreen>Vimeo Video</iframe>', 
			$content 
		);

		/*
		// Converts Google Form links to forms new version
		$content = preg_replace(
			'#(?:https://)?(?:docs\.)?(?:google\.com/forms/d/)([\w-]+)(?:\S+)?#', 
			'<iframe title="Google Form" width="100%" height="600px" src="https://docs.google.com/forms/d/$1/viewform?embedded=true&hgd=1" frameborder="0" marginheight="0" marginwidth="0">Google Form</iframe>', 
			$content 
		);
		
		// Converts Google Form links to forms old version
		$content = preg_replace(
			'#(?:https://)?(?:docs\.)?(?:google\.com/spreadsheet/viewform\?formkey=)([\w-]+)(?:\S+)?#', 
			'<iframe title="Google Form" width="100%" height="600px" src="https://docs.google.com/spreadsheet/embeddedform?formkey=$1" frameborder="0" marginheight="0" marginwidth="0">Google Form</iframe>', 
			$content 
		);*/


		// UPDATE LAST EDIT META
		/*if ( ! update_post_meta ( $post_id, 'post_last_edit', current_time( 'mysql' ) ) ) 
			add_post_meta( $post_id, 'post_last_edit', current_time( 'mysql' ) );

		if ( ! update_post_meta ( $post_id, 'post_last_editor', get_current_user_id() ) ) 
			add_post_meta( $post_id, 'post_last_editor', get_current_user_id() );*/

		update_post_meta ( $post_id, 'post_last_edit', current_time( 'mysql' ) );
		update_post_meta ( $post_id, 'post_last_editor', get_current_user_id() );

		// SAVE IT
		
		// Unhook this function so it doesn't loop infinitely
		remove_action('save_post', 'eka_save_post');
		
		// Update the post, which calls save_post again
		wp_update_post( array( 'ID' => $post_id, 'post_content' => $content ) );
		
		// Re-hook this function
		add_action('save_post', 'eka_save_post');

	}
	add_action('save_post', 'eka_save_post');




	/* ==============================================================================================================
		
	Increases the number of search results
	
	============================================================================================================== */ 

	add_filter( 'post_limits', 'eka_postsperpage' );
	function eka_postsperpage( $limits ) {
		if ( is_search() ) {
			global $wp_query;
			$wp_query -> query_vars[ 'posts_per_page' ] = 100;
		}
		return $limits;
	}
