<?php
	ini_set('display_errors',1); 
?>

<?php
/**
 * Template Name: Testing Playground
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<h2><?php the_title(); ?></h2>
<?php eka_page_menu(); ?>


<textarea style="width: 500px; height: 900px;">

<?php 
	$pages = wp_list_pages ( array(
		'depth'        => 1,
		'child_of'     => 0,
		'exclude'      => '17',
		'title_li'     => '',
		'echo'         => 0,
		'sort_column'  => 'menu_order',
		'sort_order' => 'ASC',
		'post_type'    => 'page',
    	'post_status'  => 'publish' 
	)); 

	print_a($pages);
?>

</textarea>
<?php endwhile; ?>


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>

