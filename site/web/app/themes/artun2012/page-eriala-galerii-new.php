<?php
/**
 * Template Name: Eriala, galeriid NEW DESIGN
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title_new(); ?></h2>
		<?php eka_page_menu_new( "dept" ); ?>
	</div><!--/.pageheader-->

	<?php eka_page_admin(); ?>

	<div class="gallery-home clearfix" id="content">
		<div class="postscontent clearfix">
			<h3 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h3>

			<div class="column_left">
				<div class="contentbox editable-<?php echo $post->ID; ?>">
					<?php the_content(); ?>
				</div>
				<?php eka_the_posts(); ?>
			</div>

			<?php
				// Calls the add project form
				eka_add_project_form();
			?>

			<?php
			// Check if tagging is enabled on the page
			if ( get_post_meta( $post->ID, 'eka_disallow_tagging', true ) != 1 ):

				// Gets the tag info, if one is set
				if( $wp_query->query_vars['tag'] != '' ) {
					$selected_tag = $wp_query->query_vars['tag'];
						
					// Gets this tag item
					$tagObj = get_term_by( 'slug', $selected_tag, 'post_tag' );
						
					// Modify the arguments
					$args['tag_id'] = $tagObj->term_id;

					// Update the $selected_tag variable, for security reasons
					$selected_tag = $tagObj->slug;
				} else {
					$selected_tag = '';
					$args['tag_id'] = '';
				}

				?>

				<ul class="tagslist" id="tagslist">
					<?php

					// Gets all the tags related to posts shown on this page

					// First checks if there are any posts at all
					$this_page = term_exists( (string)$post->ID, 'show_on_page' );
					if ( $this_page ): 

						$tags = get_category_tags( array( 
							'categories' 	=> $this_page['term_id'], 
							'taxonomy' 		=> 'show_on_page',
							'term' 			=> 'post_tag',
							'post_type' 	=> 'eka_project' 
						) );

						if ( !empty( $tags ) ):	?>

							<li class="tagoption <?php current_tag( 'all', $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag=">Kõik</a></li>
						
						<?php endif;
										
						foreach ( $tags as $tag ) : ?>
							<li class="tagoption <?php current_tag( $tag->slug, $selected_tag ) ?>"><a href="<?php echo get_permalink() ?>?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name; ?></a></li>
						<?php endforeach; ?>

						<?php
							if ( count( $tags ) >= 10 ) :
						?>
							<li class="moretags" id="moretags"><?php echo __('rohkem silte...', 'artun2012'); ?></li>
						<?php endif; ?>
					<?php endif; ?>
				</ul><!--/.taglist-->

			<?php endif; ?>
			

		<?php if ( eka_current_user_has_rights() ) : ?>
			<div class="sortable gallery-dept clearfix" id="gallery">
		<?php else : ?>
			<div class="visitor gallery-dept clearfix" id="gallery">
		<?php endif; ?>

				<?php eka_load_gallery( $args ); ?>
			</div>

			<div class="column_left">
				<?php editable_post_meta( $post->ID, 'column_right', 'rich' ); ?>
			</div>

		</div><!--/.postscontent-->
	</div><!--/#content-->
	
<?php setPostViews( get_the_ID() ); ?>
		
<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>