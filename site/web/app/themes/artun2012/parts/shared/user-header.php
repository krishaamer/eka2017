<?php
	if ( isset($GLOBALS['eka_message']) ) {
		echo $GLOBALS['eka_message'];
	};
?></div>

<?php 
	global $current_user;
	wp_get_current_user();

	if ( is_user_logged_in() ) : 
?>
	<div class="useractions clearfix" id="useractions">
		
		<?php if ( current_user_can( 'manage_options' ) ) : ?>
			<a href="<?php echo site_url(); ?>/wp-admin/"><?php echo __( 'Haldur', 'artun2012' ); ?></a>
		<?php endif; ?>

		<div class="usercontent">
			<ul>
				<li><a href="#" id="togglecurated"><?php echo __( 'Minu kureeritud sisu', 'artun2012' ); ?></a></li>
			</ul>
		</div><!--/.usercontent-->
			
		<div class="userinfo">
			<ul>
				<li><a href="<?php echo wp_logout_url( get_permalink() ); ?>" title="Logi välja"><?php echo __('Logi välja', 'artun2012' ); ?></a></li>
				<li><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></li>
			</ul>
		</div><!--/.userinfo-->
			
		<div class="userlinks">
			<?php // We are hardcoding those to save a good bunch of sql queries ?>
			<ul class="menu">
			<?php if ( ICL_LANGUAGE_CODE == 'et') : ?>
				<li class="menu-item"><a href="http://docs.google.com/">Dokumendid</a></li>
				<li class="menu-item"><a href="https://www.google.com/calendar">Google kalender</a></li>
				<li class="menu-item"><a href="https://mail.google.com">Postkast</a></li>
				<li class="menu-item"><a href="https://artun.ois.ee/">ÕIS</a></li>
				<li class="menu-item"><a href="https://dok.artun.ee/">Webdesktop</a></li>
				<li class="menu-item"><a href="http://moodle.artun.ee/">Moodle</a></li>
			<?php else: ?>
				<li class="menu-item"><a href="http://docs.google.com/">Documents</a></li>
				<li class="menu-item"><a href="https://www.google.com/calendar">Google Calendar</a></li>
				<li class="menu-item"><a href="https://mail.google.com">Mailbox</a></li>
				<li class="menu-item"><a href="https://dok.artun.ee/">Webdesktop</a></li>
				<li class="menu-item"><a href="https://artun.ois.ee/en/">SIS</a></li>
				<li class="menu-item"><a href="http://moodle.artun.ee/">Moodle</a></li>
			<?php endif; ?>	
			</ul>
		</div><!--/.userlinks-->

		<div class="usercanvas">

			<?php if( get_transient( 'cat-' . $current_user->user_login ) ) : // checks wether user has a category set ?>

				<form action=""  method="POST" class="firstlogin" id="firstlogin">

					<h2>
						<?php echo __( 'Teretulemast Kunstiakadeemia kodulehele', 'artun2012' ); ?>
					</h2>
					<p>
						<?php echo __( 'Registreerimise lõpetamiseks vali palun osakonnad millega oled seotud.', 'artun2012' ); ?>
					</p>

					<?php echo eka_cat_select( 0, 0 ); ?>

					<?php wp_nonce_field( 'user_nonce', 'user_nonce_field' ); ?>
					<input type="hidden" name="eka_submit_type" value="update_user_category">
					<input type="submit" value="Valmis">

				</form>
				
			<?php endif; // first login cat select prompt ?>

			<div id="usercuratedcontent" class="usercuratedcontent">
				<ul class="menu curatedtabs clearfix" id="curatedtabs">
					<li data-type="page" class="current_page_item"><?php echo __( 'Lehed', 'artun2012' ); ?></li>
					<li data-type="post"><?php echo __( 'Postitused', 'artun2012' ); ?></li>
					<li data-type="eka_contacts"><?php echo __( 'Kontaktid', 'artun2012' ); ?></li>
					<li data-type="eka_project"><?php echo __( 'Tööd', 'artun2012' ); ?></li>
				</ul>
				<ul class="curatedhead clearfix">
					<li style="width:35%;"><?php echo __( 'Pealkiri', 'artun2012' ); ?></li>
					<li style="width:10%;"><?php echo __( 'Vaatamisi', 'artun2012' ); ?></li>
					<li style="width:20%;"><?php echo __( 'Viimati muudetud', 'artun2012' ); ?></li>
					<li style="width:20%;"><?php echo __( 'Viimane muutja', 'artun2012' ); ?></li>
				</ul>
				<ul id="curatedcontainer" class="curatedlist"></ul>

				<span class="green" id="closecurated"><?php echo __( 'Sulge', 'artun2012' ); ?></span>
			</div>

		</div><!--/.usercanvas-->
		
	</div><!--/.useractions-->
<?php endif; ?>