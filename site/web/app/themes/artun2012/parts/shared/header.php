<?php get_template_part( 'parts/shared/user', 'header' ); ?>

<?php if ( 'eka_xpages' == get_post_type() ): ?>
	<header class="noheader">
		<h1 class="heading-x"><a href="<?php echo site_url(); ?>"><?php echo __('Eesti Kunstiakadeemia', 'artun2012'); ?></a></h1>
		<ul class="headerlinks">
			<li>
				<ul class="utility">
					<?php pll_the_languages(  ); ?>
				</ul><!--/.utility-->
			</li>
		</ul><!--/.headerlinks-->	 
	</header>
	<div id="container" class="container">
<?php else: ?>
	<?php eka_news_ticker(); ?>
	<?php if ( is_home() ): ?>
		<header class="homeheader">
			<h1 class="heading-home-<?php echo ICL_LANGUAGE_CODE; ?>"><?php bloginfo( 'name' ); ?></h1>
			<ul class="headerlinks-home">
				<li>
					<?php 
						if ( ICL_LANGUAGE_CODE == 'et') {
							dynamic_sidebar( 'Esileht EST' );
						} else {
							dynamic_sidebar( 'Esileht ENG' );
						}
					?>
				</li>
				<li>
					<ul class="utility">
						<?php if ( !is_user_logged_in() ): ?>
							<li><?php echo eka_login_url(); ?></li>
						<?php endif; ?>
						<?php

						    $languages = icl_get_languages( 'skip_missing=0' );
						    if( !empty( $languages ) ){
						        foreach( $languages as $l ){

						        	if ( $l['language_code'] == 'en' ) {
						        		if( !$l['active'] ) echo '<li><a href="'.$l['url'].'">ENG</a></li>';
						        	} else {
						        		if( !$l['active'] ) echo '<li><a href="'.$l['url'].'">EST</a></li>';
						        	}

						        }
						    }

						?>
					</ul><!--/.utility-->
				</li>
			</ul><!--/.headerlinks-home--> 
		</header>
		<div id="container" class="container-home">
	<?php else: ?>
		<header>
			<nav id="navigation">
				<h1><a href="<?php echo icl_get_home_url(); ?>" title="<?php echo __('Esilehele', 'artun2012'); ?>"><?php bloginfo( 'name' ); ?></a></h1>
				<div class="closenav" id="closenav"><a href="#"></a></div>
				<?php eka_main_menu(); ?>
			</nav>
			<div class="mobileheader"><div class="logo"><a href="<?php echo icl_get_home_url(); ?>" title="<?php echo __('Esilehele', 'artun2012'); ?>"></a></div><div class="opennav" id="opennav"><a href="#navigation"></a></div></div>
			<ul class="headerlinks">
				<li>
					<ul class="utility">
						<?php if ( !is_user_logged_in() ): ?>
							<li><?php echo eka_login_url(); ?></li>
						<?php endif; ?>
						
						<?php

						    $languages = icl_get_languages( 'skip_missing=0' );
						    if( !empty( $languages ) ){
						        foreach( $languages as $l ){

						        	if ( $l['language_code'] == 'en' ) {
						        		if( !$l['active'] ) echo '<li><a href="'.$l['url'].'">ENG</a></li>';
						        	} else {
						        		if( !$l['active'] ) echo '<li><a href="'.$l['url'].'">EST</a></li>';
						        	}

						        }
						    }

						?>
						
						<li class="searchbegin" id="searchbegin"></li>
						<li class="searchbox" id="quicksearch">  
							<?php get_search_form(); ?>
						</li><!--/.searchbox--> 
					</ul><!--/.utility-->
				</li>
			</ul><!--/.headerlinks-->	  
		</header>
		<div id="container" class="container">
	<?php endif; // is_home ?>
<?php endif; // is post_type eka_xpages ?>