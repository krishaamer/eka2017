<?php
	$current_type = $post->post_type;
	$current_template_slug = get_page_template_slug( $post->ID ); 
	$images = array();

?>

<?php if ( is_home() ): ?>
	<div class="gallery-home clearfix" id="gallery">
<?php elseif ( $current_template_slug == 'page-eriala-tood.php' ): ?>
	<div class="gallery-projects clearfix" id="gallery">
<?php elseif ( $current_template_slug == 'page-eriala-esileht-new.php' || $current_template_slug == 'page-eriala-galerii-new.php' || $current_template_slug == 'page-eriala-inimesed-new.php' ): ?>
	<div class="gallery-home clearfix">	
<?php elseif ( $current_type == 'eka_project' ): ?>
	<div class="gallery-projects clearfix sortable" id="gallery">
	<?php eka_add_images_form(); ?>	
<?php else: ?>
	<div class="gallery-scroll clearfix sortable" id="gallery" data-type="gallery">
	<?php eka_add_images_form(); ?>
<?php endif; ?>

<?php

	/* =============================================================================================================
	
	Handle all the images loaded in the grid by and below posts / pages
	
	============================================================================================================= */ 
	
	// returns images associated with the page
	function eka_get_images() {
		global $post;
		
		return get_posts( array(
			'post_type' => 'attachment',
			'numberposts' => -1,
			'post_parent' => $post->ID,
			'orderby' => 'menu_order',
			'order' => 'ASC'
		) );	
	}
	
	// returns posts which have been set to appear as banner on front page and are sticky	
	function eka_get_stickybanners() {
		list( $today_year, $today_month, $today_day ) = preg_split( '([^0-9])', current_time('mysql') );
		
		return get_posts( array(
			'post_type' => 'any',
			'numberposts' => -1,
			'lang' => pll_current_language(), 
			'meta_query' => array(
				array(
					'key' => 'show_front_page',
					'value' => '1'
				),
				array(
					'key' => 'show_front_page_enddate',
					'value' => $today_year . $today_month . $today_day,
					'compare' => '>='
				), 
				array(
					'key' => 'show_sticky',
					'value' => '1'
				)
			)
		) );
	}
	
	// returns projects
	function eka_get_portfolio() {
		global $post;
		
		$department = get_the_category( $post->ID ); 
		$projects = array();
				
		if( $department[0] ) {

			$et_cat_id = icl_object_id( $department[0] -> cat_ID, 'category', true, 'et' );

			$projects = get_posts( array(
				'post_type' => 'eka_project',
				'numberposts' => -1,
				'category'  => $et_cat_id
			) );
		}
		
		return $projects;
	}
	
			
	// COMBINE THE IMAGES ARRAY DEPENDING ON PAGE 		
	// images for the front page	
	if ( is_home() ) {
		
		$images = eka_get_stickybanners();
		
	// images department pages, on projects page
	} else if ( $current_template_slug == 'page-eriala-tood.php' ) {

		$images = eka_get_portfolio();

	// images department pages	
	} else if ( ( $current_type == 'page' || $current_type == 'eka_project' || $current_type == 'eka_contacts' || $current_type == 'eka_xpages' ) && get_post_meta ( $post->ID, 'eka_disallow_gallery', true ) != 1 ) {
				
		$images = eka_get_images();
		
	} 
	
	// PROCESS THE IMAGES/POSTS/CALENDAR/PROJECTS ARRAY		 
	if ( !empty( $images ) ) {
		$tmp_post = $post;
		
		the_post();
		
		foreach ( $images as $post ) { 
		
			setup_postdata( $post );
			
			$imgargs = array(
				'alt'   => trim(strip_tags( $post->post_title )),
				'title' => trim(strip_tags( $post->post_title )),
			);
			
			// for posts and calendar, on front page
			if ( $post->post_type == 'post' || $post->post_type == 'eka_calendar' ) {
				
				$url = get_post_meta($post->ID, 'show_front_page_url', true);
				
				if ($url) { 
					// checks if the link has a http:// at start, adds it if not
					if( strpos($url, "http") === false ) {
						$url = 'http://'.$url;
					}
					$target = "_blank";
				} else {
					$url = $post->guid;
					$target = "_self";
				}

				$content = eka_get_the_excerpt(140);
				
				// gets the first image associated with this post				
				$showimage = eka_attachment( $post->ID );
				if ( $showimage ) {
					?>
					<div class="imagewrapper">
						<div>
							<div class="imagebox"> 
								<?php echo eka_get_attachment_image( $showimage, $imgargs ); ?>
								<div class="imagedetails click_link" data-post_guid="<?php echo $url; ?>" target="<?php echo $target; ?>">
									<h5><?php echo trim(strip_tags( $post->post_title )); ?></h5>
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">... <?php echo __( 'loe edasi', 'artun2012' ); ?></div>
								</div><!--/.imagedetails-->
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper--> 
					<?php	
				} else { ?>
					<div class="imagewrapper">
						<div>
							<div class="imagebox"> 
								<div class="textimage">
									<h5>
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>"><?php echo trim(strip_tags( $post->post_title )); ?></a>
									</h5>
									<div class="imagecontent"><?php echo $content; ?></div>
									<div class="readmore">
										<a href="<?php echo $url; ?>" target="<?php echo $target; ?>">... <?php echo __( 'loe edasi', 'artun2012' ); ?>&nbsp;</a>
									</div>
								</div><!--/.textimage-->
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->
				
				<?php
				}
			
			// for projects	list (!)
			} else if ( $post->post_type == 'eka_project' ) {
								
				// gets the first attachment associated with this post				
				$showimage = eka_attachment( $post->ID );

				$projectauthor = '<p>' . get_post_meta($post->ID, 'project_author', true). '</p>';
				
				$department = get_the_category( $post->ID ); 
				
				if( $department[0] ) {
					$projectdept = '<p>' . $department[0]->cat_name . '</p>';
					$projectfaculty = '<p>' . get_cat_name($department[0]->category_parent) . '</p>';
				}
							
				// if we have an image associated with that post	
				if ( $showimage ) { ?>
					<div class="imagewrapper">
						<div>
							<div class="imagebox">
								<?php echo eka_get_attachment_image( $showimage, $imgargs ); ?>            
								<div class="imagedetails click_link" data-post_guid="<?php echo $post->guid; ?>">
									<h5><?php echo trim(strip_tags( $post->post_title )); ?></h5>
									<?php echo $projectauthor; ?>
									<?php echo $projectdept; ?>
									<?php echo $projectfaculty; ?>

									<?php if ( eka_current_user_has_rights() ) : ?>
										<p class="adminimage">
											<?php echo wp_delete_post_link(); ?>
										</p>
									<?php endif; ?>
								</div><!--/.imagedetails-->
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->  
				<?php } else { ?>
					<div class="imagewrapper">
						<div>
							<div class="imagebox"> 
								<div class="textimage">
									<h5><a href="<?php echo $post->guid; ?>"><?php echo trim(strip_tags( $post->post_title )); ?></a></h5>
								</div><!--/.textimage-->

								<?php if ( eka_current_user_has_rights() ) : ?>
									<p class="adminimage">
										<?php echo wp_delete_post_link(); ?>
									</p>
								<?php endif; ?>  
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->
				
				<?php }

			// for just images, or attachment of other types, on individual pages and posts
			} else if ( $post->post_type == 'attachment' ) {

				$img = eka_get_attachment_image( $post, $imgargs );

				if ( $img ): ?>

					<div class="<?php echo $post->ID; ?> imagewrapper" id="<?php echo $post->ID; ?>">
						<div>
							<div class="imagebox">	
								<?php echo $img; ?>  

								<div class="imagedetails click_attachment" data-post_id="<?php echo $post->ID ?>">
									<p class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true"><?php the_title(); ?></p>

									<?php if ( eka_current_user_has_rights() ) : ?>
										<p class="adminimage">
											<?php eka_post_categories(); ?>
											<span class="sort handle imageedit"><span class="dashicons dashicons-randomize"></span></span>
											<?php wp_edit_attachment_link(); ?>
											<?php wp_delete_attachment_link(); ?>
										</p>
									<?php endif; ?>
								</div><!--/.imagedetails-->
                   
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->

				<?php else: ?>

					<div class="<?php echo $post->ID; ?> imagewrapper" id="<?php echo $post->ID; ?>">
						<div>
							<div class="imagebox">
								<div class="textimage click_attachment" data-post_id="<?php echo $post->ID ?>">            
									<p><a class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true"><?php the_title(); ?></a></p>
												 
									<?php if ( eka_current_user_has_rights() ) : ?>
										<p class="adminimage">
											<span class="imageedit sort handle"><span class="dashicons dashicons-sort"></span></span>
											<?php wp_edit_attachment_link(); ?>
											<?php wp_delete_attachment_link(); ?>
										</p>
									<?php endif; ?>

								</div>
							</div><!--/.imagebox-->
						</div>
					</div><!--/.imagewrapper-->  					

			<?php
				endif; // if has images
			}
		}	// loop $images
		
		$post = $tmp_post;
	}	// if $images has content	
		
?>
</div><!--/.gallery-->
</div><!--/.content-->
</div><!--/#container--> 
<div class="overlay-modal">
	<div class="open-modal" id="openModal">
		<?php if ( pll_current_language() == 'et' ): ?>
			Täida küsitlus, võida meeneid!
		<?php else: ?>
			Participate in survey, win prizes!				
		<?php endif; ?>
	</div>
	<div class="modal-container" id="modalContainer">
		<div class="close-modal" id="closeModal">×</div>
		<div class="modal-content">
			<?php if ( pll_current_language() == 'et' ): ?>
				<iframe src="https://www.surveymonkey.com/r/HSVX2PJ" seamless width="100%" height="100%"></iframe>
			<?php else: ?>
				<iframe src="https://www.surveymonkey.com/r/FXQ6NYJ" seamless width="100%" height="100%"></iframe>				
			<?php endif; ?>
		</div>
	</div>
</div>
<div class="loading-animation" id="loading-animation"></div>
<?php if ( 'eka_xpages' != get_post_type() && !is_home() ): ?>
	<footer>
		<?php 
			if ( pll_current_language() == 'et' ) {
				dynamic_sidebar( 'Jalus EST' );
			} else {
				dynamic_sidebar( 'Jalus ENG' );
			}
		?>
	</footer>
<?php endif; ?>
<!-- Hotjar Tracking Code for http://artun.ee -->
<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:155112,hjsv:5};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
</script>