<!DOCTYPE HTML>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie ie6" lang="et"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie ie7" lang="et"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie ie8" lang="et"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="et"><!--<![endif]-->
	<head>
		<title><?php wp_title( '—', 1, 'right' ); ?><?php echo __( 'Eesti Kunstiakadeemia', 'artun2012' ); ?></title>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
	  	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php echo __( 'Eesti Kunstiakadeemia, kui ainus kunstiülikool Eestis, tunnetab enda vastutust õppe, täiendõppe ja teadustöö korralduse ning kvaliteedi eest ühiskonna kultuuriruumi ja elukeskkonda kujundavates kunstivaldkondades.', 'artun2012' ); ?>">
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/favicon.ico"/>

		<script>(function() {
		  var _fbq = window._fbq || (window._fbq = []);
		  if (!_fbq.loaded) {
		    var fbds = document.createElement('script');
		    fbds.async = true;
		    fbds.src = '//connect.facebook.net/en_US/fbds.js';
		    var s = document.getElementsByTagName('script')[0];
		    s.parentNode.insertBefore(fbds, s);
		    _fbq.loaded = true;
		  }
		  _fbq.push(['addPixelId', '1493396687550610']);
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', 'PixelInitialized', {}]);
		</script>
		<noscript><img height="1" width="1" border="0" alt="" style="display:none" src="https://www.facebook.com/tr?id=1493396687550610&amp;ev=NoScript" /></noscript>

        <?php wp_enqueue_script("jquery"); ?>
        
        <!--[if lt IE 9]>
        	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
        <![endif]-->
        <!--[if IE]>
        	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
		<?php wp_head(); ?>
		
	</head>
	<body <?php body_class(); ?> ontouchstart="" onmouseover="">
	<!-- Google Tag Manager -->
		<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-PCPHMW"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-PCPHMW');</script>
	<!-- End Google Tag Manager -->