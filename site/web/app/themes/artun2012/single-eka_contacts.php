<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="pageheader">
	<h2 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta nimi"><?php the_title(); ?></h2>
</div><!--/.pageheader-->
<div class="content clearfix" id="content">
	<div class="column_left float">
		<p>
			<?php
				if ( pll_current_language() == 'et' ) {
					$role = editable_post_meta( $post->ID, 'contact_role', 'input' ); 
				} else {
					$role = editable_post_meta( $post->ID, 'contact_role_en', 'input' ); 
				}
			?>
			<?php if ( get_post_meta($post->ID, 'contact_role', true) ) : ?>
				, 
			<?php endif; ?>
			<?php
				$categories = get_the_category();
				$separator = ', ';
				$output = '';
				if( $categories ) {
					foreach($categories as $category) {
						$output .= $category->cat_name.$separator;
					}
					echo trim($output, $separator);
				}
			?>
		</p>

		<?php 
			$email = get_post_meta($post->ID, 'contact_email', true); 
			$class= ( $email ) ? '' : 'editableonly';
		?>
		<p class="<?php echo $class; ?>">
			<span class="dashicons dashicons-email"></span> 
			<a href="mailto:<?php echo $email; ?>" class="editablelink">
				<?php editable_post_meta( $post->ID, 'contact_email', 'input', false, 'Sisesta e-post' ); ?>
			</a>
		</p>

		<?php 
			$phone = get_post_meta($post->ID, 'contact_phone', true); 
			$class= ( $phone ) ? '' : 'editableonly';
		?>
		<p class="<?php echo $class; ?>">
			<span class="dashicons dashicons-phone"></span> 
			<?php editable_post_meta( $post->ID, 'contact_phone', 'input', false, 'Sisesta telefoninumber' ); ?>
		</p>

		<?php 
			$mobile = get_post_meta($post->ID, 'contact_mobile', true); 
			$class= ( $mobile ) ? '' : 'editableonly';
		?>
		<p class="<?php echo $class; ?>">
			<span class="dashicons dashicons-smartphone"></span> 
			<?php editable_post_meta( $post->ID, 'contact_mobile', 'input', false, 'Sisesta mobiilinumber' ); ?>
		</p>

		<?php 
			$www = get_post_meta($post->ID, 'contact_www', true); 
			$class= ( $www ) ? '' : 'editableonly';
		?>
		<p class="<?php echo $class; ?>">
			<span class="dashicons dashicons-admin-links"></span> 
			<a href="<?php echo $www; ?>" target="_blank" class="editablelink">
				<?php editable_post_meta( $post->ID, 'contact_www', 'input', false, 'Sisesta veebilehe link' ); ?>
			</a>
		</p>

		<?php 
			$cv = get_post_meta($post->ID, 'contact_cv', true); 
			$class= ( $cv ) ? '' : 'editableonly';
		?>
		<p class="<?php echo $class; ?>">
			<span class="dashicons dashicons-id-alt"></span> 
			<a href="<?php echo $cv; ?>" target="_blank" class="editablelink">
				<?php editable_post_meta( $post->ID, 'contact_cv', 'input', false, 'Sisesta CV link' ); ?>
			</a>
		</p>

		<div class="contentbox editable-<?php echo $post->ID; ?>">
			<?php the_content(); ?>
		</div>
	</div><!--/.column_left-->
	<div class="column_right float">
		<?php if ( current_user_can( 'edit_post' , $post->ID ) ): ?>
		<aside class="adminaside clearfix">
			<p><?php echo getPostViews( $post->ID ) ?> <?php echo __( 'vaatamist', 'artun2012' ); ?></p>

			<?php 
				$updater = get_post_meta($post->ID, 'post_last_editor', true);

				// check if we have an updater and wether this post has been update
				if ( $updater && ( strtotime( $post->post_date ) - strtotime( $post->post_modified ) !== 0 ) ) {

					$updater_info = get_userdata( $updater );

					echo '<p>' . __( 'Viimati muutis:', 'artun2012' ) . '<br />' . $updater_info->user_firstname . ' ' . $updater_info->user_lastname . ', ' . strftime( '%e. %B %Y', strtotime( $post->post_modified ) ) . '</p>';

				}
			?>
			<?php eka_post_categories(); ?>
			<span class="js_contentsave editpostlink inline primary" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-yes"></span> <?php echo __( 'Salvesta', 'artun2012' ); ?></span>
			<span class="js_contentundo editpostlink inline" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-no-alt"></span> <?php echo __( 'Tühista', 'artun2012' ); ?></span>
			<span class="js_contentedit editpostlink inline" data-edit-id="<?php echo $post->ID; ?>"><span class="dashicons dashicons-edit"></span> <?php echo __( 'Muuda', 'artun2012' ); ?></span>

			<?php if ( current_user_can( 'manage_options' )) {
				echo '<a class="editpostlink inline" href="' . get_edit_post_link( $post->ID ) . '"><span class="dashicons dashicons-admin-settings"></span> ' . __( 'Haldur', 'artun2012' ) . '</a>';
			} ?>

			<span class="js_openedit editpostlink inline"><span class="dashicons dashicons-admin-generic"></span> <?php echo __( 'Seaded', 'artun2012' ); ?></span>
			<?php eka_update_contact_form(); ?>
		</aside>
		<?php endif; ?>
	</div>

	<?php setPostViews(get_the_ID()); ?>
	
<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>