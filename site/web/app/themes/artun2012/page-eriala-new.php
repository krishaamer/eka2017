<?php
/**
 * Template Name: Eriala, NEW DESIGN
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title_new(); ?></h2>
		<?php eka_page_menu_new( "dept" ); ?>
	</div><!--/.pageheader-->

	<?php eka_page_admin(); ?>

	<div class="content clearfix" id="content">
		<div class="column_left_new float">
			<?php 
			if ( is_user_logged_in() ) : ?>
				<aside>
					<?php editable_post_meta( $post->ID, 'column_right', 'rich' ); ?>
				</aside>
			<?php else: ?>
				<?php $column_right = get_post_meta( $post->ID, 'column_right', true );
				if( $column_right != '' ) : ?>
					<aside>
						<?php echo apply_filters( 'the_content', $column_right ); ?>
					</aside>
				<?php endif; ?> 
			<?php endif; ?>
			<?php eka_the_calendar(); ?>
			<?php

				// Find connected contacts
				$contacts = new WP_Query( array(
					'connected_type' 	=> 'pages_to_contacts',
					'connected_items' 	=> get_queried_object(),
					'lang'				=> '',
					'nopaging' 			=> true,
				) );

				// Display connected contacts
				if ( $contacts->have_posts() ) :
				?>
				<aside>
					<h3><span class="bg"><?php echo __('Kontakt', 'artun2012'); ?></span></h3>
					<ul>
					<?php while ( $contacts->have_posts() ) : $contacts->the_post(); ?>
						<li class="hcard contact_card">
							<h4 class="contact_title">
								<a href="<?php the_permalink(); ?>" class="fn bg"><?php the_title(); ?></a>
							</h4>
							<p>
								<span class="bg"><?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?></span>
							</p>
							<p>
								<span class="tel bg"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span>
								<br />
								<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email bg">
									<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
								</a>
							</p>
							<p class="contact_portrait">
								<a href="<?php the_permalink(); ?>"><?php eka_first_image( $post->ID ); ?></a>
							</p>
						</li>
					<?php endwhile; ?>
					</ul>
				</aside>

				<?php 
				// Prevent weirdness
				wp_reset_postdata();
				
				endif;
			?>
			
			<?php
				// Find connected shared content bits
				/*$connected = new WP_Query( array(
				  'connected_type' => 'sharedcontent_to_pages',
				  'connected_items' => $post,
				  'nopaging' => true,
				  'connected_meta' => array( 'position' => 'sidebar' )
				) );
				
				// Display connected contect
				if ( $connected->have_posts() ) :
				?>
				<aside>
					<?php while ( $connected->have_posts() ) : $connected->the_post(); ?>
						<h3><?php the_title(); ?></h3>
						<p>
							<?php the_content(); ?>
						</p>
					<?php endwhile; ?>
				</aside>
				<?php 
				// Prevent weirdness
				wp_reset_postdata();
				
				endif;*/
			?>
		</div><!--/.column_left_new-->

		<div class="column_right_new float">
			<h3 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h3>

			<div class="contentbox editable-<?php echo $post->ID; ?>">
				<?php the_content(); ?>
			</div>

			<?php eka_the_posts(); ?>
		</div><!--/.column_right_new-->

<?php setPostViews( get_the_ID() ); ?>		

<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>