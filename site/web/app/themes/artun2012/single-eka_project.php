<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
<div class="pageheader">
	<h2 class="titlebox editable-<?php echo $post->ID; ?>" data-disable-toolbar="true" data-disable-return="true" data-placeholder="Sisesta pealkiri"><?php the_title(); ?></h2>
</div><!--/.pageheader-->
<div class="content clearfix" id="content">
	<div class="column_left float">
        <div class="contentbox editable-<?php echo $post->ID; ?>">
			<?php the_content(); ?>
		</div>       
    </div><!--/.column_left-->
    <div class="column_right float">
		<?php if ( current_user_can( 'edit_post' , $post->ID ) ): ?>
			<aside class="adminaside clearfix">
			<p><?php echo getPostViews( $post->ID ) ?> <?php echo __( 'vaatamist', 'artun2012' ); ?></p>

			<?php 
				$updater = get_post_meta($post->ID, 'post_last_editor', true);

				// check if we have an updater and wether this post has been update
				if ( $updater && ( strtotime( $post->post_date ) - strtotime( $post->post_modified ) !== 0 ) ) {

					$updater_info = get_userdata( $updater );

					echo '<p>' . __( 'Viimati muutis:', 'artun2012' ) . '<br />' . $updater_info->user_firstname . ' ' . $updater_info->user_lastname . ', ' . strftime( '%e. %B %Y', strtotime( $post->post_modified ) ) . '</p>';

				}
			?>
			<?php eka_post_categories(); ?>

			<span class="js_contentsave editpostlink inline primary" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-yes"></span> <?php echo __( 'Salvesta', 'artun2012' ); ?></span>
			<span class="js_contentundo editpostlink inline" data-save-id="<?php echo $post->ID; ?>" style="display:none;"><span class="dashicons dashicons-no-alt"></span> <?php echo __( 'Tühista', 'artun2012' ); ?></span>
			<span class="js_contentedit editpostlink inline" data-edit-id="<?php echo $post->ID; ?>"><span class="dashicons dashicons-edit"></span> <?php echo __( 'Muuda', 'artun2012' ); ?></span>

			<?php if ( current_user_can( 'manage_options' )) {
				echo '<a class="editpostlink inline" href="' . get_edit_post_link( $post->ID ) . '"><span class="dashicons dashicons-admin-settings"></span> ' . __( 'Haldur', 'artun2012' ) . '</a>';
			} ?>

			<span class="js_openedit editpostlink inline"><span class="dashicons dashicons-admin-generic"></span> <?php echo __( 'Seaded', 'artun2012' ); ?></span>
			<?php eka_update_project_form(); ?>
		</aside>
		<?php endif; ?>
	</div>


<?php setPostViews(get_the_ID()); ?>

<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>