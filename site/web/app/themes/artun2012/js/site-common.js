	jQuery( document ).ready( function() {
		
		/*
		 *	COMMON SCRIPTS
		 */
		
		// Settig defaults and variables
		jQuery.datepicker.setDefaults({
			dayNames: ["Pühapäev", "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev"],
			dayNamesMin: ["P", "E", "T", "K", "N", "R", "L"],
			monthNames: ["jaanuar","veeburar","märts","aprill","mai","juuni","juuli","august","september","oktoober","november","detsember"],
			firstDay: 1,
			prevText: "<",
			nextText: ">"
		});

		// For D&D
		var filesDropped = 0;

		// For Parallax and other fancy folks
		var $window = jQuery(window),
			$gallery = jQuery( 'div[data-type="gallery"]' ),
			wheight = $window.height(),
			gheight = $gallery.outerHeight(),
			cheight = jQuery( 'body' ).height(),
			difference,
			movement;
		
		// Setting the hovering variable and changing it when hovering on specific elements. 
		var hovering = 0;
		jQuery('body').on(
			{
				mouseenter: function()
				{
					hovering = 1;
				},
				mouseleave: function()
				{
					hovering = 0;
				}
			},
			'.deletelink, .fee-hover-container'
		);
		
		
		// Hiding and ordering things. 
		jQuery('#comments label').hide();
		jQuery('.comment-form-comment').insertBefore('.comment-form-author');

		// Display posts content on clicking the title
		jQuery( '#eka_posts_container' ).on( 'click', '.articletitle, .articlethumb', function( event ) {
			var $tgt = jQuery( event.target ).closest( 'article' );
			$tgt.find( '.articlethumb' ).toggle( 100 );
			$tgt.find( '.articlecontent' ).toggleClass( 'opencontent' );
			$tgt.find( 'h3' ).toggleClass( 'opentitleholder' );

			// Try to get and insert any images that might accompany the post
			var params = {
				"parentpost"	: jQuery( this ).data( 'post' ),
				"action"		: "load_postgallery"
			};

			jQuery.post( ekaajax.ajaxurl, params, function ( data ) {
				if( data ){
					jQuery( '#gallery-' + params.parentpost ).html( data );
				}
			});
		});

		// Show comment form details
		jQuery('#comment').focus(function() {
			jQuery('#commentform p').addClass('openview');
		});

		// Detect IE8 & earlier, add body class
		if (!jQuery.support.leadingWhitespace) {
			jQuery('body').addClass('ie');
		}

		// Show messages, if there are any
		if (jQuery("#eka_messages").text().length > 0) {
			jQuery('#eka_messages').show().delay(3000).hide(100);
		}

		// Showing department page add department links
		jQuery('#addTab').click(function(){
			jQuery(".emptypage:hidden:first").show('fast');
			
			if( jQuery('.emptypage:hidden:first').length === 0 ) {
				jQuery('#addTab').hide();
			}
		});

		// Show the search field on button click
		jQuery( '#searchbegin' ).click( function() {
			jQuery( this ).toggleClass( 'pointed_search' );
			
			jQuery( 'body' ).one( 'click' , function( event ) {
				if( jQuery(event.target).parents( '#quicksearch' ).length === 0 ) {
					jQuery( '#searchbegin' ).removeClass( 'pointed_search' );
				}
			});
			
			event.stopImmediatePropagation();
		});
			
		// NAVIGATION

		jQuery( '#mainnav' ).on( 'click', 'span', function( event ){
			jQuery( '#mainnav span' ).removeClass( 'pointed_page_item' );
			jQuery( event.target ).addClass( 'pointed_page_item' );

			// Fixing IE 7&8
			if ( !jQuery.support.leadingWhitespace ) {
				jQuery( '#mainnav' ).addClass( 'ieFix' ).removeClass( 'ieFix' );
			}

			// Open other menus on hovering them
			jQuery( '#mainnav' ).on( 'mouseover.menuinteract', 'span', function( event ){
				jQuery( '#mainnav span' ).removeClass( 'pointed_page_item' );
				jQuery( event.target ).addClass( 'pointed_page_item' );

				if ( !jQuery.support.leadingWhitespace ) {
					jQuery( '#mainnav' ).addClass( 'ieFix' ).removeClass( 'ieFix' );
				}
			});

			// Close on clicks outside
			jQuery( 'body' ).one( 'click' , function( event ) {
				if( jQuery(event.target).parents( '#mainnav' ).length === 0 ) {
					jQuery( '#mainnav span' ).removeClass( 'pointed_page_item' );
					jQuery( '#mainnav' ).off( 'mouseover.menuinteract' );
				}
			});

			event.stopImmediatePropagation();
		});

		jQuery( '#pagenavnew' ).on( 'click', 'span', function( event ){
			jQuery( '#pagenavnew span, #pagenavnew a' ).removeClass( 'pointed_page_item' );
			jQuery( event.target ).addClass( 'pointed_page_item' );

			// Open other menus on hovering them
			jQuery( '#pagenavnew' ).on( 'mouseover.menuinteract', 'span', function( event ){
				jQuery( '#pagenavnew span' ).removeClass( 'pointed_page_item' );
				jQuery( event.target ).addClass( 'pointed_page_item' );
			});

			// Close on clicks outside
			jQuery( 'body' ).one( 'click' , function( event ) {
				if( jQuery(event.target).parents( '#pagenavnew' ).length === 0 ) {
					jQuery( '#pagenavnew span' ).removeClass( 'pointed_page_item' );
					jQuery( '#pagenavnew' ).off( 'mouseover.menuinteract' );
				}
			});

			event.stopImmediatePropagation();
		});

		
		// Detect window width
		var posTop;
		if ( jQuery(window).width() < 850 ) {
			posTop = 35;
		} else {
			posTop = 28;
		}
		jQuery(window).resize(function() {
			if ( jQuery(window).width() < 850 ) {
				posTop = 35;
			} else {
				posTop = 28;
			}
		});
		
		// Make the header sticky after scrolling to a certain point
		var userScrolled = false;
		var userScrolledTwo = false;
		jQuery(window).scroll(function() {
			userScrolled = true;
			userScrolledTwo = true;
		});
		
		var $header = jQuery( 'header ');
		setInterval( function() {
			if ( userScrolled ) {
			
				if ( $window.scrollTop() > posTop ){
					$header.addClass( 'fixed' );
				} else {
					$header.removeClass( 'fixed' );
				}
				userScrolled = false;
			}
		}, 50);
		
		// Open and close the mobile nav menu + move the content
		jQuery( '#opennav' ).click( function() {
			jQuery("body").toggleClass("navopened");
		});
		
		jQuery( '#closenav' ).click( function() {
			jQuery("body").removeClass("navopened");
		});


		// Show and hide the tags menu if there are more components

		if ( jQuery( '#tagslist .tagoption' ).length > 5 ) {
			jQuery( '#moretags' ).show();
		}

		jQuery( '#moretags' ).click( function() {
			jQuery( '#tagslist .tagoption:gt(3)' ).show( 'slide' ).css('display','inline');
			jQuery( this ).hide();
		});
		
		// FRONT PAGE MENU

		// Open the links overlay
		jQuery( '.clickable' ).click( function( event ){

			var $target = jQuery(event.target);
			var selmenu = $target.attr("class").split(' ');
			var state = selmenu[2];
												
			jQuery(".indexpane").hide();
			jQuery('.indexanchor').addClass('clickable').removeClass('closeable');
			

			if ( state == 'clickable' ) {
				jQuery("#indexlinks ul li div#"+selmenu[0]).show();
				$target.removeClass('clickable').addClass('closeable');
			}

			// Close on clicks outside
			jQuery( 'body' ).one( 'click', function( event ) {
				if( event.target !== ".closeable" && event.target !== ".indexpane" && jQuery(event.target).parents('.indexpane').length === 0 ) {
					jQuery('.indexpane').hide();
					jQuery('.indexanchor').addClass('clickable').removeClass('closeable');
				}
			});

			event.stopImmediatePropagation();
		});

		// Switch degrees
		/*jQuery(".indexpane ul li b").click( function( event ){
			var $target = jQuery(event.target);
			var seldeg = $target.attr("id");
			
			jQuery('.indexpane ul li b').removeClass('selected');
			$target.addClass('selected');
						
			if ( seldeg != 'all' ) {
				jQuery('.indexpane ul li.degrees').hide();
				jQuery('.indexpane ul li.degrees.'+seldeg).show();
			} else {
				jQuery('.indexpane ul li.degrees').show();
			}

			event.stopImmediatePropagation();
		});*/
		

		// CONTACTS LIST LOADING

		// Activate on click
		jQuery( '#contacts_content' ).on( 'click', '.contact_list_click', function( event ) {
			jQuery( '.list_contact_row' ).hide();

			load_contacts( jQuery( event.target ).data( 'catid' ) );
			jQuery( '#contacts_content span' ).removeClass( 'current_contact_item' );
			jQuery( this ).removeClass( 'contact_list_click' ).addClass( 'contact_list_toggle current_contact_item' );

		});

		// Hide/show on following clicks
		jQuery( '#contacts_content' ).on( 'click', '.contact_list_toggle', function( event ) {
			jQuery( '.list_contact_row' ).hide();
			jQuery( '#catbox_' + jQuery( event.target ).data( 'catid' ) ).toggle( 'fast' );
			jQuery( this ).toggleClass( 'current_contact_item' );
		});

		// Requests and inserts more contacts
		function load_contacts( contactscat ) {
			
			var loader = jQuery( '<div class="dynamic-loading"></div>' ).appendTo( '#catbox_' + contactscat );
			setTimeout(function() { loader.addClass('loading-open'); }, 1);

			var params = {
				"cat"		: contactscat,
				"action"	: "load_contacts"
			};

			jQuery.post( ekaajax.ajaxurl, params, function (data) {
				if( data ) {

					var $tgtbox = jQuery( '#catbox_' + contactscat );
					$tgtbox.append( data );


					var	offset = $tgtbox.offset(),
						$wrapbox = jQuery( '#contacts_content' ),
						newheight = jQuery( '#catbox_' + contactscat + '> ul' ).height() + offset.top;

					// check if we need to make the page longer
					if ( $wrapbox.height() < newheight ) {
						$wrapbox.height( newheight - 200 );
					}

					jQuery( '.sortable' ).sortable({
						appendTo: "body",
						helper: "clone",
						handle: ".handle",
						update: function(event, ui) {
							jQuery('#loading-animation').show(); // Show the animate loading gif while waiting
									
							jQuery.ajax({
								url: ekaajax.ajaxurl,
								type: 'post',
								async: true,
								cache: false,
								data: {
									action: 'item_sort',
									order: jQuery(this).sortable( 'toArray', { attribute: 'class' } ).toString()
								},
								success: function(response) {
									jQuery('#loading-animation').hide();
									return;
								},
								error: function(xhr,textStatus,e) {
									jQuery('#loading-animation').hide();
									return;
								}
							});
						}
					});

					loader.remove();
						
				}

				
			});

		}


		// INFINITE SCROLLING
		var loading = 0, post_offset = 10, post_offset_front = 0;

		// NEWS FEED

		// Requests and inserts more posts
		function load_more_posts() {
			if(loading) return true;
			if(!loading) {
			
				jQuery('#loading-animation').show();
				loading = 1;

				var params = {
					"offset"	: post_offset,
					"page"		: jQuery('#load_more_posts').data('page'),
					"tag"		: jQuery('#load_more_posts').data('tag'),
					"action"	: "load_more"
				};

				jQuery.post( ekaajax.ajaxurl, params, function (data) {
					if( data ){
						post_offset += 10;
						loading = 0;
						jQuery( '#eka_posts_container' ).append(data);
					}
					jQuery( '#loading-animation' ).hide();
				});
			}
		}


		// Initiate the first load and scrolling detection on first click
		jQuery('#load_more_posts').click(function(){

			// Function to check if element is in the viewport

			jQuery.fn.isOnScreen = function(){
				 
				//var win = jQuery(window);
				 
				var viewport = {
					top : $window.scrollTop(),
					left : $window.scrollLeft()
				};
				viewport.right = viewport.left + $window.width();
				viewport.bottom = viewport.top + $window.height();
				 
				var bounds = this.offset();
				bounds.right = bounds.left + this.outerWidth();
				bounds.bottom = bounds.top + this.outerHeight();
				 
				return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
				 
			};
			
			load_more_posts();

			jQuery('#load_more_posts').hide();

			setInterval( function() {
				if ( userScrolled ) {
					if( jQuery( '#eka_posts_container article:last' ).isOnScreen() ) {
						load_more_posts();
					}
				}
			}, 100);
		});


		// FRONT PAGE

		// Requests and inserts more banners
		function load_frontpage() {
			if(loading) return true;
			if(!loading) {
			
				jQuery( '#loading-animation' ).show();
				loading = 1;

				var params = {
					"offset"	: post_offset_front,
					"action"	: "load_frontpage"
				};

				jQuery.post( ekaajax.ajaxurl, params, function (data) {
					if( data ){
						post_offset_front += 10;
						loading = 0;
						jQuery( '#gallery' ).append( data );
					}
					jQuery( '#loading-animation' ).hide();
				});
			}
		}

		// Activates scroll detection of the home page
		if ( jQuery( 'body' ).hasClass( 'home' ) ) {
			
			load_frontpage();

			// Function to check if element is in the viewport

			jQuery.fn.isOnScreen = function(){
				 
				//var win = jQuery(window);
				 
				var viewport = {
					top : $window.scrollTop(),
					left : $window.scrollLeft()
				};
				viewport.right = viewport.left + $window.width();
				viewport.bottom = viewport.top + $window.height();
				 
				var bounds = this.offset();
				bounds.right = bounds.left + this.outerWidth();
				bounds.bottom = bounds.top + this.outerHeight();
				 
				return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
				 
			};

			setInterval( function() {
				if ( userScrolledTwo ) {
					if( jQuery( '#gallery .imagewrapper:last' ).isOnScreen() ) {
						load_frontpage();
					}
					userScrolledTwo = false;
				}
			}, 200);
		}

		var loadingcurated = 0;
		// Curated content loading
		function load_curated( type ) {
			if(loadingcurated) return true;
			if(!loadingcurated) {
			
				jQuery('#loading-animation').show();
				loadingcurated = 1;

				var params = {
					"type"		: type,
					"orderby"	: jQuery('#curatedselect').data('orderby'),
					"action"	: "load_curated"
				};

				jQuery.post( ekaajax.ajaxurl, params, function (data) {
					if( data ){
						loadingcurated = 0;
						jQuery( '#curatedcontainer' ).html(data);
					}
					jQuery( '#loading-animation' ).hide();
				});
			}
		}

		jQuery( '#togglecurated' ).click( function() {
			if ( jQuery( '#usercuratedcontent' ).is( ':visible' ) ) {
				jQuery( '#usercuratedcontent' ).hide(300);
				jQuery( '#togglecurated, #curatedtabs li' ).removeClass( 'current_page_item' );
				jQuery( '#curatedtabs li:first' ).addClass( 'current_page_item' );
			} else {
				jQuery( '#usercuratedcontent' ).show(300);
				jQuery( '#togglecurated' ).addClass( 'current_page_item' );
				load_curated( 'page' );
			}
		});

		jQuery( '#closecurated' ).click( function() {
			jQuery( '#usercuratedcontent' ).hide(300);
			jQuery( '#togglecurated, #curatedtabs li' ).removeClass( 'current_page_item' );
			jQuery( '#curatedtabs li:first' ).addClass( 'current_page_item' );
		});

		jQuery( '#curatedtabs li' ).click( function( event ) {
				jQuery( '#curatedtabs li' ).removeClass( 'current_page_item' );
				jQuery( event.target ).addClass( 'current_page_item' );
				load_curated( jQuery( event.target ).data( 'type' ) );
		});

		
		/*
		 *	PRIVATE SCRIPTS
		 */
		
		/*
		* DISPLAYING AND HANDLING THE ADD CONTENT FORM
		*/
		// Show the form
		jQuery( '#addPostLink' ).click(function(){
			jQuery( '#addPostForm').toggle(300);
			editor = new MediumEditor( '.editable-newpost', editorOptions );
		});
		
		jQuery( '#delPostLink' ).click(function(){
			jQuery( '#addPostForm' ).toggle(300);
			editor.destroy();
		});

		// Show the form
		jQuery( '#addGalleryLink' ).click(function(){
			jQuery( '#addGalleryForm').toggle(300);
			editor = new MediumEditor( '.editable-newgallery', editorOptions );
		});
		
		jQuery( '#delGalleryLink' ).click(function(){
			jQuery( '#addGalleryForm' ).toggle(300);
			editor.destroy();
		});

		// Show the form
		jQuery( '#addEventLink' ).click(function(){
			jQuery( '#addEventForm').toggle(300);
			editor = new MediumEditor( '.editable-newevent', editorOptions );
		});
		
		jQuery( '#delEventLink' ).click(function(){
			jQuery( '#addEventForm' ).toggle(300);
			editor.destroy();
		});

		// Show the edit post form
		jQuery( '#container' ).on( 'click', '.js_openedit', function() {
			var tgtform = jQuery( this ).nextAll( '.js_edit_post' );
			tgtform.slideDown( 300 );

			jQuery( '.js_edit_post label' ).not( '.display' ).hide(); // for non-javascript compatibility
		});

		jQuery( '#container' ).on( 'click', '.js_closeedit', function() {
			jQuery( this ).parents( 'form' ).slideUp( 300 );
		});


		jQuery( '#container' ).on( 'click', '.js_frontpage_show', function() {
			
			if( jQuery( this ).is(':checked')) {
				jQuery( this ).parent( 'p' ).next( '.js_editpost_ad' ).show( 300 );
			} else {
				jQuery( this ).parent( 'p' ).next( '.js_editpost_ad' ).hide( 300 );
			}
		});

		jQuery( '#eka_english_show' ).click(function() {
			if( jQuery(this).is(':checked')) {
				jQuery("#english_ad").show( 300 );
			} else {
				jQuery("#english_ad").hide( 300 );
			}
		});

		jQuery( '#eka_english_event_show' ).click(function() {
			if( jQuery(this).is(':checked')) {
				jQuery("#english_event_ad").show( 300 );
			} else {
				jQuery("#english_event_ad").hide( 300 );
			}
		});

		jQuery( '.js_frontpage_datepicker' ).datepicker({
			dateFormat: "DD, d MM yy",
			minDate: -1
		});

		jQuery( '#en_title' ).focus( function() {
			jQuery( '#en_content' ).show( 200 );
		});

		jQuery( '#en_event_title' ).focus( function() {
			jQuery( '#en_event_content' ).show( 200 );
		});

		// ****
		// Editing content front-end vith the Medium Editor
		// ****

		// Enable editor on fields with class "editable"
		var editor, currentEdit, currentFields = [], editorOptions = {
			toolbar: {
				buttons: [
					{
						name: 'bold',
						aria: 'paks tekst',
						contentDefault: '<span class="dashicons dashicons-editor-bold"></span>'
					},
					{
						name: 'italic',
						aria: 'kursiiv',
						contentDefault: '<span class="dashicons dashicons-editor-italic"></span>'
					},
					{
						name: 'h3',
						aria: 'muuda pealkirjaks',
						contentDefault: 'H1'
					},
					{
						name: 'h4',
						aria: 'muuda alapealkirjaks',
						contentDefault: 'H2'
					},
					{
						name: 'anchor',
						aria: 'muuda lingiks',
						contentDefault: '<span class="dashicons dashicons-admin-links"></span>'
					},
					{
						name: 'orderedlist',
						aria: 'numbritega nimekiri',
						contentDefault: '<span class="dashicons dashicons-editor-ol"></span>'
					},
					{
						name: 'unorderedlist',
						aria: 'punktidega nimekiri',
						contentDefault: '<span class="dashicons dashicons-editor-ul"></span>'
					},
					'table',
					{
						name: 'removeFormat',
						aria: 'eemalda vormindus',
						contentDefault: '<span class="dashicons dashicons-editor-removeformatting"></span>'
					}
				]
			},
			placeholder: {
				   text: 'Sisu lisamiseks kliki siia'
			},
			anchor: {
				linkValidation: true,
				placeholderText: 'Sisesta siia link',
				targetCheckbox: true,
				targetCheckboxText: 'Ava uues aknas'
			},
			paste: {
				forcePlainText: true,
				cleanPastedHTML: true,
				cleanReplacements: [],
				cleanAttrs: ['class', 'style', 'dir', 'face', 'size', 'color'],
				cleanTags: ['meta']
			},
			autoLink: true,
			imageDragging: false,
			extensions: {
				'table': new MediumEditorTable({
					contentDefault: '<span class="dashicons dashicons-editor-table"></span>',
					contentFA: '<span class="dashicons dashicons-editor-table"></span>'
				})
			}
		};

		// Enable fields editing
		jQuery( '#container' ).on( 'click', '.js_contentedit', function() {
			currentEdit = jQuery( this ).data( 'edit-id' );

			editor = new MediumEditor( '.editable-' + currentEdit, editorOptions );

			jQuery( '.editable-' + currentEdit ).each(function(i, obj) {
			    currentFields[i] = jQuery( this ).html(); 
			});

			jQuery( 'body' ).addClass( 'editing' );
			jQuery( '.js_contentsave[data-save-id="' + currentEdit + '"]' ).show( 300 );
			jQuery( '.js_contentundo[data-save-id="' + currentEdit + '"]' ).show( 300 );
			jQuery( this ).hide( 300 );
		});

		// Save changes!
		jQuery( '#container' ).on( 'click', '.js_contentsave', function() {
			currentEdit = jQuery( this ).data( 'save-id' );

			editor.destroy();
			saveChanges( currentEdit );

			jQuery( 'body' ).removeClass( 'editing' );
			jQuery( '.js_contentedit[data-edit-id="' + currentEdit + '"]' ).show( 300 );
			jQuery( '.js_contentundo[data-save-id="' + currentEdit + '"]' ).hide( 300 );
			jQuery( this ).hide( 300 );
		});

		// Cancel!
		jQuery( '#container' ).on( 'click', '.js_contentundo', function() {
			currentEdit = jQuery( this ).data( 'save-id' );

			jQuery( '.editable-' + currentEdit ).each(function(i, obj) {
			    jQuery( this ).html( currentFields[i] ); 
			});

			editor.destroy();

			jQuery( 'body' ).removeClass( 'editing' );
			jQuery( '.js_contentedit[data-edit-id="' + currentEdit + '"]' ).show( 300 );
			jQuery( '.js_contentsave[data-save-id="' + currentEdit + '"]' ).hide( 300 );
			jQuery( this ).hide( 300 );
		});

		// Make currently edited links not click
		jQuery( 'html' ).on( 'click', '.editing .editablelink', function( event ) {
			event.preventDefault();
		});

		jQuery( '#dropdown-toggle' ).click(
			function () {
				jQuery( '#dropdown-menu' ).slideDown( 100 );
			}
		);

		jQuery( '#dropdown-menu' ).click(
			function () {
				jQuery( '#dropdown-menu' ).slideUp( 100 );
			}
		);



		// Generic test to see whether an element and attribute are supported in the current browser
		function elementSupportsAttribute(element,attribute) {
			var test = document.createElement(element);
			if (attribute in test) {
				return true;
			} else {
				return false;
			}
		}
		
		// Things to do when user's browser doesn't support "placeholder" attribute on inputs
		if (!elementSupportsAttribute('input','placeholder')) {
			jQuery('[placeholder]').focus(function() {
				var input = jQuery(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
					input.removeClass('placeholder');
				}
			}).blur(function() {
				var input = jQuery(this);
				if (input.val() === '' || input.val() == input.attr('placeholder')) {
					input.addClass('placeholder');
					input.val(input.attr('placeholder'));
				}
			}).blur();
			jQuery('[placeholder]').parents('form').submit(function() {
				jQuery(this).find('[placeholder]').each(function() {
					var input = jQuery(this);
					if (input.val() == input.attr('placeholder')) {
						input.val('');
					}
				});
			});
		}
		
		// "Textarea" styling.
		
		/*jQuery( '#addpost_content' ).click(function() {
			if ( jQuery(this).text() == 'Lisa sisu' || jQuery(this).text() == 'Lisa sisutekst' ) {
				jQuery(this).html('<br>');
			}
		});*/
		
		
		// On form submit, copy the content from editor div to our hidden textarea
		// On form submit, validate if required fields have content
		jQuery( '#new_post' ).submit(function( event ) {
			if ( filesDropped == 1 ) {
				event.preventDefault();
				XHRuploadFiles();
			}

			jQuery( '#new_post_submit' ).hide();
			jQuery( '#new_post_loading' ).show();
			
			var content = jQuery('div#addpost_content').html();
			jQuery("textarea#addpost_submit_content").html(content);
		
			var isFormValid = true;
			jQuery("#new_post input.required:text").each(function(){
				if (jQuery.trim(jQuery(this).val()).length === 0){
					jQuery(this).addClass("highlight");
					isFormValid = false;
				} else {
					jQuery(this).removeClass("highlight");
				}
			});

			if ( jQuery( '#eka_frontpage_show' ).is(':checked') && !jQuery( '#frontpage_datepicker' ).val() ) {
				jQuery( '#frontpage_datepicker' ).addClass("highlight");
				isFormValid = false;
			}

			if (!isFormValid) alert("Palun sisesta kõik vajalikud väljad.");
			return isFormValid;
		});
		
		jQuery( "#new_image" ).submit(function( event ) {
			if ( filesDropped == 1 ) {
				event.preventDefault();
				XHRuploadFiles();
			}
			jQuery( '#new_image_submit' ).hide();
			jQuery( '#new_image_loading' ).show();
		});

		jQuery( '#new_gallery' ).submit(function( event ) {
			if ( filesDropped == 1 ) {
				event.preventDefault();
				XHRuploadFiles();
			}

			jQuery( '#new_gallery_submit' ).hide();
			jQuery( '#new_gallery_loading' ).show();
			
			var content = jQuery('div#addgallery_content').html();
			jQuery("textarea#addgallery_submit_content").html(content);
		
			var isFormValid = true;
			jQuery("#new_gallery input.required:text").each(function(){
				if (jQuery.trim(jQuery(this).val()).length === 0){
					jQuery(this).addClass("highlight");
					isFormValid = false;
				} else {
					jQuery(this).removeClass("highlight");
				}
			});

			/*if ( jQuery( '#eka_frontpage_show' ).is(':checked') && !jQuery( '#frontpage_datepicker' ).val() ) {
				jQuery( '#frontpage_datepicker' ).addClass("highlight");
				isFormValid = false;
			}*/

			if (!isFormValid) alert("Palun sisesta kõik vajalikud väljad.");
			return isFormValid;
		});

		jQuery( '#new_event' ).submit(function( event ) {
			if ( filesDropped == 1 ) {
				event.preventDefault();
				XHRuploadFiles();
			}

			jQuery( '#new_post_submit' ).hide();
			jQuery( '#new_post_loading' ).show();
			
			var content = jQuery('div#addevent_content').html();
			jQuery("textarea#addevent_submit_content").html(content);
		
			var isFormValid = true;
			jQuery("#new_event input.required:text").each(function(){
				if (jQuery.trim(jQuery(this).val()).length === 0){
					jQuery(this).addClass("highlight");
					isFormValid = false;
				} else {
					jQuery(this).removeClass("highlight");
				}
			});

			if ( jQuery( '#eka_frontpage_event_show' ).is(':checked') && !jQuery( '#frontpage_event_datepicker' ).val() ) {
				jQuery( '#frontpage_event_datepicker' ).addClass("highlight");
				isFormValid = false;
			}

			if (!isFormValid) alert("Palun sisesta kõik vajalikud väljad.");
			return isFormValid;
		});
		
		// Autosuggest on tag selection
		jQuery('#post_tags').suggest( ekaajax.ajaxurl + '?action=ajax-tag-search&tax=calendar_tag', {multiple:true, multipleSep: ","} );
		jQuery('#event_tags').suggest( ekaajax.ajaxurl + '?action=ajax-tag-search&tax=calendar_tag', {multiple:true, multipleSep: ","} );
		
		
		
		// FILE uploading magic bogus. YES we're implementing drag&drop because it's 2013
		if( window.FormData !== undefined ) {
			formdata = new FormData();
		}
		
		// Loop that reads and represents file information	
		function uploadFile (file) {
			var reader;
						
			//	If the file is an image and the web browser supports FileReader, present a preview or file name in the file list
			if (typeof FileReader !== "undefined" && (/image/i).test(file.type)) {
				reader = new FileReader();
				reader.onload = function (e) {
					jQuery( '<img />' ).attr({ 'src': e.target.result, 'alt': file.name, 'title': file.name }).appendTo(jQuery('#listAddImages, #listAddFiles, #listAddGalleryFiles, #listAddEventFiles'));
					jQuery( '#content' ).css( 'min-height', jQuery( '#addImageForm' ).height() );
				};
				reader.readAsDataURL(file);
			} else {
				jQuery( '#listAddImages, #listAddFiles, #listAddGalleryFiles, #listAddEventFiles' ).append( '<p>'+file.name+'</p>' );
				jQuery( '#content' ).css( 'min-height', jQuery( '#addImageForm' ).height() );
			}
			
			if (formdata) {
				formdata.append("upload_attachment[]", file);
			}
			
		}
		
		// Checks if browser supports file upload and if does, loops every file to give output.
		function traverseFiles (files) {
			
			if (typeof files !== "undefined") {
				for (var i=0, l=files.length; i<l; i++) {
					uploadFile(files[i]);
				}
			}
			else {
				jQuery('#listAddImages, #listAddFiles, #listAddGalleryFiles, #listAddEventFiles').append('Teie veebilehitseja ei võimalda kahjuks piltide üleslaadimist. Palun uuenda lehitsejat.');
			}
		}
			
		jQuery(document).on("change", "#addImages, #addFiles, #addGalleryFiles, #addEventFiles", function () {
			jQuery('#listAddImages, #listAddFiles, #listAddGalleryFiles, #listAddEventFiles').empty();
			filesDropped = 0;

			traverseFiles(this.files);
			$activeForm = jQuery(this).closest('form');
		});
		
		// Drag & Drop Magics
		
		jQuery.event.props.push('dataTransfer');
	
		jQuery(".dropArea").on('dragenter', function(event) {
			jQuery(this).addClass('dropnow');
			
			event.preventDefault();
			event.stopPropagation();
		});
		
		jQuery(".dropArea").on('dragleave', function(event) {
			jQuery(this).removeClass('dropnow');
			event.preventDefault();
			event.stopPropagation();
		});
		
		jQuery("body").on('dragover', function(event) {
			event.preventDefault();
			event.stopPropagation();
								
			return false;
		});
		
		jQuery("body").on('drop', function(event) {
			
			event.preventDefault();
			event.stopPropagation();
			
			if ( jQuery(event.target).hasClass( 'dropArea' ) ) {
				$activeForm = jQuery(event.target).closest('form');
				jQuery('#listAddImages, #listAddFiles, #listAddGalleryFiles').empty();
				traverseFiles(event.dataTransfer.files);
				filesDropped = 1;
			}
			jQuery( ".dropArea" ).removeClass('dropnow').height('auto');
								
		});
		
		jQuery("body").on('dragenter', function(event) {
			event.preventDefault();
			event.stopPropagation();
						
			if ( !jQuery( ".dropArea" ).hasClass('expanded') ) {
				jQuery( ".dropArea" ).addClass('expanded').animate({
					height: "300px"
				}, { duration: "slow" });
			}
							
			return false;
		});
		
		jQuery("body").on('dragleave', function(event) {
			jQuery( ".dropArea" ).animate({
				height: "auto"
			}, { duration: "slow" });
			event.preventDefault();
			event.stopPropagation();
			return false;
		});

			
		
		// Front page ad functions
		jQuery( "#frontpage_datepicker" ).datepicker({
			dateFormat: "DD, d MM yy",
			minDate: -1,
			altField: "#frontpage_datepicker_submit",
			altFormat: "yymmdd"
		});
		
		jQuery('#eka_frontpage_show').click(function() {
			if( jQuery(this).is(':checked')) {
				jQuery("#addpost_ad").show( 300 );
			} else {
				jQuery("#addpost_ad").hide( 300 );
			}
		});

		jQuery( "#frontpage_event_datepicker" ).datepicker({
			dateFormat: "DD, d MM yy",
			minDate: -1,
			altField: "#frontpage_event_datepicker_submit",
			altFormat: "yymmdd"
		});
		
		jQuery('#eka_frontpage_event_show').click(function() {
			if( jQuery(this).is(':checked')) {
				jQuery("#addevent_ad").show( 300 );
			} else {
				jQuery("#addevent_ad").hide( 300 );
			}
		});		
		
		// Calendar add timepicker
		jQuery( "#startdate_datepicker" ).datepicker({
			dateFormat: "DD, d MM yy",
			altField: "#startdate_datepicker_submit",
			altFormat: "yymmdd",
			onClose: function( selectedDate ) {
				jQuery( "#enddate_datepicker" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		
		jQuery( "#enddate_datepicker" ).datepicker({
			dateFormat: "DD, d MM yy",
			altField: "#enddate_datepicker_submit",
			altFormat: "yymmdd",
			onClose: function( selectedDate ) {
				jQuery( "#startdate_datepicker" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
		
		
		// Add image form
		// Show the form
		jQuery( '#addImageLink' ).click( function(){
			jQuery( '#addImageForm' ).toggle();
			jQuery( '#gallery' ).css( { 'z-index' : '5' } );

		});
		jQuery( '#delImageLink' ).click( function(){
			jQuery( '#addImageForm' ).toggle();
			jQuery( '#gallery' ).css( { 'z-index' : 'inherit' } );
		});

		/*
		*	ACTIONS IN LOGGED IN HEADER
		*/

		jQuery("#firstlogin").submit(function(e) {
			if(!jQuery('input[type=checkbox]:checked').length) {
				alert("Palun valige vähemalt üks osakond.");

				//stop the form from submitting
				return false;
			}

			return true;
		});
		
		/*
		* DISPLAYING AND MANIPULATING BACKGROUND IMAGES
		*/
		
		jQuery( "#gallery" ).on( "click", ".click_link", function() {
			var link_to = jQuery(this).attr("data-post_guid");
			window.location.href=link_to;
		});
				
		// Calendar date picker
		jQuery( "#calendar_datepicker" ).datepicker({
				dateFormat: "yymmdd",
				onSelect: function(dateText, inst) {
					dateLocation = '?aeg=' + dateText;
					window.location = dateLocation;
					return false;
				}
			});
				
		// News ticker
		function tick(){
			jQuery('#newsticker li:first').slideUp( function () { jQuery(this).appendTo(jQuery('#newsticker')).slideDown(); });
		}
		
		jQuery('#newsticker').mouseover(function() {
			window.clearInterval(intervalID);
		}).mouseout(function(){
			intervalID = window.setInterval(function(){tick();}, 4000);
		});
		
		var intervalID = window.setInterval(function(){tick();}, 4000);

		// Modal popup
		jQuery( document ).on( "click", "#openModal", function() {
			jQuery( '#modalContainer' ).addClass( 'open' );
		});

		jQuery( document ).on( "click", "#closeModal", function() {
			jQuery( '#modalContainer' ).removeClass( 'open' );
		});
		
		// AJAX Magic
		
		// Loading and displaying large images when smaller clicked
		function loadLarge( post_id ) {
			jQuery( '#loading-animation' ).show();
			jQuery.ajax({
				type : 'post',
				dataType : 'html',
				url : ekaajax.ajaxurl,
				data : { action: 'load_large_image', post_id : post_id, _ajax_nonce: ekaajax.ajaxnonce },
				success: function(response) {
					jQuery( '#imageOverlay img, #imageOverlay p' ).remove();
					jQuery( '#imageOverlay' ).append( response );
					jQuery( '#loading-animation' ).hide();
				}
			});
		}

		// Showing next/prev links where applicable
		function showNextPrev( post_id ) {
			var nextID = jQuery( '#' + post_id ).next( '.imagewrapper' ).find( '.click_attachment' ).data( 'post_id' ),
				prevID = jQuery( '#' + post_id ).prev( '.imagewrapper' ).find( '.click_attachment' ).data( 'post_id' );

			jQuery( '#nextPrev' ).html( '' );

			if ( typeof nextID != 'undefined' ) {
				jQuery( '#nextPrev' ).append( '<span class="next" data-post_id="' + nextID + '">&rarr;</span>' );
			}

			if ( typeof prevID != 'undefined' ) {
				jQuery( '#nextPrev' ).append( '<span class="prev" data-post_id="' + prevID + '">&larr;</span>' );
			}

			jQuery( window ).keyup(function (e) {
				var key = e.which;
				if( key == 13 || key == 39 || key == 37 ) { 
					$( '#nextprev .next' ).click();
					return false;  
				} else if(key == 37) { // left arrow
					$( '#nextprev .prev' ).click();
					return false;  
				}
			});
		}

		jQuery( '#container' ).on( 'click', '.click_attachment', function( event ) {
						
			// Check if there isnt an active edit field nearby
			if( ( !jQuery( '.js_contentsave.imageedit' ).is( ':visible' )) ) {

				var $target = jQuery( event.target );

				// Check we're not clicking on the delete link	
				if ( !$target.hasClass( 'deletelink' ) && !$target.hasClass( 'imageedit' ) && !$target.hasClass( 'dashicons' ) ) {

					jQuery( 'body' ).append( '<div id="imageOverlay" class="imageoverlay"><div id="closeOverlay" class="closeoverlay">×</div><div id="nextPrev" class="nextprev"></div></div>' );

					loadLarge( jQuery( this ).attr( 'data-post_id' ) );
					showNextPrev( jQuery( this ).attr( 'data-post_id' ) );
				}
			}
		});
		jQuery( 'body' ).on( 'click', '#closeOverlay' , function( event ){
			jQuery( '#imageOverlay' ).remove();
		});
		jQuery( 'body' ).on( 'click', '#nextPrev span' , function( event ){
			loadLarge( jQuery( this ).attr( 'data-post_id' ) );
			showNextPrev( jQuery( this ).attr( 'data-post_id' ) );
		});
		
		// Deleting images
		jQuery( 'body' ).on( 'click', '.js-deleteLink', function() {
								
			if ( confirm( 'Oled kindel, et soovid pildi kustutada?' ) ) {

				jQuery( this ).closest( '.imagewrapper, .imagewrapperlarge' ).fadeOut();
				jQuery.ajax( {
						type : 'post',
						dataType : 'html',
						url : ekaajax.ajaxurl,
						data : { action: 'delete_attachment', att_id : jQuery(this).attr( 'data-post_id' ), _ajax_nonce: ekaajax.ajaxnonce }
				} );

			}
		});
		
		// Sorting contacts	
		jQuery( '.sortable' ).sortable({
			appendTo: "body",
			helper: "clone",
			handle: ".handle",
			update: function(event, ui) {
				jQuery('#loading-animation').show(); // Show the animate loading gif while waiting
						
				jQuery.ajax({
					url: ekaajax.ajaxurl,
					type: 'post',
					async: true,
					cache: false,
					data: {
						action: 'item_sort',
						order: jQuery(this).sortable( 'toArray', { attribute: 'class' } ).toString()
					},
					success: function(response) {
						jQuery('#loading-animation').hide();
						return;
					},
					error: function(xhr,textStatus,e) {
						jQuery('#loading-animation').hide();
						return;
					}
				});
			}
		});
		
		// Uploading images added via DnD
		// We are keeping the old-fashioned file upload on form submit as well for older browser (< IE9) support.
		function XHRuploadFiles(){
			
			// check if there are files
			if (typeof formdata !== "undefined") {
				
				formdata.append("_ajax_nonce", ekaajax.ajaxnonce);
				formdata.append("action", "return_attachment");
				
				jQuery.ajax({
					url: ekaajax.ajaxurl,
					type: 'POST',
					xhr: function() {
						ekaXhr = jQuery.ajaxSettings.xhr();
						/*if(ekaXhr.upload){
							ekaXhr.upload.addEventListener('load',finishUpload, false);
						}*/
						return ekaXhr;
					},
					data: formdata,
					cache: false,
					contentType: false,
					processData: false,
					success: function(output) {
						jQuery('.eka_dnd_files').val(output);
						filesDropped = 0;
						$activeForm.submit();
					},
					/*beforeSend: function() {
						$activeForm.append('Laen faile üles...');
					},*/
					error: function(jqXHR, textStatus, errorThrown){
						//console.log('Error detected: ' + errorThrown + ' ' + textStatus);
					}
				});
			
			}
		}

		// Updating post content
		function saveChanges( postid ) {
			jQuery( '#loading-animation' ).show();

			var metas = {};

			jQuery( '.editable-meta.editable-' + postid ).each( function() {
				metas[ jQuery( this ).data('meta-name') ] = jQuery( this ).html();
			});

			var params = {
				"post"		: postid,
				"content"	: jQuery( '.contentbox.editable-' + postid ).html(),
				"title"		: jQuery( '.titlebox.editable-' + postid ).text(),
				"metas"		: metas,
				"action"	: "save_changes"
			};

			jQuery.post( ekaajax.ajaxurl, params, function ( data ) {
				if( data ){
					jQuery( '#loading-animation' ).hide();
				};
			});
		}

		// Gallery scrolling

		if ( $gallery.find( '.imagewrapper' ).length && $window.width() > 1170 ) {

			if ( gheight > cheight ) {

				// Set a minimum height of the content so there would always be a small scroll, in case there is none
				if ( cheight < wheight ) {
					var added = ( wheight - cheight + 50 );
					cheight += added;
					jQuery( '#content' ).css( 'height', '+=' + added );
				}

				difference = ( gheight - cheight ) / ( cheight - wheight );
				movement = '-';
			} else {
				difference = ( cheight - gheight ) / ( cheight - wheight );
				movement = '';
			}

			jQuery(window).scroll( function(){				
				$gallery.css( { transform:  'translateY(' + ( movement + $window.scrollTop() * ( difference ) ) + 'px)' } );
			});

		}


	});