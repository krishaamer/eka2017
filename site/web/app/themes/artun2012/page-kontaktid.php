<?php
/**
 * Template Name: Kontaktide leht
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	
	<div class="pageheader">
		<h2><?php the_parent_title(); ?></h2>
		<?php eka_page_menu(); ?>
	</div><!--/.pageheader-->

	<div class="content">
		<section class="clearfix" style="margin-bottom: 2em">
			<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			    <input type="text" class="stdsearchbox" value="" name="s" id="s" placeholder="<?php echo __( 'Otsi nime...', 'artun2012' ); ?>" />
			    <input type="submit" class="stdsearchbtn" id="searchsubmit" value="<?php echo __( 'Otsi', 'artun2012' ); ?>" />
			</form>
		</section>
		<section id="contacts_content">

			<?php eka_add_contact_form(1); ?>

			<?php
				// Get all the categories, arrange them into a multidimensional parent->cat->child array for hierarchy
				// 1 = undefined, 3 = erialad, 39 = tugiüksused

				if ( ICL_LANGUAGE_CODE == 'en') {
					$depts = 270;
					$structs = 271;
				} else {
					$depts = 3;
					$structs = 39;
				}


				$allcats = get_terms( 'category', array( 
					'orderby'	=> 'name',
					'exclude'	=> array( 1, $depts, $structs )
				));

				$cats = array();
				foreach( $allcats as $cat ) {
					$cats[ $cat->parent ][] = $cat;
				}

			?>
				<h3><?php echo __( 'Tugiüksused', 'artun2012' ) ?></h3>
				<ul class="contact_list">
			<?php

				// Tugistruktuurid
				foreach( $cats[$structs] as $cat ) {
					echo '<li id="cat_' . $cat->term_id . '" class="clearfix"><div class="column_left_contacts"><span class="contact_list_click" data-catid="' . $cat->term_id . '">' . $cat->name . '</span></div><div class="column_right_contacts" id="catbox_' . $cat->term_id . '"></div></li>';
				}

				echo '</ul><ul>';

				// Teaduskonnad
				foreach( $cats[$depts] as $div ) {
					echo '<li id="cat_' . $div->term_id . '" class="clearfix"><div class="column_left_contacts"><span class="contact_list_title contact_list_click" data-catid="' . $div->term_id . '">' . $div->name . '</span></div><div class="column_right_contacts" id="catbox_' . $div->term_id . '"></div>
							<ul class="contact_list">';

					if ( array_key_exists( $div->term_id, $cats ) ) {

						foreach( $cats[ $div->term_id ] as $cat ) {
							echo '<li id="cat_' . $cat->term_id . '" class="clearfix"><div class="column_left_contacts"><span class="contact_list_click" data-catid="' . $cat->term_id . '">' . $cat->name . '</span></div><div class="column_right_contacts" id="catbox_' . $cat->term_id . '"></div></li>';
						}

					}

					echo '</ul></li>';

				}

				echo '</ul>';

			?>
			 					
		</section>

		<?php setPostViews(get_the_ID()); ?>

<?php endwhile; ?>
			   
<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>