<?php
/**
 * Template Name: Eriala, inimesed
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 4.0
 */

/* Includes the header.php and everything inside it */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<div class="pageheader">
		<h2><?php the_parent_title(); ?></h2>
		<?php eka_page_menu(); ?>
	</div><!--/.pageheader-->

	<?php setlocale( LC_ALL, "et_EE" ); ?>

	<div class="content clearfix" id="content">
	<h3><?php the_title(); ?></h3>
		<?php eka_add_contact_form(); ?>			

			<?php if ( eka_current_user_has_rights() ) : ?>
				<ul class="sortable" style="list-style:none;">
			<?php else : ?>
				<ul class="visitor" style="list-style:none;">
			<?php endif; ?>
			<?php	
				$category = get_the_category(); 

				$et_cat_id = icl_object_id( $category[0]->term_id, 'category', true, 'et' );

				// arguments to and get the posts for contacts			
				$contacts = get_posts( array( 
					'post_type' 		=> 'eka_contacts',
					'numberposts'     	=> -1,
					'lang'				=> '',
					'category__in' 		=> array( $et_cat_id ),
					'orderby'         	=> 'menu_order',
					'order'           	=> 'ASC'
				) );
													
				// begin the posts (contacts) loop
				foreach( $contacts as $post ) : setup_postdata( $post ); ?>
					<li class="<?php echo $post->ID; ?> vcard clearfix contact_card">
						<h4 class="contact_title">
							<a href="<?php the_permalink(); ?>" class="fn bg"><?php the_title(); ?></a>
						</h4>
						<p>
							<span class="bg"><?php editable_post_meta( $post->ID, 'contact_role', 'input' ); ?></span>
						</p>
						<p>
							<span class="tel bg"><?php editable_post_meta( $post->ID, 'contact_phone', 'input' ); ?></span>
							<br />
							<a href="mailto:<?php echo get_post_meta($post->ID, 'contact_email', true); ?>" class="email bg">
							<?php editable_post_meta( $post->ID, 'contact_email', 'input' ); ?>
							</a>
						</p>
						<p class="contact_portrait">
							<a href="<?php the_permalink(); ?>"><?php eka_first_image( $post->ID ); ?></a>
						</p>
						<?php if ( current_user_can('edit_posts') ) : ?>
							<div class="edit">										
								<span class="bg"><span class="sort handle">Sorteeri</span> <?php echo wp_delete_post_link(); ?></span>
							</div>
						<?php endif; ?>
					</li>
			<?php endforeach;  wp_reset_postdata(); ?>
				</ul>



<?php endwhile; ?>

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>