<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0s
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="pageheader">
		<h2><?php echo __( 'Page not found' ); ?></h2>
</div><!--/.pageheader-->
<div class="content clearfix" id="content">
	<div class="">
		Lehte mida otsid ei leitud. <a href="<?php echo get_option( 'home' ); ?>">Naase esilehele</a> või proovi otsingut.
		<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<input type="text" class="bigsearchbox" value="<?php echo get_search_query(); ?>" name="s" id="s" placeholder="<?php echo __( 'Otsi nime, eriala, dokumenti, ...', 'artun2012' ); ?>" />
			<input type="submit" class="bigsearchbtn" id="searchsubmit" value="<?php echo __( 'Otsi', 'artun2012' ); ?>" />
		</form>
	</div>
</div>


<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>