<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file 
 *
 * Please see /external/starkers-utilities.php for info on get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<div class="indexlinks clearfix" id="indexlinks">

<?php
	$pages = get_pages( array(
		'sort_order' 	=> 'ASC',
		'sort_column' 	=> 'menu_order',
		'hierarchical' 	=> false,
		'parent' 		=> 0,
		'post_type' 	=> 'page',
		'post_status' 	=> 'publish'
	) ); 
		
		$links = '<ul><li class="searchbox-home">' . get_search_form( false ) . '</li>';
		
		$j = 0;				
		foreach ( $pages as $page ) {
			
			$display = 'block';
	
			$links .= '<li><span class="nav-' . $page->ID . ' indexanchor clickable" data-pageid="' . $page->ID . '">' . $page->post_title . '</span><div id="nav-'.$page->ID.'" class="indexpane"><ul>';
			
			// If we're dealing with departments menu
			if ( $page->ID == 5 || $page->ID == 5642 ) {

				$links .= '<li class="menutitle">' . __( 'Bakalaureus', 'artun2012' ) . '</li>';
				$subba = eka_get_links( $page->ID, 'ba' );
				foreach ( $subba as $subpage ) {
					$links .= '<li><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}
				$subbax = eka_get_links( $page->ID, 'bax' );
				if ( $subbax ) {

					$links .= '<li class="menutitle">' . __( 'Tsükliõpe', 'artun2012' ) . '</li>';
					foreach ( $subbax as $subpage ) {
						$subcurrent = '';
						if ( is_tree($subpage->ID) || $subpage->ID == $wp_query->post->ID ) {
							$subcurrent = ' current_page_item';
						}
						$links .= '<li class="nav-' . $subpage->ID . $subcurrent . '"><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
					}

				}

				$links .= '</ul><ul><li class="menutitle">' . __( 'Magister', 'artun2012' ) . '</li>';
				$subma = eka_get_links( $page->ID, 'ma' );
				foreach ( $subma as $subpage ) {
					$links .= '<li><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}

				$links .= '</ul><ul><li class="menutitle">' . __( 'Doktoriõpe', 'artun2012' ) . '</li>';
				$subphd = eka_get_links( $page->ID, 'phd' );
				foreach ( $subphd as $subpage ) {
					$links .= '<li><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
				}

			// If we're dealing with the regular menu
			} else {	

				$subpages = eka_get_links( $page->ID );

				$subsize = count( $subpages );
				if ( $subsize <= 2 ) {
					$linkspercolumn = 2;
				} else {
					$linkspercolumn = round( $subsize/3 );
				}
										
				$i = 0;
				foreach ( $subpages as $subpage ) {
					$links .= '<li><a href="' . site_url() . '/' . $page->post_name . '/' . $subpage->post_name . '">' . $subpage->post_title . '</a></li>';
					
					$i++;
					
					if ( $i == $linkspercolumn ) {
						$links .= '</ul><ul> ';
						$i = 0;
					}
				}
			}
			
			$links .= '</ul></div></li>';			

			if ( $j == 2 ) {
				$links .= '</ul><ul> ';
			}

			$j++;		
		}
		
		$links .= '	</ul>';
		echo $links;
		
?>
</div><!--/.indexlinks-->

<?php get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>