<?php
	//ini_set('display_errors',1); 
?>
<?php

require_once("__inc_wp.php");

//If there's already a user logged in, tell the user and give them a link back to where they were.
$currUser = wp_get_current_user(); 
if( $currUser->ID ) {
   	die('Kasutaja ' . $currUser->user_login . ' on juba sisse logitud! <br /><br /><a href="' . get_site_url() . '">Mine esilehele</a>');
}

$text = "Login sisse…";

define('CLIENT_ID','125836874115.apps.googleusercontent.com');
define('CLIENT_SECRET','kpKbdlao3VdAiw9Vcc_dQAzq');
define('REDIRECT_URI','http://www.artun.ee/wp-content/plugins/eka-user-management/login.php');

/*
Requests EKA login details. 
*/
 
if ( isset($_GET['code']) && $_GET['code'] != '') {
	if ( $_GET['state'] != '' && wp_verify_nonce($_GET['state'], 'eka-login-nonce') ) {

		/*
		 	Exchanges code for access token
		*/
		
		$token_response = wp_remote_request('https://accounts.google.com/o/oauth2/token', array(
			'method' => 'POST',
			'timeout' => 60,
			'sslverify' => false,
			'body' =>  array(
				'client_id' => CLIENT_ID,
				'client_secret' => CLIENT_SECRET,
				'code' => $_GET['code'],
				'redirect_uri' => REDIRECT_URI,
				'grant_type' => 'authorization_code'
			)
		));		

		// Check for errors in request, if there are none, continue…
		if( is_wp_error( $token_response ) ) {
		   echo 'ERROR: Koodi vahetamine juurdepääsutokeni vastu ebaõnnestus. Palun edastage see sõnum kommunikatsioonitalitusele.';
		} else {
			$args = json_decode( wp_remote_retrieve_body( $token_response ), true );
			
			$token = $args['access_token'];			
			
			/*
			 	Request user's details from Google
			*/
		
			$user_response = wp_remote_request('https://www.googleapis.com/oauth2/v1/userinfo?access_token='.$token, array(
				'timeout' => 60,
				'sslverify' => false,
				'method' => 'GET'
			));
			
				
			// Check for errors in request, if there are none, continue…
			if( is_wp_error( $user_response ) ) {
			   echo 'ERROR: Kasutaja andmete päring ebaõnnestus.';
			} else {
				$target_user = json_decode( wp_remote_retrieve_body( $user_response ), true );
				
				/*echo wp_remote_retrieve_body( $user_response );
				
				echo '<p>wtf?</p>';
				
				print_r($target_user);
			
				echo $target_user['email'];*/
			
				// check if we got an e-mail address back and wether its an artun.ee one
				if( strlen($target_user['email']) != 0 && strpos($target_user['email'], 'artun.ee') !== false ) {
						
					$google_id = $target_user['id'];
						
					// this could potentially be insecure, but we need to log the user in properly and as such need to "know" its password. 
					// so we're going to create it based on Google ID
						
					$secret_chars = array('p','?','$','%','^','&','q','4','e','D','H','y','K','i','7','!','X','s','d','f','g','h','j','k','l','z','x','c','v','b','n','m','a','Q','W','E','R','T','Y','U','I','O','P','A','S','r','F','G','t','J','u','L','Z','a','C','V','B','N','M','1','2','3','4','5','6','7','8','9');
							
					$user_password = array();
					$user_password = str_split(number_format($google_id/($google_id[7]+7), 0, '', ''));
												
					$pass_length = count($user_password);
																						
					for ($i=0; $i<$pass_length; $i++) {
						$user_password[$i] = $secret_chars[$user_password[$i]+$i];
					}
							
					$user_password = implode("", $user_password);
										
					// tries to receive a user with that info
					$existing_user = get_users(array( 'meta_key' => 'google_id', 'meta_value' => $google_id ));
						
					// if there is a user, get its details
					if ( $existing_user ) {
						foreach ($existing_user as $user) {
							$user_login_id = $user->ID;
							$user_login_name = $user->user_login;
						}
					// if not, register a new one.
					} else {
						$user_data = array();
						$user_data['user_login']    = str_replace( "@artun.ee", "", $target_user["email"] );
						$user_data['user_pass']     = $user_password;
						$user_data['user_nicename'] = sanitize_title( $target_user['name'] );
						$user_data['first_name']    = $target_user['given_name'];
						$user_data['last_name']     = $target_user['family_name'];
						$user_data['display_name']  = $target_user['name'];
						$user_data['user_email']    = $target_user["email"];
							
						$user_login_id = wp_insert_user( $user_data );
						if( is_wp_error($user_login_id) ) {
							die( 'ERROR: Uut kasutajat ei loodud' );
						}
							
						$user_login_name = $user_data['user_login'];
							
						// Add user metadata, so we can find and log in that user the next time around
						update_user_meta($user_login_id, 'google_id', $google_id);
						
						// Add user the "Editor" role
						update_user_meta($user_login_id, 'wp_user_level', 7);						
						update_user_meta($user_login_id, 'wp_capabilities', maybe_unserialize('a:1:{s:6:"editor";b:1;}'));												
						
					}
						
					// All done, Log the user in!
					wp_set_auth_cookie( $user_login_id, true );
						
					$creds = array();
					$creds['user_login'] = $user_login_name;
					$creds['user_password'] = $user_password;
					$creds['remember'] = true;
					$user = wp_signon( $creds, false );
					if ( is_wp_error( $user ) ) {
						echo $user->get_error_message();
					} else {
							
						//Redirect the user to front page
						$redirectTo = get_site_url();
						header("Location: " . $redirectTo);
						exit;
							
					}
				} else {
					echo 'Juurdepääsu pole antud! Palun kasutage sisselogimiseks artun.ee kontot ja lubage veebilehel sellele juurdepääs.';	
				} 	
			} 	
		}
	} else {
		echo 'Puudub juurdepääs sisselogimisele.';	
	}

// No code received, just display the log in link
} else {
     $text = '<a href="http://www.artun.ee">Sisselogimist alusta siit</a>';
}

/*
Generates the logging in URL. 
*/

/*function eka_login_url() {
	
	$scope = urlencode('https://www.googleapis.com/auth/userinfo.email') . '+' . urlencode('https://www.googleapis.com/auth/userinfo.profile');
	$response_type = 'code';
	$domain = 'artun.ee';
	
	$google_dialog_url = sprintf("https://accounts.google.com/o/oauth2/auth?scope=%s&state=%s&redirect_uri=%s&response_type=%s&client_id=%s&hd=%s",
		$scope,
		wp_create_nonce ('eka-login-nonce'),
		urlencode(REDIRECT_URI),
		urlencode($response_type),		
    	urlencode(CLIENT_ID),
		urlencode($domain)
    );

	return '<a href="'. $google_dialog_url .'" title="' . _('Logi sisse') . '">' . _('Logi sisse') . '</a>';	
	
};*/

?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Login sisse… — Eesti Kunstiakadeemia</title>
    </head>
    <body>
    	<?php echo $text; ?>
    </body>
</html>
