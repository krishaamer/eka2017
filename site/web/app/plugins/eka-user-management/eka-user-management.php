<?php
   /*
   Plugin Name: EKA User Profiles Management
   Plugin URI: http://www.artun.ee
   Description: Allows matching users to categories and other functionalities related to user management on artun.ee
   Version: 0.1
   Author: Tanel Kärp
   Author URI: http://ee.linkedin.com/in/tanelkarp
   License: GPL2
   */

   define('CLIENT_ID','125836874115.apps.googleusercontent.com');
   define('CLIENT_SECRET','kpKbdlao3VdAiw9Vcc_dQAzq');
   define('REDIRECT_URI','http://www.artun.ee/wp-content/plugins/eka-user-management/login.php');


/*
	Adds category selection to user profiles
*/
add_action( 'show_user_profile', 'add_user_categories' );
add_action( 'edit_user_profile', 'add_user_categories' );

function add_user_categories( $user ){
    ?>
    <table class="form-table">
    <tr>
        <th><label for="user_categories"><?php _e("Kasutaja osakonnad"); ?></label></th>
        <td>
            <?php
                $data = get_the_author_meta( 'user_categories', $user->ID );
					
				$categories = get_categories(array( 
					'hide_empty' 	=> false, 
					'taxonomy'		=> 'category',
					'exclude'		=> '1',					
				));

				// Helper function to retireve categories in hierarchical order for better usability
				// First index all categories by parent id, for easy lookup later
				$cats_by_parent = array();
				foreach ( $categories as $cat ) {
					$parent_id = $cat->category_parent;
					if ( !array_key_exists( $parent_id, $cats_by_parent ) ) {
						$cats_by_parent[$parent_id] = array();
					}
					$cats_by_parent[$parent_id][] = $cat;
				}
								
				// Then build a hierarchical tree
				$cat_tree = array();
				function add_cats_to_bag( $catsarray, &$child_bag, &$children )	{
					// for reasons beyond our universe, "global" doesn not work for the $cats_by_parent array.
					$cats_by_parent = $catsarray;					
										
					foreach ($children as $child_cat) {
												
						$child_id = $child_cat->cat_ID;
						
					 	if ( array_key_exists( $child_id, $cats_by_parent ) ) {
							
							
							$child_cat->children = array();
							add_cats_to_bag( $cats_by_parent, $child_cat->children, $cats_by_parent[$child_id] );
						} 
					 	$child_bag[$child_id] = $child_cat;
				   	}
				}
				add_cats_to_bag($cats_by_parent, $cat_tree, $cats_by_parent[0]);
								
                if ($cat_tree){
                    foreach ( $cat_tree as $categoryheading ){ 
						echo '<h3>' . $categoryheading->name . '</h3>';
												
						foreach ( $categoryheading->children as $category ){ 							
							if(in_array($category->term_id,(array)$data)) {
								$selected = 'checked="checked""';
							} else {
								$selected = '';
							}
							echo '<input name="user_categories[]" value="'.$category->term_id.'" '.$selected.' type="checkbox"/> '.$category->name.'<br/>';
							if ( $category->children ) {
								foreach ( $category->children as $subcategory ){ 							
									if(in_array($subcategory->term_id,(array)$data)) {
										$selected = 'checked="checked""';
									} else {
										$selected = '';
									}
									echo '&nbsp&nbsp&nbsp&nbsp <input name="user_categories[]" value="'.$subcategory->term_id.'" '.$selected.' type="checkbox"/> '.$subcategory->name.'<br/>';
																
								}	
							}
						}
                    }
                }
            ?>
        </td>
    </tr>
    </table>
    <?php
}

//save the user category fields 
add_action( 'personal_options_update', 'save_user_categories' );
add_action( 'edit_user_profile_update', 'save_user_categories' );

function save_user_categories( $user_id ){
    if ( !current_user_can( 'edit_user', $user_id ) ) { return false; }
    update_user_meta( $user_id, 'user_categories', $_POST['user_categories'] );
}


// Checks wether a user has a category defined upon login
function eka_login_category($user_login, $user) {

	// If user has no cat set
	if( !get_the_author_meta( 'user_categories', $user->ID ) ) {
		set_transient( 'cat-' . $user_login, 1, 86400); // 60 * 60 * 24
	};

}
add_action('wp_login', 'eka_login_category', 10, 2);


/*
	Adds new capability check for editing posts and pages.
	Takes into consideration the user's role and category
*/

function eka_current_user_has_rights() {
	global $current_user, $post;
	
	// First, get current user data. Return false if there is none.
	if ( $current_user ) {
		$user = get_userdata( $current_user->ID );
	} else {
		wp_get_current_user();
        $user = get_userdata( $current_user->ID );
	}
    if ( empty( $user ) )
		return false;
	
	// Check if the user is logged in	
	if ( !is_user_logged_in() )
		return false;	
		
	// Check if the user is admin
	if ( current_user_can( 'manage_options' ) )
		return true;
			
	$data = get_the_author_meta( 'user_categories', $user->ID );
	
	// Then get the current page or post categories
	
	if ( $post ) {	
		$categories = get_the_category();
	} else {
		global $wp_query;
		$id = $wp_query->post->ID;
		$categories = get_the_category($id);		
	}
	
	// Loops through all the categories, checks if user cats match any on them. 
	if( $categories ){
		foreach( $categories as $category ) {

			if ( function_exists( 'icl_object_id' ) ) {
				$term_id = icl_object_id( $category->term_id, 'category', true, 'et' );
			} else {
				$term_id = $category->term_id;
			}

			if( in_array( $term_id, (array)$data ) ) {
				return true;
			} 		
		}
	} 
	
	return false;
}

/*
	Adds our capability check to the Front End Editor plugin
*/

function fee_specific_category( $allow, $data ) {
	global $current_user;

	if ( function_exists( 'icl_object_id' ) ) {
		$type = get_post_type($data['post_id']);
		$post_alid = (int) icl_object_id( $data['post_id'], $type, true, 'et' );
	} else {
		$post_alid = (int) $data['post_id'];
	}

	return $allow && ( current_user_can( 'manage_options' ) || in_category( get_the_author_meta( 'user_categories', $current_user->ID ), $post_alid ) );

}
add_filter( 'front_end_editor_allow_post', 'fee_specific_category', 10, 2 );

/*
	Generates the logging in URL. 
*/

function eka_login_url() {
	
	$scope = urlencode('https://www.googleapis.com/auth/userinfo.email') . '+' . urlencode('https://www.googleapis.com/auth/userinfo.profile');
	$response_type = 'code';
	$domain = 'artun.ee';
	
	$google_dialog_url = sprintf("https://accounts.google.com/o/oauth2/auth?scope=%s&state=%s&redirect_uri=%s&response_type=%s&client_id=%s&hd=%s",
		$scope,
		wp_create_nonce ('eka-login-nonce'),
		urlencode(REDIRECT_URI),
		urlencode($response_type),		
    	urlencode(CLIENT_ID),
		urlencode($domain)
    );

	return '<a href="'. $google_dialog_url .'" title="' . _('Logi sisse') . '">' . _('Logi sisse') . '</a>';	
	
};

/*
	Creates an admin page to connect google groups to artun.ee categories
	EDIT: Will user manual user category selection for the time being.
*/
/*add_action('admin_menu', 'jfb_add_admin_page', 99);
function jfb_add_admin_page()
{ 
    global $jfb_name;
    add_options_page("$jfb_name Options", 'WP-FB AutoConn' . (defined('JFB_PREMIUM')?"+":""), 'administrator', "wp-fb-autoconnect", 'jfb_admin_page');
}*/
?>